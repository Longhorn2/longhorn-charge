package com.longhorn;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@EnableFeignClients(basePackages = { "com.longhorn.client" })
public class LonghornWebSocketApplication  {

    public static void main(String[] args) {
        SpringApplication.run(LonghornWebSocketApplication.class, args);
    }

//    ExecutorService workThreadPool = Executors.newCachedThreadPool();
//   // WorkThreadPool workThreadPool = new WorkThreadPool();
//    public void run(String... args) throws Exception {
//        workThreadPool.execute(() -> {
//            try {
//                new WebSocketServer().run();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//    }
}
