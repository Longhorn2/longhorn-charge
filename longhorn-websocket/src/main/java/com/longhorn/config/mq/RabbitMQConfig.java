package com.longhorn.config.mq;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {
    @Autowired
    private MQProperties mqProperties;

    @Bean
    public Queue queue() {
        boolean durable = true;
        boolean exclusive = false;
        boolean autoDelete = false;
        return new Queue(mqProperties.getQueue(), durable, exclusive, autoDelete);
    }

    //创建队列
    @Bean
    public Queue queueMessage() {
        return new Queue(mqProperties.getQueue());
    }
    //    @Bean
//    public DirectExchange defaultExchange() {
//        boolean durable = true;
//        boolean autoDelete = false;
//        return new DirectExchange(mqProperties.getDefaultExchange(), durable, autoDelete);
//    }
    //创建交换器
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(mqProperties.getDefaultExchange());
    }

    //对列绑定并关联到ROUTINGKEY
    @Bean
    Binding bindingExchangeMessages() {
        return BindingBuilder.bind(queueMessage()).to(exchange()).with(mqProperties.getRouteKey());//*表示一个词,#表示零个或多个词
    }

//    @Bean
//    public Binding binding() {
//        return BindingBuilder.bind(queue())
//                .to(defaultExchange())
//                .with(mqProperties.getRouteKey());
//    }

}