package com.longhorn.config.handler;

import com.google.gson.Gson;
import com.longhorn.common.business.WebSocketSessionVo;
import com.longhorn.common.utils.StringUtils;
import com.longhorn.config.mq.MQProperties;
import com.longhorn.config.mq.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class WebSocketImpl implements WebSocket{


    @Resource
    private RabbitMQUtils rabbitMQUtils;
    @Autowired
    private MQProperties mqProperties;

    /**
     * 在线连接数（线程安全）
     */
    private final AtomicInteger connectionCount = new AtomicInteger(0);

    /**
     * 线程安全的无序集合（存储会话）
     */
    private final CopyOnWriteArraySet<WebSocketSession> sessions = new CopyOnWriteArraySet<>();
    public final static Hashtable<String, WebSocketSession> webSocketSession=new Hashtable<>();
    public final WebSocketSessionVo webSocketSessionVo=new WebSocketSessionVo();


    @Override
    public void handleOpen(WebSocketSession session) {
        sessions.add(session);
        int count = connectionCount.incrementAndGet();
        webSocketSession.put(session.getId(),session);
        log.info("a new connection opened，current online count：{}", count);
    }

    @Override
    public void handleClose(WebSocketSession session) {
        sessions.remove(session);
        //webSocketSession.remove(session.getId());
        int count = connectionCount.decrementAndGet();
        log.info("a new connection closed,current online count:{}", count);

        webSocketSessionVo.setSessionId(session.getId());
        webSocketSessionVo.setStatus(0);
        webSocketSessionVo.setDefaultExchange(mqProperties.getWebsocket_url());
        String url[]=session.getUri().toString().split("\\/");
        webSocketSessionVo.setPileCode(url[url.length-1]);
        webSocketSession.remove(url[url.length-1]);

        webSocketSessionVo.setTimes(new Timestamp(System.currentTimeMillis()));
        //webSocketSession.put(session.getId(),session);
        rabbitMQUtils.sendMessage(webSocketSessionVo);

    }

    @Override
    public void handleMessage(WebSocketSession session, String message) {
        // 只处理前端传来的文本消息，并且直接丢弃了客户端传来的消息
        String url[]=session.getUri().toString().split("\\/");
        log.info("Received:<<<=====pileCode:{},received a message:{}",url[url.length-1], StringEscapeUtils.unescapeJava(new Gson().toJson(message)));
        if(message==null||("").equals(message)){
            return;
        }
        webSocketSessionVo.setSessionId(session.getId());
        webSocketSessionVo.setStatus(1);
        webSocketSessionVo.setDefaultExchange(mqProperties.getWebsocket_url());
        webSocketSessionVo.setPileCode(url[url.length-1]);
        List<Object> list= StringUtils.getStrToArrary(message);
        webSocketSessionVo.setData(list);
        webSocketSessionVo.setTimes(new Timestamp(System.currentTimeMillis()));

        //if (webSocketSession.get(session.getId())==null){
        if (webSocketSession.get(url[url.length-1])==null){
            //webSocketSession.put(session.getId(),session);
            webSocketSession.put(url[url.length-1],session);
            sessions.add(session);
            rabbitMQUtils.sendStatus(webSocketSessionVo);
        }
        rabbitMQUtils.sendMessage(webSocketSessionVo);

    }

    @Override
    public void sendMessage(WebSocketSession session, String message) throws IOException {
        String url[]=session.getUri().toString().split("\\/");
        log.info("Send:=====>>>pileCode:{},Send a message:{}",url[url.length-1],message.toString());
        synchronized (session) {   //加锁
            this.sendMessage(session, new TextMessage(message));
        }
    }

    @Override
    public void sendMessage(String userId, TextMessage message) throws IOException {
        Optional<WebSocketSession> userSession = sessions.stream().filter(session -> {
            if (!session.isOpen()) {
                return false;
            }
            Map<String, Object> attributes = session.getAttributes();
            if (!attributes.containsKey("uid") ){
                return false;
            }
            String uid = (String) attributes.get("uid");
            return uid.equals(userId);
        }).findFirst();
        if (userSession.isPresent()) {
            userSession.get().sendMessage(message);
        }
    }

    @Override
    public void sendMessage(String userId, String message) throws IOException {
        this.sendMessage(userId, new TextMessage(message));
    }

    @Override
    public void sendMessage(WebSocketSession session, TextMessage message) throws IOException {
        session.sendMessage(message);
    }

    @Override
    public void broadCast(String message) throws IOException {
        for (WebSocketSession session : sessions) {
            if (!session.isOpen()) {
                continue;
            }
            this.sendMessage(session, message);
        }
    }

    @Override
    public void broadCast(TextMessage message) throws IOException {
        for (WebSocketSession session : sessions) {
            if (!session.isOpen()) {
                continue;
            }
            session.sendMessage(message);
        }
    }

    @Override
    public void handleError(WebSocketSession session, Throwable error) {
        log.error("websocket error：{}，session id：{}", error.getMessage(), session.getId());
    }


    @Override
    public Set<WebSocketSession> getSessions() {
        return sessions;
    }

    @Override
    public int getConnectionCount() {
        return connectionCount.get();
    }

    /**
     * 监听消息
     */
    @RabbitListener(queues = "${mq.websocket_url}")
    public void receive(Message msg, Channel channel) throws Exception {

        //String appId = msg.getMessageProperties().getAppId();
        long tag = msg.getMessageProperties().getDeliveryTag();

        try {
            MessageConverter messageConverter = new SimpleMessageConverter();
            String receiveMsg = String.valueOf(messageConverter.fromMessage(msg));
            //log.info("success:{} " , receiveMsg);
            WebSocketSessionVo vos=new  Gson().fromJson(receiveMsg,WebSocketSessionVo.class);
            //vos.getData().get(1).toString();
            vos.getData().set(1,'"'+vos.getData().get(1).toString()+'"');
            if (vos.getData().size()==4){
                vos.getData().set(2,'"'+vos.getData().get(2).toString()+'"');
            }
//            String data="[";
//            for (int i=0;i<vos.getData().size();i++){
//                if (i==vos.getData().size()-1){
//                    data+=vos.getData().get(i)+"]";
//                }else {
//                    data+=vos.getData().get(i)+",";
//                }
//            }
            if (vos.getPileCode()==null){
                log.info("设备号为空:{}",new Gson().toJson(vos));
                return;
            }
            if (webSocketSession.get(vos.getPileCode())==null){
            //if (webSocketSession.get(vos.getSessionId())==null){
                //System.out.println(new Gson().toJson(vos));
                log.info("已离线:{}",new Gson().toJson(vos));
                return;
            }
            sendMessage(webSocketSession.get(vos.getPileCode()),vos.getData().toString());
            //sendMessage(webSocketSession.get(vos.getSessionId()),data);
        } catch (Exception e) {
            log.error("推送失败-错误信息:{},消息内容:{}", e.getMessage(), String.valueOf(new SimpleMessageConverter().fromMessage(msg)));
            e.printStackTrace();
        } finally {
            channel.basicAck(tag, false);
        }

    }
}
