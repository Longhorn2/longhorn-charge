package com.longhorn.config.handler;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.*;

public class WebSocketInterceptor implements HandshakeInterceptor {

    @Override
    public boolean beforeHandshake(@NonNull ServerHttpRequest request, @NonNull ServerHttpResponse response, @NonNull WebSocketHandler wsHandler, @NonNull Map<String, Object> attributes) throws Exception {
        //if (request instanceof ServletServerHttpRequest) {
            //ServletServerHttpRequest servletServerHttpRequest = (ServletServerHttpRequest) request;
            // 模拟用户（通常利用JWT令牌解析用户信息）
            //String userId = servletServerHttpRequest.getServletRequest().getParameter("uid");
            // TODO 判断用户是否存在
            //attributes.put("uid", userId);
            //servletServerHttpRequest.getServletRequest().setAttribute("Upgrade","websocket");
//            System.out.println(request.getHeaders());
//            ((ServletServerHttpRequest) request).getServletRequest().setAttribute("Upgrade","websocket");
//            Map<String, List<String>> headers = new LinkedHashMap<>();
//            Enumeration<String> nameEnumeration = ((ServletServerHttpRequest) request).getServletRequest().getHeaderNames();
//
//            while (nameEnumeration.hasMoreElements()) {
//                String name = nameEnumeration.nextElement();
//                List<String> values = headers.get(name);
//                if (values == null) {
//                    values = new ArrayList<>();
//                    headers.put(name, values);
//                }
//                Enumeration<String> valueEnumeration = ((ServletServerHttpRequest) request).getServletRequest().getHeaders(name);
//                while (valueEnumeration.hasMoreElements()) {
//                    values.add(valueEnumeration.nextElement());
//                }
//                System.out.println("name=============="+name);
//            }

            return true;
        //}
        //System.out.println("=============2");
        //return false;
    }

    @Override
    public void afterHandshake(@NonNull ServerHttpRequest request, @NonNull ServerHttpResponse response, @NonNull WebSocketHandler wsHandler, Exception exception) {
        response.setStatusCode(HttpStatus.SWITCHING_PROTOCOLS);
    }


}