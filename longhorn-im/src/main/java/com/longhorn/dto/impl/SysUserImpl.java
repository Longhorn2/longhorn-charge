package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.SysUserMapper;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.dto.SysUser;
import com.longhorn.dto.service.SysUserService;
import com.longhorn.model.utils.DesUtils;
import com.longhorn.model.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SysUserImpl implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public ResponseVO saveUserId(Long stationId, String pileCode) {
        List<String> list =sysUserMapper.queryByStationId(stationId);
        redisUtil.set("pile::station::userId::"+pileCode,new Gson().toJson(list));
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO alarmConfig(SysAlarmConfig vo) {
        redisUtil.set("pile::station::config::"+vo.getStation_management_id(),new Gson().toJson(vo));
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

}
