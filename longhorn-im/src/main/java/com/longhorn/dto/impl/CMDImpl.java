package com.longhorn.dto.impl;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.common.ocpp1_6.dot.*;
import com.longhorn.config.mq.ServiceToPileImpl;
import com.longhorn.dto.service.CMDService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class CMDImpl implements CMDService {
    @Resource
    private ServiceToPileImpl serviceToPileImpl;

    @Override
    public ResponseVO startCharge(ReserveNow reserveNow, String pileCode) throws BusinessException {
        serviceToPileImpl.sendReserveNow(reserveNow, pileCode);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO stopCharge(CancelReservation cancelReservation, String pileCode) {
        serviceToPileImpl.sendCancelReservation(cancelReservation,pileCode);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO changeAvailability(ChangeAvailability changeAvailability, String pileCode) {
        serviceToPileImpl.sendChangeAvailability(changeAvailability,pileCode);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }
    @Override
    public ResponseVO sendRemoteStartTransaction(RemoteStartTransaction remoteStartTransaction, String pileCode) {
        serviceToPileImpl.sendRemoteStartTransaction(remoteStartTransaction,pileCode);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

    public ResponseVO sendRemoteStopTransaction(RemoteStopTransaction remoteStopTransaction, String pileCode) {
        serviceToPileImpl.sendRemoteStopTransaction(remoteStopTransaction,pileCode);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }
}
