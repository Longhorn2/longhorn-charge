package com.longhorn.dto.impl;


import com.longhorn.client.MinIoClient;
import com.longhorn.dto.mapper.PileManagementMapper;
import com.longhorn.dto.mapper.SysUserMapper;
import com.longhorn.dto.service.PileManagementService;
import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PileManagementImpl implements PileManagementService {
    @Autowired
    private PileManagementMapper pileManagementMapper;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public PileManagement queryByPileCode(String pile_code) {
        return pileManagementMapper.queryByPileCode(pile_code);
    }
    public List<String> queryByStationId(Long station_management_id){
        return pileManagementMapper.queryByStationId(station_management_id);
    }


}
