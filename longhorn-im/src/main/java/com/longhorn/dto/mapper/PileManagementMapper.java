package com.longhorn.dto.mapper;

import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PileManagementMapper {
    PileManagement queryByPileCode(@Param("pile_code")String pile_code);
    List<String> queryByStationId(@Param("station_management_id")Long station_management_id);
}
