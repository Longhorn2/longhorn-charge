package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public interface SysUserMapper {
    List<String> queryByStationId(@Param("station_management_id")Long station_management_id);

    @Update("update sys_charge_order set status=#{status} where order_id=#{order_id}")
    void updStatusByOrderId(@Param("order_id")String order_id,@Param("status")Integer status);

    @Update("update sys_charge_order set card_code=#{card_code},stop_reason=#{stop_reason},start_time=#{start_time},end_time=#{end_time},status=#{status} where order_id=#{order_id}")
    void updOrder(SysChargeOrder sysChargeOrder);

    @Select("select * from  sys_charge_order where order_id=#{order_id}")
    SysChargeOrder queryByOrderId(@Param("order_id")Long order_id);

    @Update("INSERT INTO sys_charge_order(id,card_code,order_id,stop_reason,start_time,end_time,status,pile_code,create_time) VALUES (#{id},#{card_code},#{order_id},#{stop_reason},#{start_time},#{end_time},#{status},#{pile_code},#{create_time})")
    void addOrder(SysChargeOrder sysChargeOrder);

    @Update("update sys_charge_order set stop_reason=#{stop_reason} where order_id=#{order_id}")
    void updStopReasonByOrderId(@Param("order_id")Long order_id,@Param("stop_reason") Integer stop_reason);
}
