package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysChargeOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Component
public interface SysChargeOrderMapper {

    @Update("update sys_charge_order set stop_reason=#{stop_reason},end_time=#{end_time} where order_id=#{order_id}")
    void updStopReasonByOrderId(@Param("order_id")String order_id, @Param("stop_reason") Integer stop_reason, @Param("end_time")Timestamp end_time);
}
