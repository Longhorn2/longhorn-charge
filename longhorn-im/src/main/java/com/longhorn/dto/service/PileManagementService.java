package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

public interface PileManagementService {
    PileManagement queryByPileCode(@Param("pile_code")String pile_code);

}
