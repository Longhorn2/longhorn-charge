package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

public interface SysUserService {
    ResponseVO saveUserId(Long stationId,String pileCode);

    ResponseVO alarmConfig(SysAlarmConfig vo);

}
