package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.ocpp1_6.dot.*;

public interface CMDService {
    /**
     * 开启充电
     * */
    ResponseVO startCharge(ReserveNow reserveNow, String pileCode);

    /**
     * 停止充电
     * */
    ResponseVO stopCharge(CancelReservation cancelReservation, String pileCode);


    /**
     * 服务器下发 连接器状态更改(解锁充电枪【连接器】)
     * “Inoperative” 不工作 ,不可用 ;
     * “Operative” 可使用
     * */
    ResponseVO changeAvailability(ChangeAvailability changeAvailability, String pileCode);


    /**
     * 服务器下发 远程启动
     * */
    ResponseVO sendRemoteStartTransaction(RemoteStartTransaction RemoteStartTransaction, String pileCode);

    /**
     * 服务器下发 远程停止
     * */
    ResponseVO sendRemoteStopTransaction(RemoteStopTransaction RemoteStopTransaction, String pileCode);


}
