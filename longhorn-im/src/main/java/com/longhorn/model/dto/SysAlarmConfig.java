package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 充电桩系统告警通知配置
 * */
@Data
public class SysAlarmConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 站点id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;

    /**
     * 联系人
     * */
    private String contact_person;
    /**
     * 联系电话
     * */
    private String contact_phone;
    /**
     * 邮箱
     * */
    private String email;


    /**
     * 枪温度
     * */
    private Integer gunTemperature;
    /**
     * 推送方式 1=短信 2=邮箱 3=短信+邮箱
     * */
    private Integer gunPushMethod;

    /**
     * 充电状态 1=停止 2=不停止
     * */
    private Integer gunChargeStatus;
    /**
     * 电池温度
     * */
    private Integer batteryTemperature;
    /**
     * 推送方式 1=短信 2=邮箱 3=短信+邮箱
     * */
    private Integer batteryPushMethod;

    /**
     * 充电状态 1=停止 2=不停止
     * */
    private Integer batteryChargeStatus;

    /**
     * 离线率
     * */
    private Integer offline;
    /**
     * 推送方式 1=短信 2=邮箱 3=短信+邮箱
     * */
    private Integer offlinePushMethod;


    /**
     * 故障率
     * */
    private Integer alarm;
    /**
     * 推送方式 1=短信 2=邮箱 3=短信+邮箱
     * */
    private Integer alarmPushMethod;

    private Timestamp create_time;

    private Timestamp update_time;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
}
