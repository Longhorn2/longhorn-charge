package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SysChargeOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 订单号,主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 订单状态 默认0=启动中  1=充电中 2=停止中 3=结算中 4=已完结
     */
    private Integer status;
    /**
     * 订单来源 Android,iOS,web,app
     */
    private String source;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 订单开始时间
     */
    private String start_time;
    /**
     * 订单结束时间
     */
    private String end_time;
    /**
     * 停止原因：1=app主动停止，2=充满自动停止，3=急停
     */
    private Integer stop_reason;
    /**
     * 启动方式：1=扫码启动，2=插枪自动启动，3=定时启动
     */
    private Integer start_method;
    /**
     * 充电电量
     */
    private Double capacity;
    /**
     * 开始soc
     */
    private Integer start_soc;
    /**
     * 结束soc
     */
    private Integer end_soc;
    /**
     * 充电时常
     */
    private Double charge_time;
    /**
     * 车牌号
     */
    private String car_number;
    /**
     * Vin码
     */
    private String vin;
    /**
     * 充电卡编码
     */
    private String card_code;
    /**
     * 用户id
     */
    private Long user_id;
    /**
     * 充电桩编号
     */
    private String pile_code;
    /**
     * 充电桩枪号，连接器id
     */
    private Long connectorId;
    /**
     *订单id,生成规则 年月日时分秒 + 四位随机数
     */
    private Long order_id;
    /**
     * 该订单用户名
     */
    private String username;

}
