package com.longhorn.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;


@Data
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;


    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 桩编号
     * */
    private String pile_code;

    /**
     * 充电桩抢号  连接器id
     * */
    private Integer connectorId;
    /**
     * 电压 V
     * */
    private Double voltage;

    /**
     * 电流 A
     * */
    private Double current;
    /**
     * 温度
     * */
    private Double temperature;
    /**
     * 功率
     * */
    private Double power;
    /**
     * 当前充电soc
     * */
    private Integer soc;
    /**
     * 订单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long orderId;
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 上报时间
     * */
    private Timestamp create_time;

}
