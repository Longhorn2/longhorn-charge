package com.longhorn.model.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class OcppAuthorizeVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 1=在线  0离线
     * */
    private Integer status;

    private String idTag;
}
