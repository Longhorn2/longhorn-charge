package com.longhorn.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * mqtt推送给前端数据 实体数据
 *
 * */
@Data
public class AppMqttDataVo implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 桩号
     * */
    private String pileCode;
    /**
     * 1=在线 0=离线
     * */
    private Integer status;

    private Object data;
}
