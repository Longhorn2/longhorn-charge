package com.longhorn.config;

import com.google.gson.Gson;
import com.longhorn.common.business.WebSocketSessionVo;
import com.longhorn.common.ocpp1_6.dot.RemoteStartTransaction;
import com.longhorn.config.mq.RabbitMQUtils;
import com.longhorn.config.mq.ServiceToPileImpl;
import com.longhorn.config.mqttImpl.MqttGatewaySendImpl;
import com.longhorn.dto.mapper.SysChargeOrderMapper;
import com.longhorn.model.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 监听过期key
 * */
@Component
@Slf4j
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    @Autowired
    private RedisUtil redisUtil;
    @Resource
    private ServiceToPileImpl serviceToPileImpl;
    @Resource
    private SysChargeOrderMapper sysChargeOrderMapper;
    @Resource
    private MqttGatewaySendImpl mqttGatewaySendImpl;
    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /**
     * 针对redis数据失效事件，进行数据处理
     * @param message
     * @param pattern
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        // message.toString()可以获取失效的key
        String expiredKey = message.toString();

        log.info("过期key:{}",expiredKey);
        log.info("pattern:{}",new String(pattern));
        //避免分布式重复使用
        String oldLock=redisUtil.getAndSet(expiredKey+".lock","1");
        if ("1".equals(oldLock))return;

        if (!expiredKey.contains("online")){
            redisUtil.del(expiredKey,expiredKey+".lock");
            return;
        }
        String flag=expiredKey.replaceAll("online","");
        String data=redisUtil.get(flag+"onlineFlag");
        //RemoteStartTransaction vo=new Gson().fromJson(data,RemoteStartTransaction.class);
        WebSocketSessionVo vo=new Gson().fromJson(data,WebSocketSessionVo.class);

        String code=expiredKey.replaceAll("RemoteStartTransaction::","").replaceAll("::online","");


        RemoteStartTransaction ro=new Gson().fromJson(vo.getData().get(3).toString(),RemoteStartTransaction.class);
        if (vo.getStatus()>=3){//第四次不下发，结算
            log.info("下发三次无响应结束code:{},:{}",code,vo);
            redisUtil.del("RemoteStartTransaction::"+code+"::onlineFlag");
            redisUtil.del(expiredKey,expiredKey+".lock");
            sysChargeOrderMapper.updStopReasonByOrderId(ro.getIdTag(),9,new Timestamp(System.currentTimeMillis()));
            mqttGatewaySendImpl.publishMqttMessageWithTopic(null,code+"/"+ro.getIdTag()+"/ErrorStart",0);

            return;
        }
        redisUtil.del(expiredKey,expiredKey+".lock");
        serviceToPileImpl.sendRemoteStartTransaction(ro,code);
        log.info("再次启动code:{},:{}",code,vo);

    }
}