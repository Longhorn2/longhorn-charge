package com.longhorn.config.mq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mq")
@Data
public class MQProperties {
    private String defaultExchange;
    private String routeKey;
    private String queue;

}