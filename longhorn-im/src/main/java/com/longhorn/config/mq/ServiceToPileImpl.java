package com.longhorn.config.mq;


import com.google.gson.Gson;
import com.longhorn.common.business.WebSocketSessionVo;
import com.longhorn.common.ocpp1_6.dot.*;
import com.longhorn.common.utils.Utils;
import com.longhorn.model.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务器主动下发命令
 * */

@Component
@Slf4j
public class ServiceToPileImpl {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Autowired
    private MQProperties mqProperties;
    @Autowired
    private RedisUtil redisUtil;
    private static ServiceToPileImpl serviceToPileImpl;

    @PostConstruct
    private void init() {
        serviceToPileImpl = this;
        serviceToPileImpl.rabbitTemplate = this.rabbitTemplate;
        serviceToPileImpl.mqProperties = this.mqProperties;
        serviceToPileImpl.redisUtil=this.redisUtil;
    }

    /**
     * 下发取消预定充电
     *
     * */

    public void sendCancelReservation(CancelReservation cancelReservation,String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"CancelReservation");
        list.add(3,new Gson().toJson(cancelReservation).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 连接器状态更改(解锁充电枪【连接器】)
     * “Inoperative” 不工作 ,不可用 ;
     * “Operative” 可使用
     * */

    public void sendChangeAvailability(ChangeAvailability changeAvailability,String pile_code){
        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"ChangeAvailability");
        list.add(3,new Gson().toJson(changeAvailability).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }
    /**
     * 服务器下发配置
     *
     * */

    public void sendChangeConfiguration(ChangeConfiguration changeConfiguration,String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"ChangeConfiguration");
        list.add(3,new Gson().toJson(changeConfiguration).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 清除缓存请求
     *
     * */

    public void sendClearCache(String pile_code){
        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"ClearCache");
        list.add(3,"{}");
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 清除（删除）特定的充电配置文件
     *
     * */

    public void sendClearChargingProfile(ClearChargingProfile clearChargingProfile,String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"ClearChargingProfile");
        list.add(3,new Gson().toJson(clearChargingProfile).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * -服务器下发 数据传输请求
     *
     * */

    public void sendDataTransfer(DataTransfer dataTransfer, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"DataTransfer");
        list.add(3,new Gson().toJson(dataTransfer).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }


    /**
     * -服务器下发 获取复合计划
     *
     * */

    public void sendGetCompositeSchedule(GetCompositeSchedule getCompositeSchedule, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"GetCompositeSchedule");
        list.add(3,new Gson().toJson(getCompositeSchedule).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * -服务器下发 获取配置
     *
     * */

    public void sendGetConfiguration(GetConfiguration getConfiguration, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"GetConfiguration");
        list.add(3,new Gson().toJson(getConfiguration).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }


    /**
     * -服务器下发 获取诊断
     *
     * */

    public void sendGetDiagnostics(GetDiagnostics getDiagnostics, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"GetDiagnostics");
        list.add(3,new Gson().toJson(getDiagnostics).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 获取本地列表版本
     *
     * */

    public void sendGetLocalListVersion(GetLocalListVersion getLocalListVersion, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"GetLocalListVersion");
        list.add(3,new Gson().toJson(getLocalListVersion).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 远程启动事务
     *
     * */

    public void sendRemoteStartTransaction(RemoteStartTransaction remoteStartTransaction, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"RemoteStartTransaction");
        list.add(3,new Gson().toJson(remoteStartTransaction).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("RemoteStartTransaction::"+pile_code+"::"+remoteStartTransaction.getConnectorId()+"::"+remoteStartTransaction.getIdTag(),new Gson().toJson(list).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString(),60);

        //保存10秒 过期重试下发启动充电
        serviceToPileImpl.redisUtil.set("RemoteStartTransaction::"+pile_code+"::online",new Gson().toJson(list).toString(),3);
        String flag=serviceToPileImpl.redisUtil.get("RemoteStartTransaction::"+pile_code+"::onlineFlag");
        if (flag!=null){
            WebSocketSessionVo vos=new Gson().fromJson(flag,WebSocketSessionVo.class);
            vos.setStatus(vos.getStatus()+1);
            serviceToPileImpl.redisUtil.set("RemoteStartTransaction::"+pile_code+"::onlineFlag",new Gson().toJson(vos).toString());
        }else {
            vo.setStatus(1);
            serviceToPileImpl.redisUtil.set("RemoteStartTransaction::"+pile_code+"::onlineFlag",new Gson().toJson(vo).toString());
        }
        //serviceToPileImpl.redisUtil.set("RemoteStartTransaction::"+pile_code+"::onlineFlag",new Gson().toJson(vo).toString());
    }

    /**
     * 服务器下发 远程停止事务
     *
     * */

    public void sendRemoteStopTransaction(RemoteStopTransaction remoteStopTransaction, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"RemoteStopTransaction");
        list.add(3,new Gson().toJson(remoteStopTransaction).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString(),60);

    }


    /**
     * 服务器 立即预订 预约充电
     *
     * */

    public void sendReserveNow(ReserveNow reserveNow, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"ReserveNow");
        list.add(3,new Gson().toJson(reserveNow).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器发送 重置
     *
     * */

    public void sendReset(Reset reset, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"Reset");
        list.add(3,new Gson().toJson(reset).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 本地列表 6.41
     *
     * */

    public void sendSendLocalList(SendLocalList sendLocalList, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"SendLocalList");
        list.add(3,new Gson().toJson(sendLocalList).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }


    /**
     * 服务器下发 设置充电配置文件
     * 使用此消息将计费配置文件发送到计费点 6.43
     *
     * */

    public void sendSetChargingProfile(SetChargingProfile setChargingProfile, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"SetChargingProfile");
        list.add(3,new Gson().toJson(setChargingProfile).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 充电桩发送 开始事务
     *
     * */

    public void sendStartTransaction(StartTransaction startTransaction, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"StartTransaction");
        list.add(3,new Gson().toJson(startTransaction).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 充电桩发送 开始事务
     * param : requestedMessage  “BootNotification”,
     * “DiagnosticsStatusNotification”,
     * “FirmwareStatusNotification”,
     * “Heartbeat”,
     * “MeterValues”,
     * “StatusNotification”
     *
     * */

    public void sendTriggerMessage(TriggerMessage triggerMessage, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"TriggerMessage");
        list.add(3,new Gson().toJson(triggerMessage).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发，解锁连接器
     *
     *
     * */

    public void sendUnlockConnector(UnlockConnector unlockConnector, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"UnlockConnector");
        list.add(3,new Gson().toJson(unlockConnector).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }

    /**
     * 服务器下发 更新固件
     * */

    public void sendUpdateFirmware(UpdateFirmware updateFirmware, String pile_code){

        String webSocketSessionVo=serviceToPileImpl.redisUtil.get("status::"+pile_code);
        if (webSocketSessionVo==null){
            return;
        }
        WebSocketSessionVo vo=new Gson().fromJson(webSocketSessionVo,WebSocketSessionVo.class);
        List<Object>list=new ArrayList<>();
        list.add(0,"2");
        String uuid= Utils.getUuid();
        list.add(1,uuid);
        list.add(2,"UpdateFirmware");
        list.add(3,new Gson().toJson(updateFirmware).toString());
        vo.setData(list);
        serviceToPileImpl.rabbitTemplate.convertAndSend(vo.getDefaultExchange(),new Gson().toJson(vo).toString());
        serviceToPileImpl.redisUtil.set("cmd::"+pile_code+"::"+uuid,new Gson().toJson(list).toString());
    }





}
