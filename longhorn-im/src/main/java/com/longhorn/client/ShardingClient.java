package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.Order;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

//@FeignClient(value = "longhorn-sharding")
@FeignClient(value = "http://192.168.1.214:10002/")
public interface ShardingClient {

    @ApiOperation(value = "充电数据新增")
    @RequestMapping(value = "/api/v1/longhorn-sharding/sharding/andChargeProcessData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO andChargeProcessData(@RequestBody Order vo) throws BusinessException ;
}
