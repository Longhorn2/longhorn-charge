package com.longhorn.controller;


import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.model.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "获取redis缓存")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/redisUtils")
public class RedisUtilsController{

    @Autowired
    private RedisUtil redisUtil;

    @ApiOperation(value = "通过redis key获取value")
    @RequestMapping(value = "/getKey/{key}", method = RequestMethod.GET)
    public ResponseVO getKey(@PathVariable String key) throws BusinessException {
        log.info("key:{},",key);
        return new ResponseVO(StatusCodeEnum.SUCCESS,redisUtil.get(key));
    }
}
