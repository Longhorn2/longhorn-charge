package com.longhorn.controller.cmd;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.ocpp1_6.dot.*;
import com.longhorn.dto.impl.CMDImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Api(tags = "控制下发命令")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/cmd")
public class CMDController {

    @Autowired
    private CMDImpl cmdImpl;

    @ApiOperation(value = "开启预约充电")
    @RequestMapping(value = "/startCharge/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO startCharge(@RequestBody ReserveNow reserveNow, @PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("开启充电:{},pileCode:{}",reserveNow,pileCode);
        return cmdImpl.startCharge(reserveNow,pileCode);
    }
    @ApiOperation(value = "停止充电")
    @RequestMapping(value = "/stopCharge/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO stopCharge(@RequestBody CancelReservation cancelReservation, @PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("停止充电:{},pileCode:{}",cancelReservation,pileCode);
        return cmdImpl.stopCharge(cancelReservation,pileCode);
    }


    @ApiOperation(value = "充电枪状态解锁/锁定")
    @RequestMapping(value = "/changeAvailability/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO changeAvailability(@RequestBody ChangeAvailability changeAvailability, @PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("充电枪状态解锁/锁定:{},pileCode:{}",changeAvailability,pileCode);
        return cmdImpl.changeAvailability(changeAvailability,pileCode);
    }

    @ApiOperation(value = "远程启动")
    @RequestMapping(value = "/remoteStartTransaction/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO sendRemoteStartTransaction(@RequestBody RemoteStartTransaction remoteStartTransaction, @PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("远程启动:{},pileCode:{}",remoteStartTransaction,pileCode);
        return cmdImpl.sendRemoteStartTransaction(remoteStartTransaction,pileCode);
    }

    @ApiOperation(value = "远程停止")
    @RequestMapping(value = "/remoteStopTransaction/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO sendRemoteStopTransaction(@RequestBody RemoteStopTransaction remoteStopTransaction, @PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("远程停止:{},pileCode:{}",remoteStopTransaction,pileCode);
        return cmdImpl.sendRemoteStopTransaction(remoteStopTransaction,pileCode);
    }

}
