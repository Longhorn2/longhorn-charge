package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysUserImpl;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.dto.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "im消息用户信息缓存 存储业务逻辑")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    private SysUserImpl sysUserImpl;

    @ApiOperation(value = "存储通知信息id")
    @GetMapping("/saveUserId/{stationId}/{pileCode}")
    public ResponseVO saveUserId(@PathVariable("stationId") Long stationId,@PathVariable("pileCode")String pileCode) throws BusinessException {
        log.info("存储通知信息stationId:{}",stationId);
        return sysUserImpl.saveUserId(stationId,pileCode);
    }

    @ApiOperation(value = "告警通知配置")
    @RequestMapping(value = "/alarm/config", method = RequestMethod.POST)
    public ResponseVO alarmConfig(@RequestBody SysAlarmConfig vo) throws BusinessException {
        log.info("告警通知信息vo:{}",vo);
        return sysUserImpl.alarmConfig(vo);
    }

}

