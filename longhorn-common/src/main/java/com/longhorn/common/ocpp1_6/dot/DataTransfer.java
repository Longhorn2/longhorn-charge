package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发  数据传输请求
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class DataTransfer implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     *最大长度255
     *供应商 ID
     * */
    private String vendorId;
    /**
     *最大长度50
     *消息 ID
     * */
    private String messageId;
    /**
     *
     *数据
     * */
    private Object data;
}
