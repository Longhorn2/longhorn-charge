package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 立即预订  预约充电
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class ReserveNowResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     * "Faulted",发现错误
     * "Occupied",占用
     * "Unavailable" 不可用的
     * */
    private String status;
}
