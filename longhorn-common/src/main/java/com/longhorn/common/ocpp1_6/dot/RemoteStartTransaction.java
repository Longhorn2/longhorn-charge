package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发  远程启动事务
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class RemoteStartTransaction implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;

    /**
     * 最大长度20
     * Charge Point 必须用来启动交易的标识符
     * */
    private String idTag;

    /**
     * Charge Point 用于请求交易的计费配置文件。ChargingProfilePurpose 必须设置为 TxProfile
     * */
    private ChargingProfile chargingProfile;



}

@Data
class ChargingProfile{
    /**
     * 充电配置文件 Id
     *此配置文件的唯一标识符
     * */
    private Integer chargingProfileId;
    /**
     * 交易标识
     *仅当 ChargingProfilePurpose 设置为 TxProfile 时才有效，transactionId 可用于将配置文件与特定事务匹配
     * */
    private Integer transactionId;
    /**
     * 配置文件层次结构堆栈中的值确定级别。较高的值优先于较低的值。最低等级为 0
     * */
    private Integer stackLevel;
    /**
     * 定义此消息传输的计划的目的
     *
     *"ChargePointMaxProfile",
     *"TxDefaultProfile",
     *"TxProfile"
     * */
    private String chargingProfilePurpose;
    /**
     * 表示日程的种类
     *"Absolute",
     *"Recurring",
     *"Relative"
     * */
    private String chargingProfileKind;

    /**
     * 指示重复的起点
     *"Daily",
     *"Weekly"
     * */
    private String recurrencyKind;
    /**
     * 配置文件开始有效的时间点。如果不存在，则该配置文件在充电点收到后立即生效。
     * 当 ChargingProfilePurpose 为 TxProfile 时不使用
     *"Daily",
     *"Weekly"
     * */
    private String validFrom;
    /**
     *配置文件停止有效的时间点。如果不存在，则该配置文件在被另一个配置文件替换之前一直有效。
     * 当 ChargingProfilePurpose 为 TxProfile 时不使用
     * */
    private String validTo;

    /**
     * 充电时间表 7.13
     * */
    private ChargingSchedule chargingSchedule;

}
