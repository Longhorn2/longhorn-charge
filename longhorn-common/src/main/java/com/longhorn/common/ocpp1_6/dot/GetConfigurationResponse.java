package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 服务器下发 获取获取配置
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetConfigurationResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 已知键列表
     *
     * */
    private List<ConfigurationKey> configurationKey;
    /**
     * 未知键列表
     *
     * */
    private String []unknownKey;



}

@Data
class ConfigurationKey{

    private String key;

    private Boolean readonly;

    private String value;

}