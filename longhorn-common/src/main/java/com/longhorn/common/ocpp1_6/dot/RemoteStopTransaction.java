package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发  远程停止事务
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class RemoteStopTransaction implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 最大长度20
     * Charge Point 必须用来停止交易的标识符
     * */
    private Integer transactionId;

}
