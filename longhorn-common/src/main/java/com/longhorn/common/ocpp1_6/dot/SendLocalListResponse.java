package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 本地列表 6.41
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class SendLocalListResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"Accepted", 同意
     * "Failed",失败
     * "NotSupported",不支持
     * "VersionMismatch" 版本不匹配
     * */
    private String status;
}
