package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器回复  停止交易
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class StopTransactionResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 授权实体类
     * */
    private IdTagInfo idTagInfo;
}
