package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器回复  开始事务
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class StartTransactionResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 授权实体类
     * */
    private IdTagInfo idTagInfo;
    /**
     *交易标识
     *这包含中央系统提供的事务 ID
     * */
    private Integer transactionId;
}
