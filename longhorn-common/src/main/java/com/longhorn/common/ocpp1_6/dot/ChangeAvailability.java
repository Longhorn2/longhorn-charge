package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 连接器状态更改(解锁充电枪【连接器】)
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ChangeAvailability implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;
    /**
     *"Inoperative",不工作 ,不可用
     * "Operative" 可使用
     * */
    private String type;
}
