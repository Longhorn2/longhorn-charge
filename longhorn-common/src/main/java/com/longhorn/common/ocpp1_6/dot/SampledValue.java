package com.longhorn.common.ocpp1_6.dot;


import lombok.Data;

import java.io.Serializable;

/**
 * 采样值 ;一个或多个测量值 列表
 * */
@Data
public class SampledValue implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 值作为“原始”（十进制）数字或“SignedData”。字段类型是“字符串”，以允许数字签名的数据读数。
     * 十进制数值也是可接受的，以允许被测量的小数值，例如温度和电流
     * */
    private String value;
    /**
     * 详细值类型：开始、结束或样本。默认值 = “Sample.Periodic”
     * "Interruption.Begin",
     * "Interruption.End",
     * "Sample.Clock",
     * "Sample.Periodic",
     * "Transaction.Begin",
     * "Transaction.End",
     * "Trigger",
     * "Other"
     * */
    private String context;
    /**
     * 原始数据或签名数据。默认 = “原始”
     * "Raw",
     * "SignedData"
     * */
    private String format;

    /**
     * 测量类型。默认 = “Energy.Active.Import.Register”
     *
     * "Energy.Active.Export.Register",
     * "Energy.Active.Import.Register",
     * "Energy.Reactive.Export.Register",
     * "Energy.Reactive.Import.Register",
     * "Energy.Active.Export.Interval",
     * "Energy.Active.Import.Interval",
     * "Energy.Reactive.Export.Interval",
     * "Energy.Reactive.Import.Interval",
     * "Power.Active.Export",
     * "Power.Active.Import",
     * "Power.Offered",
     * "Power.Reactive.Export",
     * "Power.Reactive.Import",
     * "Power.Factor",
     * "Current.Import",
     * "Current.Export",
     * "Current.Offered",
     * "Voltage",
     * "Frequency",
     * "Temperature",
     * "SoC",
     * "RPM"
     * */
    private String measurand;
    /**
     * 指示如何解释测量值。例如在 L1 和中性线之间 (L1-N) 请注意，
     * 并非所有相位值都适用于所有被测量。当相位缺失时，测量值被解释为一个整体值
     *"L1",
     *"L2",
     *"L3",
     *"N",
     *"L1-N",
     *"L2-N",
     *"L3-N",
     *"L1-L2",
     *"L2-L3",
     *"L3-L1"
     * */
    private String phase;
    /**
     * 测量位置。
     * 默认 =“插座”
     *"Cable",
     *"EV",
     *"Inlet",
     *"Outlet",
     *"Body"
     * */
    private String location;
    /**
     * 值的单位。如果（默认）被测量是“能量”类型，则默认 =“Wh”
     *"Wh",
     *"kWh",
     *"varh",
     *"kvarh",
     *"W",
     *"kW",
     *"VA",
     *"kVA",
     *"var",
     *"kvar",
     *"A",
     *"V",
     *"K",
     *"Celcius",
     *"Celsius",
     *"Fahrenheit",
     *"Percent"
     * */
    private String unit;

}
