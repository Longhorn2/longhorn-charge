package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 交易数据
 * */
@Data
public class TransactionData implements Serializable {
    private static final long serialVersionUID = 1L;


    private String timestamp;

    private List<SampledValue> sampledValue;

}