package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩上报  状态通知
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class StatusNotification implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;

    /**
     * 这包含充电点报告的错误代码
     *"ConnectorLockFailure",
     *"EVCommunicationError",
     *"GroundFailure",
     *"HighTemperature",
     *"InternalError",
     *"LocalListConflict",
     *"NoError",
     *"OtherError",
     *"OverCurrentFailure",
     *"PowerMeterFailure",
     *"PowerSwitchFailure",
     *"ReaderFailure",
     *"ResetFailure",
     *"UnderVoltage",
     *"OverVoltage",
     *"WeakSignal"
     * */
    private String errorCode;

    /**
     * 与错误相关的其他自由格式信息
     * 最大长度50
     * */
    private String info;

    /**
     * 这包含充电点的当前状态
     *  "Available",
     *  "Preparing",
     *  "Charging",
     *  "SuspendedEVSE",
     *  "SuspendedEV",
     *  "Finishing",
     *  "Reserved",
     *  "Unavailable",
     *  "Faulted"
     * */
    private String status;
    /**
     * 报告状态的时间。如果不存在，将假定收到消息的时间
     * */
    private String timestamp;

    /**
     * 供应商 ID
     * 这标识了特定于供应商的实现
     * */
    private String vendorId;

    /**
     * 供应商错误代码 这包含特定于供应商的错误代码
     * */
    private String vendorErrorCode;

}
