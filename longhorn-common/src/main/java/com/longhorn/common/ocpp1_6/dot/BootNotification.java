package com.longhorn.common.ocpp1_6.dot;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩启动通知
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class BootNotification implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     * 充电点供应商 0~20
     * */
    public String chargePointVendor;
    /**
     * 充电点模型 0~20
     * */
    public String chargePointModel;
    /**
     * 充电点序列号 0~25
     * */
    public String chargePointSerialNumber;
    /**
     * 充电盒序列号 0~25
     * */
    public String chargeBoxSerialNumber;
    /**
     * 固件版本 0~50
     * */
    public String firmwareVersion;
    /**
     * 调制解调器 SIM 卡的 ICCID 0~20
     * */
    public String iccid;
    /**
     * 调制解调器 SIM 卡的 IMSI 0~20
     * */
    public String imsi;
    /**
     * 仪表类型 0~25
     * */
    public String meterType;

    /**
     * 仪表序列号 0~25
     * */
    public String meterSerialNumber;

}
