package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩上报 心跳
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class Heartbeat implements Serializable {
    private static final long serialVersionUID = 1L;

    private String currentTime;

}
