package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 服务器下发 本地列表 6.41
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class SendLocalList implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *如果是完整更新，这是完整列表的版本号。在差异更新的情况下，它是应用更新后列表的版本号
     * */
    private Integer listVersion;
    /**
     * 在完全更新的情况下，这包含形成新本地授权列表的值列表。在差异更新的情况下，
     * 它包含要应用于充电点本地授权列表的更改。配置键中提供了最大数量的 AuthorizationData 元素：
     * 发送本地列表最大长度
     * */
    private List<LocalAuthorizationList> localAuthorizationList;

    /**
     * 必需的。这包含此请求的更新类型（完整或差异）
     * "Differential",
     * "Full"
     * */
    private String updateType;
}

@Data
class LocalAuthorizationList{
    /**
     * 最大长度20
     * 标记id
     * */
    private String idTag;
    /**
     * 授权实体类
     * */
    private IdTagInfo idTagInfo;


}