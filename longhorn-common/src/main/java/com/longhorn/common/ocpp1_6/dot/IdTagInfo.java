package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

@Data
public class IdTagInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 日期
     * */
    public String expiryDate;
    /**
     *父id
     *
     * */
    public String parentIdTag;

    /**
     *"Accepted",  接受
     *"Blocked",  已阻止
     *"Expired", 失效,过期
     * "Invalid",无效的
     * "ConcurrentTx"  竞争Tx
     *
     * */
    public String status;
}
