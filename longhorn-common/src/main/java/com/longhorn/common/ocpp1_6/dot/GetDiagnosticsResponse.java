package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 获取诊断
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetDiagnosticsResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 最大长度 255
     * 这包含将上传的带有诊断信息的文件的名称。
     * 当没有可用的诊断信息时，该字段不存在
     * */
    private String fileName;
}
