package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发  清除（删除）特定的充电配置文件
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ClearChargingProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     * 要清除的计费配置文件的 ID
     * */
    private Integer id;
    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;
    /**
     *指定将清除的计费配置文件的用途，如果它们满足请求中的其他标准
     * */
    private String chargingProfilePurpose;

    /**
     *指定将为其清除计费配置文件的堆栈级别，如果它们满足请求中的其他条件
     * */
    private Integer stackLevel;

}
