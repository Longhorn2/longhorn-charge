package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复  设置充电配置文件
 * 使用此消息将计费配置文件发送到计费点 6.43
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class SetChargingProfileResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"Accepted", 同意
     * "Rejected",拒绝
     * "NotSupported",不支持
     * */
    private String status;

}
