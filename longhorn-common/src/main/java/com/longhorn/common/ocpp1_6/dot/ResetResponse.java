package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 重置
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class ResetResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"Accepted", 同意
     *"Rejected", 拒绝
     * */
    private String status;
}
