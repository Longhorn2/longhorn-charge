package com.longhorn.common.ocpp1_6.dot;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器主动下发  取消预订
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class CancelReservation implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     * 取消预订的 ID
     * */
    private Integer reservationId;


}
