package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 解锁连接器
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class UnlockConnector implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;


}
