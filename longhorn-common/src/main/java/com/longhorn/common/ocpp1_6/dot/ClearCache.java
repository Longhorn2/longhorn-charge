package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发  清除缓存请求
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ClearCache implements Serializable {
    private static final long serialVersionUID = 1L;




}
