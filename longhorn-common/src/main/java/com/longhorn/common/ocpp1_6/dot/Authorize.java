package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩发送身份信息给服务端授权
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public  class Authorize implements Serializable {
    private static final long serialVersionUID = 1L;

    public String idTag;
}