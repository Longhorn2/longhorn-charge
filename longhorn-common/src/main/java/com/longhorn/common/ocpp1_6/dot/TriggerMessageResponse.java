package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 触发消息
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class TriggerMessageResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",同意
     *"Rejected",拒绝
     * "NotImplemented" 未实施
     * */
    private String status;

}
