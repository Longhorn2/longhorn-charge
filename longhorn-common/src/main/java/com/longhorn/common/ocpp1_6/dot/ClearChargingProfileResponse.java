package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复  清除（删除）特定的充电配置文件
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ClearChargingProfileResponse implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     *"Accepted",  接受
     *"Unknown", 未知
     * */
    private String status;
}
