package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 充电桩上报  仪表值  包括充电电压、电流、SOC等
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class MeterValues implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;
    /**
     * 与这些仪表样本相关的事务
     * 交易标识
     * */
    private Integer transactionId;
    /**
     * 仪表值
     * */
    private List<MeterValue> meterValue;


}


