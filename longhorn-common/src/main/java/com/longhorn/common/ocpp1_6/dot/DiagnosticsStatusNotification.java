package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩上报 诊断状态通知
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class DiagnosticsStatusNotification implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Idle",  空闲
     *"Uploaded", 已上传
     * "UploadFailed" 下载失败
     * "Uploading"  上传中
     * */
    private String status;

}
