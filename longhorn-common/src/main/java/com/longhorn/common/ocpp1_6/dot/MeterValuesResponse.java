package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器回复  仪表值  包括充电电压、电流、SOC等
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class MeterValuesResponse implements Serializable {
    private static final long serialVersionUID = 1L;



}
