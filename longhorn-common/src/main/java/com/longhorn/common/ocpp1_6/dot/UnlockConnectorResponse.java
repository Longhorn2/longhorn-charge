package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
/**
 *
 * 充电桩回复 解锁连接器
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class UnlockConnectorResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Unlocked",已解锁
     *"UnlockFailed",解锁失败
     * "NotSupported" 不支持
     * */
    private String status;
}
