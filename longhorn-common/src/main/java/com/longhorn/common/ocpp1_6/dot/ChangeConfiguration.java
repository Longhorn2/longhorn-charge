package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发配置
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ChangeConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"最大长度50
     * */
    private String key;
    /**
     *"最大长度500
     * */
    private String value;

}
