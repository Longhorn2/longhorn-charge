package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 获取复合计划
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetCompositeSchedule implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;
    /**
     *时间以秒为单位。请求时间表的长度
     *
     * */
    private Integer duration;
    /**
     * 可用于强制功率或电流配置文件
     * A 安培（电流）
     * W  瓦特（功率）
     * */
    private String chargingRateUnit;


}
