package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 连接器状态更改(解锁充电枪【连接器】)
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ChangeAvailabilityResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     *"Scheduled", 预定，安排
     * */
    private String status;
}
