package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器 立即预订  预约充电
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class ReserveNow implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;

    /**
     * 这包含预订结束的日期和时间
     * */
    private String expiryDate;
    /**
     * 最大长度20
     * 身份标签
     * Charge Point 必须为其保留连接器的标识符
     * */
    private String idTag;
    /**
     * 最大长度20
     *父 idTag
     * */
    private String parentIdTag;
    /**
     * 预订编号
     * 此预订的唯一 ID
     * */
    private Integer reservationId;


}
