package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 获取本地列表版本
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetLocalListVersion implements Serializable {
    private static final long serialVersionUID = 1L;


}
