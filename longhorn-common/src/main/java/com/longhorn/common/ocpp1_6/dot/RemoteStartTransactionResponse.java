package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复  远程启动事务
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class RemoteStartTransactionResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     * */
    private String status;


}
