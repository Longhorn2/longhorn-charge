package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 获取配置
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *
     * 请求配置值的键列表
     * */
    private String [] key;
}
