package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 获取诊断
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetDiagnostics implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 这包含应将诊断文件上传到的位置（目录）
     * */
    private String location;
    /**
     * 这指定 Charge Point 在放弃之前必须尝试上传诊断信息的次数。如果此字段不存在，则由 Charge Point 决定它想要重试多少次
     * */
    private Integer retries;
    /**
     * 可以尝试重试的时间间隔（以秒为单位）。如果此字段不存在，则由 Charge Point 决定尝试之间等待多长时间
     * */
    private Integer retryInterval;
    /**
     * 这包含要包含在诊断中的最早记录信息的日期和时间
     * */
    private String startTime;
    /**
     * 这包含要包含在诊断中的最新日志记录信息的日期和时间
     * */
    private String stopTime;


}
