package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器回复 心跳
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class HeartbeatResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 当前时间
     *这包含中央系统的当前时间
     * */
    private String currentTime;

}
