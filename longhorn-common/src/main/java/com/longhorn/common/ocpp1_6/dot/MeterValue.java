package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 仪表值列表
 * */
@Data
public class MeterValue implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 测量值的时间戳
     * */
    private String timestamp;
    /**
     * 采样值 ;一个或多个测量值
     *
     * */
    private List<SampledValue> sampledValue;

}
