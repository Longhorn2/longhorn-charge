package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩发送  开始事务
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class StartTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;
    /**
     * 最大长度20
     * 这包含必须为其启动事务的标识符；身份标签
     * */
    private String idTag;
    /**
     * 这包含事务开始时连接器的计量值（以 Wh 为单位）
     * */
    private Integer meterStart;
    /**
     * 取消预订的 ID
     * */
    private Integer reservationId;
    /**
     * 这包含事务开始的日期和时间
     * */
    private String timestamp;

}
