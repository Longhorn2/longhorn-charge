package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 更新固件
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class UpdateFirmwareResponse implements Serializable {
    private static final long serialVersionUID = 1L;

}
