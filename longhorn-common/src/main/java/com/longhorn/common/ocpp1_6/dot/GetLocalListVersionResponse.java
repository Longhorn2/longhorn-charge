package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 获取本地列表版本
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetLocalListVersionResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 列表版本
     * 其中包含 Charge Point 中本地授权列表的当前版本号
     * */

    private  Integer listVersion;
}
