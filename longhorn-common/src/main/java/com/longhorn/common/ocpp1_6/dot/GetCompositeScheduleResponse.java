package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 充电桩回复 获取复合计划
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class GetCompositeScheduleResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *需要更改可用性的连接器的 ID。如果充电点及其所有连接器的可用性需要更改，则使用 ID“0”（零）
     *
     * */
    private Integer connectorId;


    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     *
     * */
    private String status;


    /**
     * 时间 2021.02.21 12:00:00
     *  计费配置文件中包含的周期与该时间点相关
     *
     * */
    private String scheduleStart;

    /**
     *  计划的复合充电时间表，随时间的能量消耗。总是相对于 ScheduleStart
     *
     * */
    private ChargingSchedule chargingSchedule;
}
/**
 * 充电时间表 7.13
 * */
@Data
class ChargingSchedule{
    /**
     *时间以秒为单位。请求时间表的长度
     *充电计划的持续时间（以秒为单位）。如果持续时间为空，则最后一个周期将无限期地继续，或者直到事务结束，以防 startSchedule 不存在
     * */
    private Integer duration;

    /**
     * 时间 2021.02.21 12:00:00
     *   绝对时间表的起点。如果没有，时间表将与充电开始有关
     * */
    private String startSchedule;
    /**
     * A 安培（电流）
     * W  瓦特（功率）
     * 计量单位限制以单位表示
     * */
    private String chargingRateUnit;

/**
 * 计费时间表
 * */
    private List<ChargingSchedulePeriod>chargingSchedulePeriod;
/**
 *最小充电率
 * 电动汽车支持的最低充电率。计量单位由 chargingRateUnit 定义。该参数旨在供本地智能充电算法使用，
 * 以优化功率分配，以防充电过程在较低充电速率下效率低下。最多接受一位小数（例如 8.1）
 * */
    private double minChargingRate;

}
/**
 * 计费时间表
 * */
@Data
class ChargingSchedulePeriod{
    /**
     *
     * 周期的开始时间，以计划开始后的秒数为单位。
     * StartPeriod 的值还定义了上一周期的停止时间
     * */
    private Integer startPeriod;
    /**
     *
     * 计划期间的功率限制，以安培表示。最多接受一位小数（例如 8.1）
     * */
    private double limit;
    /**
     * 可用于充电的相数。如果需要多个阶段，将假定 numberPhases=3，除非给出另一个数字
     * */
    private Integer numberPhases;

}
