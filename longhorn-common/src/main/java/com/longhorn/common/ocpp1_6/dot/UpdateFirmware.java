package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器下发 更新固件
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class UpdateFirmware implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 这包含应将诊断文件上传到的位置（目录）
     * */
    private String location;
    /**
     * 这指定 Charge Point 在放弃之前必须尝试上传诊断信息的次数。如果此字段不存在，则由 Charge Point 决定它想要重试多少次
     * */
    private Integer retries;
    /**
     * 可以尝试重试的时间间隔（以秒为单位）。如果此字段不存在，则由 Charge Point 决定尝试之间等待多长时间
     * */
    private Integer retryInterval;

    /**
     * 日期和时间
     * 这包含充电点必须检索（新）固件的日期和时间
     * */
    private String retrieveDate;

}
