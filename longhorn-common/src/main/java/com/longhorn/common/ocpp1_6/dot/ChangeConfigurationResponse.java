package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复服务器下发配置
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class ChangeConfigurationResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     * "RebootRequired"  需要重新启动
     *"NotSupported" 不支持
     * */
    private String status;

}
