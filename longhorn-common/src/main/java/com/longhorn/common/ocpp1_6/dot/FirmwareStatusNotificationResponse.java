package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器回复 固件状态通知
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class FirmwareStatusNotificationResponse implements Serializable {
    private static final long serialVersionUID = 1L;

}
