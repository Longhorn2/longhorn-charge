package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 充电桩上报  停止交易
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class StopTransaction implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     * 最大长度20
     * 这包含必须为其启动事务的标识符；身份标签
     * */
    private String idTag;
    /**
     * 这包含事务开始时连接器的计量值（以 Wh 为单位）
     * */
    private Integer meterStop;

    /**
     * 这包含交易停止的日期和时间
     * */
    private String timestamp;

    /**
     * 这包含由StartTransaction.conf 交易标识
     * */
    private Integer transactionId;
    /**
     * 这包含事务被停止的原因。仅当原因为“本地”时才可以省略
     *"EmergencyStop",
     * "EVDisconnected",
     * "HardReset",
     * "Local",
     * "Other",
     * "PowerLoss",
     * "Reboot",
     * "Remote",
     * "SoftReset",
     * "UnlockCommand",
     * "DeAuthorized"
     * */
    private String reason;
    /**
     * 这包含与计费目的相关的交易使用详细信息
     * */
    private List<TransactionData> transactionData;
    
}

