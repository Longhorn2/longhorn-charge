package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务端回复 诊断状态通知
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class DiagnosticsStatusNotificationResponse implements Serializable {
    private static final long serialVersionUID = 1L;


}
