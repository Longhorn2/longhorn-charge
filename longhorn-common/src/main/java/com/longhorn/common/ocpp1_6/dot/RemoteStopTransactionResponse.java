package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

@Data
public class RemoteStopTransactionResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * "Accepted",
     * "Rejected"
     *
     * */
    private String status;
}
