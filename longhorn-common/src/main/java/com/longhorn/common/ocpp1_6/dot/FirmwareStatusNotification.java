package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩上报 固件状态通知
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class FirmwareStatusNotification implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *"Downloaded",  已下载
     *"DownloadFailed", 下载失败
     * Downloading 下载
     * Idle 空闲
     * InstallationFailed  安装失败
     * Installing  正在安装
     * Installed 已安装
     * */
    private String status;
}
