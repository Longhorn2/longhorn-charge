package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩服务端授权回应
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class AuthorizeResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 授权实体类
     * */
    public IdTagInfo idTagInfo;

}
