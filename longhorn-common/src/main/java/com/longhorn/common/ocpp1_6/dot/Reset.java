package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 服务器发送 重置
 * @author gmding
 * @data 2023.04.24
 * */
@Data
public class Reset implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 重置类型
     *"Hard",  硬件
     *"Soft", 软件
     * */
    private String type;

}
