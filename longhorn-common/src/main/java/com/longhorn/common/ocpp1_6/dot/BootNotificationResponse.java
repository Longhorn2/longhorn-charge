package com.longhorn.common.ocpp1_6.dot;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩启动通知 应答
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class BootNotificationResponse implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",  接受
     *"Pending",  代办
     *"Rejected", 拒绝
     *
     * */
    private String status;
    /**
     * 当前时间
     * */
    private String currentTime;
    /**
     * 间隔
     * 当 RegistrationStatus 为 Accepted 时，这包含以秒为单位的心跳间隔。
     * 如果中央系统返回的不是 Accepted，则间隔字段的值表示在发送下一个 BootNotification 请求之前的最短等待时间
     * */
    private Integer interval;


}
