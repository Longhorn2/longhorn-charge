package com.longhorn.common.ocpp1_6.dot;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 充电桩回复 服务器下发的数据传输请求
 * @author gmding
 * @data 2023.04.21
 * */
@Data
public class DataTransferResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *"Accepted",  接受
     *"Rejected", 拒绝
     * "UnknownMessageId" 未知消息 id
     * "UnknownVendorId"  未知供应商id
     * */
    private String status;

    /**
     *
     *数据
     * */
    private String data;

}
