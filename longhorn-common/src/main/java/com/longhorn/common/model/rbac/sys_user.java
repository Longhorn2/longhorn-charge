package com.longhorn.common.model.rbac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 用户信息表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class sys_user implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    //@TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    private String username;

    private String pwd;

    private Timestamp register_time;

    private Timestamp update_time;

    /**
     * 账户状态
     * 0=禁用
     * 1=正常
     * */
    private int status;
    /**
     * 头像
     * 0=女
     * 1=男
     * */
    private Integer sex;
    /**
     * 头像
     * */
    private String avatar;
}
