package com.longhorn.common.model.rbac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统用户角色表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class sys_user_role implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 权限id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long role_id;
}
