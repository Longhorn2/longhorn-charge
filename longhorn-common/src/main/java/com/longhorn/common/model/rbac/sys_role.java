package com.longhorn.common.model.rbac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 角色表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class sys_role implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id 角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    private String role_name;

    /**
     * 角色状态 0=禁用 1=启用
     * */
    private Integer status;

    private Timestamp create_time;

    private Timestamp update_time;

}
