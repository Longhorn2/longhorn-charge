package com.longhorn.common.model.rbac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统角色菜单表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class sys_role_menu implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 菜单 id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long menu_id;
    /**
     * 角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long role_id;

}
