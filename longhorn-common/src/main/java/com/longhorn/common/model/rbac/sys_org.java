package com.longhorn.common.model.rbac;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统组织表
 * @author gmding
 * @data 2023.04.28
 * */
@Data
public class sys_org implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 组织id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 组织名称
     */
    private String org_name;
    /**
     * 组织代码
     */
    private String org_code;
    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parent_id;
}
