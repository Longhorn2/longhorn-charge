package com.longhorn.common.business;


import com.longhorn.common.enums.BusinessTypeEnum;
import com.longhorn.common.enums.ErrorCodeEnum;

/**
 *  业务系统异常
 *  @author gmding
 *  @date 2023-04-20
 * */

public class BusinessException extends RuntimeException{
    /**
     * 状态码
     */
    private int code;
    /**
     * 错误信息
     */
    private String errMsg;

    public BusinessException(BusinessTypeEnum errorCode){
        this.code = errorCode.getCode();
        this.errMsg = errorCode.getErrMsg();
    }
    public BusinessException(ErrorCodeEnum errorCode){
        this.code = errorCode.getCode();
        this.errMsg = errorCode.getErrMsg();
    }

    public BusinessException(int code, String errMsg) {
        this.errMsg = errMsg;
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

}
