package com.longhorn.common.business;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class WebSocketSessionVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String sessionId;


    private Integer status;

    private String pileCode;

    private String defaultExchange;

    private Timestamp times;

    private List<Object> data;

}
