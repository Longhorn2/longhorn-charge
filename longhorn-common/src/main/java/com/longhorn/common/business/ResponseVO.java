package com.longhorn.common.business;


import com.longhorn.common.enums.ErrorCodeENEnum;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import lombok.Data;

import java.io.Serializable;

/***
 *
 * 全局json报文返回体
 *  @author gmding
 *  @date 2023-4-20
 */

@Data
public class ResponseVO implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     *状态码
     */
    public int code;
    /**
     * 返回信息
     * */
    public String errMsg;
    /**
     * 返回数据
     * */
    public Object data;




    public ResponseVO(StatusCodeEnum statusCodeEnum, Object data) {
        this.code = statusCodeEnum.getCode();
        this.errMsg = statusCodeEnum.getErrMsg();
        this.data = data;
    }

    public ResponseVO(ErrorCodeEnum errorCodeEnum, Object data) {
        this.code = errorCodeEnum.getCode();
        this.errMsg = errorCodeEnum.getErrMsg();
        this.data = data;
    }
    public ResponseVO(ErrorCodeEnum errorCodeEnum, Object data,String language) {
        this.code = errorCodeEnum.getCode();
        this.errMsg = errorCodeEnum.getErrMsg();
        if (language.equals("EN")){//英文
            this.errMsg=ErrorCodeENEnum.getEnumCode(errorCodeEnum.getCode()).getErrMsg();
        }

        this.data = data;
    }
    public ResponseVO(StatusCodeEnum statusCodeEnum, Object data, String language) {
        this.code = statusCodeEnum.getCode();
        this.errMsg = statusCodeEnum.getErrMsg();
        if (language.equals("EN")){//英文
            this.errMsg=ErrorCodeENEnum.getEnumCode(statusCodeEnum.getCode()).getErrMsg();
        }
        this.data = data;
    }

    public ResponseVO(){
        super();
    }

    public ResponseVO(int code, String errMsg, Object data) {
        this.code=code;
        this.errMsg=errMsg;
        this.data=data;
    }


}

