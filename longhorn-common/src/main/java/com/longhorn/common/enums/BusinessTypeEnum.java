package com.longhorn.common.enums;

/**
 *
 * 业务返回状态码
 * */
public enum BusinessTypeEnum {
    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"未授权认证")
    ;
    private int code;

    private String errMsg;

    BusinessTypeEnum(int code,  String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }



}
