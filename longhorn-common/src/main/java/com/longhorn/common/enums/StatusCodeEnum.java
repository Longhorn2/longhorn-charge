package com.longhorn.common.enums;

/**
 * 状态码描述管理
 * @author gmding
 * @date 2023-04-20
 * */

public enum StatusCodeEnum {

    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"未授权认证"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "未找到该资源"),
    //异地登录
    OFFSITELOGIN(1002,"token过期，重新刷新"),
    //用户不存在
    USER_NOT_FOUND(1003,"用户名不存在或者已被禁用"),
    //密码错误
    USERNAMEORPASSWORD_INPUT_ERROR(1004,"密码或者用户名输入错误"),
    TOKEN_NOT_FOUND(1005,"token未发现"),
    ;

    private int code;

    private String errMsg;

    StatusCodeEnum(int code, String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }

    public int setCode(int code) {
        this.code=code;
        return this.code;
    }
    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}