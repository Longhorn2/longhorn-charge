package com.longhorn.common.enums;

/**
 * 状态码描述管理
 * @author gmding
 * @date 2023-04-20
 * */

public enum ErrorCodeEnum{

    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"未授权认证"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "未找到该资源"),
    //异地登录
    OFFSITELOGIN(1002,"token过期，重新刷新"),
    //用户不存在
    USER_NOT_FOUND(1003,"用户名不存在或者已被禁用"),
    //密码错误
    USERNAMEORPASSWORD_INPUT_ERROR(1004,"密码或者用户名输入错误"),
    TOKEN_NOT_FOUND(1005,"token未发现"),
    EMAIL_Valid(1006,"邮箱不合法"),
    PWD_Valid(1007,"密码长度6~15位"),
    EMAIL_REG(1008,"邮箱已被注册过"),
    PWD_ERROR(1009,"密码已更改"),
    PASSWORD_CANNOT_IDENTICAL(1010,"修改密码不能与原密码相同"),
    IMAGE_EMPTY_ERROR(1011,"图片不能为空"),
    IMAGE_SIZE_ERROR(1012,"照片大小超出限制"),
    USER_NOT_LOGOFF(1013,"运维用户无法注销"),
    EMAIL_ERROR(1014,"邮件发送失败"),
    FEEDBACK_ERROR(1016,"反馈内容不能为空"),
    EMAIL_CODE_ERROR(1015,"验证码错误或邮箱未注册"),
    EMAIL_IS_USING(1017,"邮箱已被使用"),
    EMAIL_BELONG_OTHER(1018,"这不是你的邮箱"),
    USER_NOT_ROLE(1019,"没有权限"),
    USER_IS_MEMBER(1020,"用户已被您关联"),
    PILE_NOT_EXIST(1021,"该桩不存在"),
    PILE_HAS_BOUND(1022,"此桩已有桩主"),
    PILE_OFFLINE(1023,"桩处于离线状态，请联系管理员"),
    USER_NOT_MEMBER(1024,"您不是该桩成员"),
    PILE_NOT_INFORMATION(1025,"没有该桩的线上信息"),
    ORG_EXIST(1026,"该组织已存在或已被其他用户创建"),
    PILE_EXIST_BOUND(1027,"此桩已被添加"),
    STATION_EXIST(1028,"站点已被配置"),
    ;


    private int code;

    private String errMsg;

    ErrorCodeEnum(int code,  String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}