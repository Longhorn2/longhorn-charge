package com.longhorn.common.enums;

/**
 * 状态码描述管理  英语
 * @author gmding
 * @date 2023-04-20
 * */

public enum ErrorCodeENEnum {

    //成功
    SUCCESS(0,"success"),
    //未授权认证
    UNAUTHORIZED(401,"Unauthorized authentication"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "The resource was not found"),
    //异地登录
    OFFSITELOGIN(1002,"If the token expires, refresh it again"),
    //用户不存在
    USER_NOT_FOUND(1003,"The user name does not exist or has been disabled"),
    //密码错误
    USERNAMEORPASSWORD_INPUT_ERROR(1004,"The password or user name is incorrect"),
    TOKEN_NOT_FOUND(1005,"token not found"),
    EMAIL_Valid(1006,"Invalid mailbox"),
    PWD_Valid(1007,"The password contains 6 to 15 characters"),
    EMAIL_REG(1008,"The email has been registered"),
    PWD_ERROR(1009,"Password changed"),
    PASSWORD_CANNOT_IDENTICAL(1010,"The new password cannot be the same as the old password"),
    IMAGE_EMPTY_ERROR(1011,"The picture cannot be empty"),
    IMAGE_SIZE_ERROR(1012,"Photo size out of limit"),
    USER_NOT_LOGOFF(1013,"The O&M user cannot be logged out"),
    EMAIL_ERROR(1014,"Mail sending failure"),
    FEEDBACK_ERROR(1016,"The feedback content cannot be empty"),
    EMAIL_CODE_ERROR(1015,"The verification code is incorrect or the email address is not registered"),
    EMAIL_IS_USING(1017,"The mailbox has been used"),
    EMAIL_BELONG_OTHER(1018,"This isn't your email"),
    USER_NOT_ROLE(1019,"Have no authority"),
    USER_IS_MEMBER(1020,"The user has been associated with you"),
    PILE_NOT_EXIST(1021,"The stake does not exist"),
    PILE_HAS_BOUND(1022,"This pile has a pile owner"),
    PILE_OFFLINE(1023,"The pile is offline. Please contact the administrator"),
    USER_NOT_MEMBER(1024,"You are not a member of the pile"),
    PILE_NOT_INFORMATION(1025,"No online information for the stake"),
    ORG_EXIST(1026,"The organization already exists or has been created by another user"),
    PILE_EXIST_BOUND(1027,"This pile has been added"),
    STATION_EXIST(1028,"The site has been configured"),
    ;


    private int code;

    private String errMsg;

    ErrorCodeENEnum(int code, String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }


    /**
     * 根据code得到枚举的Value
     * 增强for循环遍历，比较判断
     *
     * @param code
     * @return
     */
    public static ErrorCodeENEnum getEnumCode(int code) {
        ErrorCodeENEnum[] errorCodeENEnums = ErrorCodeENEnum.values();
        for (ErrorCodeENEnum errorCodeENEnum : errorCodeENEnums) {
            if (errorCodeENEnum.getCode()==code) {
                return errorCodeENEnum;
            }
        }
        return ErrorCodeENEnum.RESOURCE_NOT_FOUND;
    }


    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}