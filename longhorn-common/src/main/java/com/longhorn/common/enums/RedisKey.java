package com.longhorn.common.enums;

/**
 * @author ddw (1912627448@qq.com)
 * @since 2022/8/5
 */
public enum RedisKey {
    //消息记录的REDIS key
    COMMAND_RECORD_DATA("Commond:record:"),
    EXPIRE_COMMAND_RECORD_DATA("expire:Commond:record:"),
    ;
    private String key;

    RedisKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getErrMsg() {
        return key;
    }
    @Override
    public String toString() {
        return "RedisKey{" +
                "key='" + key + '\'' +
                '}';
    }

}
