package com.longhorn.common.utils.tools;


import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Key;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * @author gmding
 * @Description  常用的工具类方法 加密解密  常量定义
 * @data 2023.04.21
 *
 * */

public class DesUtils {



    private static final String KEY = "Longhorn123456";

    /**
     * access_token有效期 2小时
     */
    public final int EXPIRESIN=2*3600;
    /**
     * refresh_token有效期 30天
     */

    public final int REFRESHEXPIRES=30*24*3600;


    /**
     * @Description 获取uuid
     *
     */
    public String getUuid(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }



    /**
     * @Description 根据密钥对指定的明文进行加密.
     *
     * @apiParam plainText 明文
     * @return 加密后的密文.
     */
    public static final String encrypt(String plainText) {
        Key secretKey = getKey(KEY);
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] p = plainText.getBytes("UTF-8");
            byte[] result = cipher.doFinal(p);
            //BASE64Encoder encoder = new BASE64Encoder();
            String encoded = Base64.encodeBase64String(result);
            return encoded;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    /**
     * @Description 根据密钥对指定的密文cipherText进行解密.
     *
     * @apiParam cipherText 密文
     * @return 解密后的明文.
     */
    public static final String decrypt(String cipherText) {
        Key secretKey = getKey(KEY);
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            //BASE64Decoder decoder = new BASE64Decoder();
            //byte[] c = decoder.decodeBuffer(cipherText);
            byte[] c = Base64.decodeBase64(cipherText);

            byte[] result = cipher.doFinal(c);
            String plainText = new String(result, "UTF-8");
            return plainText;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Key getKey(String keySeed) {
        if (keySeed == null) {
            keySeed = System.getenv("AES_SYS_KEY");
        }
        if (keySeed == null) {
            keySeed = System.getProperty("AES_SYS_KEY");
        }
        if (keySeed == null || keySeed.trim().length() == 0) {
            // 默认种子
            keySeed = "abcd1234!@#$";
        }
        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(keySeed.getBytes());
            KeyGenerator generator = KeyGenerator.getInstance("AES");
            generator.init(secureRandom);
            return generator.generateKey();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {

        String ec= (String) encrypt("admin");
        System.out.println(ec);


        String dec= (String) decrypt(ec);
        System.out.println(dec);


    }

}
