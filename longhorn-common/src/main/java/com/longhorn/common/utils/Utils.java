package com.longhorn.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * 工具类
 * @author gmding
 * @Description  常用的工具类方法
 * @data 2023.04.24
 * */
public class Utils {
    private static int randomData;

    /**
     * UUID获取
     *
     * */
    public static String getUuid(){
        return UUID.randomUUID().toString();
    }

    /**
     * 消息id生成器
     * */
    public static String getTag(){
        return getUuid();
    }
    /**
     *获取当前时间
     *@return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss'Z'");
        String dateString = formatter.format(currentTime);
        return dateString;
    }
    /**
     * 获取当前时间戳
     * @return long
     *
     * */
    public static long getTimestamp(){
        return  System.currentTimeMillis();
    }

    /**
     * 邮箱格式
     * */
    public static boolean isValidEmail(String email) {
        if ((email != null) && (!email.isEmpty())) {
            return Pattern.matches("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$", email);
        }
        return false;
    }

    /**
     * 邮箱格式
     * */
    public static boolean isValidPwd(String pwd) {
        if ((pwd != null) && (!pwd.isEmpty())) {
            if (pwd.length()>=6&&pwd.length()<=15){
                 return true;
            }
        }
        return false;
    }

    /**
     * 获取年月日时分秒
     * */

    public static Long getLongDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmsss");
        String dateString = formatter.format(currentTime);
        return Long.valueOf(dateString);
    }


    /**
     * 订单生成规则
     * */
    public synchronized static long getOrderId(){
        //Long orderId = new Date().getTime();
        Long orderId = getLongDate();
        Random random = new Random();
        int i =random.nextInt(10000);
        if (i==randomData){
            return getOrderId();
        }
        randomData=i;
        return orderId*10000+i;
    }


    public static void main(String[] args) {

        System.out.println(getLongDate());
        System.out.println(getOrderId());
    }

}
