package com.longhorn.common.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StringUtils {

    public static List<Object> getStrToArrary(String str){
        Type type = new TypeToken<ArrayList<Object>>(){}.getType();

        List<Object> list=new Gson().fromJson(str,type);
        return list;
    }
}
