package com.longhorn.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.utils.DateUtil;
import com.longhorn.model.utils.UrlUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.io.Serializable;

import java.sql.Timestamp;

/**
 * 用户信息表
 * @author gmding
 * @data 2023.04.26
 * */
@Component
@Data
public class SysUser implements Serializable {
    private static final long serialVersionUID = 1L;
    UrlUtil urlUtil;
    /**
     * 主键id 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    //@TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    private String user_name;

    private String pwd;

    private Timestamp register_time;

    private Timestamp update_time;

    /**
     * 账户状态
     * 0=禁用
     * 1=正常
     * 2=注销
     * */
    private Integer status;
    /**
     * 性别
     * 0=女
     * 1=男
     * */
    private Integer sex;
    /**
     * 头像
     * */
    private String avatar;

//    public String getAvatar(String avatar){
//        return this.avatar= SpringUtil.getBean(UrlUtil.class).getUrl() +"downloadFileByName?bucketName=avatar&fileName="+avatar;
//    }
    public  static String minioUrl;
    @Value("${minioUrl}")
    public void setMinioUrl(String minioUrl) {
        SysUser.minioUrl = minioUrl;
    }
    public void setAvatar(String avatar){
        this.avatar=minioUrl +"downloadFileByName?bucketName=avatar&fileName="+avatar;
    }


    /**
     * 邮箱地址
     * */
    private String email;
    /**
     * 账号注册来源  Android，iOS，web ，app
     * */
    private String source;
    /**
     * 用户类型  1 运维 2 普通用户(个人) 3.(10之后为客户账号类型，10之前为系统账号) 10 客户管理账号
     * */
    private Integer type;
    /**
     * 客户账号特有，客户类型
     */
    private String customer_type;
    /**
     * 用户生日
     */
    @DateTimeFormat(pattern = DateUtil.DATE_PATTERN)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String birthday;
    /**
     * 国家
     */
    private String country;
    /**
     * 省份
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 区、县
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 电话
     */
    private String phone;
    /**
     * 员工编号
     */
    private String code;
    /**
     * 部门
     */
    private String department;
    /**
     * 联系人
     */
    private  String contact_person;
    /**
     * 联系电话
     */
    private String contact_phone;
    /**
     * 主体性质
     */
    private String subject_nature;
    /**
     * 号码区号
     */
    private String contact_phone_code;
    /**
     * 旧密码
     */
    private String oldPwd;
    /**
     * 注销时反馈的问题
     */
    private MsgCenter msgCenter;

}
