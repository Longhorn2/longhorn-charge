package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class StationManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id，站点id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 站点名称
     */
    private String name;
    /**
     * 运营商
     */
    private String operator;
    /**
     * 品牌商
     */
    private String brand_owner;
    /**
     * 代理商
     */
    private String agent;
    /**
     * 服务商
     */
    private String service_provider;
    /**
     * 站点状态  对应sys_data_type数据字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long site_status;
    /**
     * 建设场所 对应sys_data_type数据字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long construction_site;
    /**
     * 运营时间
     */
    private Timestamp first_operation_time;
    /**
     * 营业时间，json字符串
     */
    private String business_hours;
    /**
     * 联系人
     */
    private String contact_person;
    /**
     * 联系电话
     */
    private String contact_phone;
    /**
     * 站点描述
     */
    private String station_descript;
    /**
     * 国家
     */
    private String country;
    /**
     * 省份
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 经度
     */
    private String longitude;
    /**
     * 纬度
     */
    private String latitude;
    /**
     * 操作人id
     */
    private Long user_id;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;
    /**
     * 站点编号
     */
    private String station_code;
    /**
     * 门牌号
     */
    private String house_number;
    /**
     * 邮编
     */
    private String zip_code;
    /**
     * 站点关联图片
     */
    private List<String> imgList;
    /**
     * 站点关联桩
     */
    private List<PileManagement> pileManagements;
    /**
     * 站点特色
     */
    private SysDataType characteristic;
    /**
     * 充电口类型
     */
    private SysDataType chargeType;
    /**
     * 充电站离当前位置距离
     */
    private BigDecimal distance;


}
