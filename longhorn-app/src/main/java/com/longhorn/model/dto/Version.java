package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class Version implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 版本类型
     * 1=app
     * 2=android
     * 3=ios
     * 4=固件(嵌入式)
     */
    private Integer type;
    /**
     * 版本号
     */
    private String version;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 是否有效
     * 1=有效
     * 2=无效
     */
    private Integer is_valid;
    /**
     * 更新类别
     * 1=强制更新
     * 2=提醒更新
     */
    private Integer update_type;
    /**
     * 更新时间
     */
    private Timestamp update_time;
}
