package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class HelpCenterType implements Serializable {
    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 图标地址
     */
    private String icon;
    /**
     * 是否展示：1=是，0=否
     */
    private Integer id_dis_play;
    /**
     * 数据字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sys_data_type_id;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;
    /**
     * 操作人id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 类型  1=app帮助中心   2=app运维知识库
     */
    private Integer type;
    /**
     * 帮助中心类实例化
     */
    private HelpCenter helpCenter;
    /**
     *
     */
}
