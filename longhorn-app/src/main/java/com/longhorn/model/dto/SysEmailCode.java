package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SysEmailCode implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 6位数验证码
     */
    private Integer code;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 状态 0=未使用，1=已使用
     */
    private Integer status;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 更改的密码
     */
    private String pwd;
    /**
     * 新更换的邮箱
     */
    private String newEmail;
}
