package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SysDataType implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 字典名称
     */
    private String name;
    /**
     * 字典编码
     */
    private String code;
    /**
     * 备注
     */
    private String remark;
    /**
     * 状态：1=有效，2=无效
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 创建人、用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 更新时间
     */
    private Timestamp update_time;
    /**
     * 语言
     */
    private String language;
    /**
     * 排序
     */
    private Integer sort;
}
