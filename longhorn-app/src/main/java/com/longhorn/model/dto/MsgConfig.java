package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class MsgConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 消息类型
     */
    private String type;
    /**
     * 短信消息
     */
    private String msg;
    /**
     * app_msg
     */
    private String app_msg;
    /**
     * 消息说明
     */
    private String descript;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;
}
