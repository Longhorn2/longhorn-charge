package com.longhorn.model.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class OCPP implements Serializable {
    /**
     * 桩编号
     */
    private String pileCode;
    /**
     * ocpp设置的url
     */
    private String url;
    /**
     * ocpp的url连接密码
     */
    private String pwd;
}
