package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class PileManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id，充电桩id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 充电桩名
     */
    private String name;
    /**
     * 站点管理id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long station_management_id;
    /**
     * 协议管理表id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long protocol_management_id;
    /**
     * 设备编号
     */
    private String pile_code;
    /**
     * 交直模式
     */
    private  String mode;
    /**
     * 设备型号
     */
    private String model;
    /**
     * 状态
     * 1=未安装；2=安装中；3=使用中；4=维护/维修；5=报废
     */
    private Integer status;
    /**
     * 枪数
     */
    private Integer gun_num;
    /**
     * 最大功率KW
     */
    private Integer max_power;
    /**
     * 资产归属方
     */
    private String vesting_party;
    /**
     * 国标类型
     */
    private String standard_type;
    /**
     * 充电口类型
     */
    private String charge_type;
    /**
     * 生产日期
     */
    private Timestamp production_date;
    /**
     * 质保日期
     */
    private String warranty_date;
    /**
     * 操作人id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;
    /**
     * 软件当前版本
     */
    private String version;
    /**
     * 站点名
     */
    private String stationName;
    /**
     * 在该桩的身份:0=桩主，1=桩成员
     */
    private Integer master;
    /**
     * 所属的站点信息
     */
    private StationManagement stationManagement;

}
