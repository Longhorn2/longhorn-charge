package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
@Data
public class StationImg implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 站点管理id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;
    /**
     * 站点管理图片
     * */
    private String img;

    public  static String minioUrl;
    @Value("${minioUrl}")
    public void setMinioUrl(String minioUrl) {
        SysUser.minioUrl = minioUrl;
    }
    public void setImg(String img){
        this.img=minioUrl +"downloadFileByName?bucketName=stationImg&fileName="+img;
    }
}
