package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class HelpCenter implements Serializable {
    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 是否显示：1=是，0=否
     */
    private Integer is_display;
    /**
     * 分类id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long help_center_type_id;
    /**
     * 数据字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sys_data_type_id;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;
    /**
     * 操作人id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 类型：类型
     * 1=app帮助中心
     * 2=app运维知识库
     * 3=CMS平台帮助中心—分类管理
     * 4=CMS平台帮助中心—常见问题
     * 5=CMS平台帮助中心—使用指南
     */
    private Integer type;
}
