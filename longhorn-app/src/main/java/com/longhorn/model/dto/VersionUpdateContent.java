package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class VersionUpdateContent implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id，版本更新内容id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 更新内容
     */
    private String update_content;
    /**
     * 语言类型，数据字典id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sys_data_type_id;
    /**
     * 版本id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long version_id;
}
