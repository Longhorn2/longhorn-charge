package com.longhorn.model.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class Wifi implements Serializable {
    /**
     * 桩编号
     */
    private String pileCode;
    /**
     *WiFi名
     */
    private String wifiName;
    /**
     *WiFi密码
     */
    private String wifiPwd;
}
