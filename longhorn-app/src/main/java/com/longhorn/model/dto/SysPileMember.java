package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SysPileMember implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 操作人id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 是否主账号 0=是，1=否
     */
    private Integer master;
    /**
     * 充电桩管理id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pile_management_id;
    /**
     * 创建时间
     */
    private Timestamp create_time;
    /**
     * 修改时间
     */
    private Timestamp update_time;

}
