package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class MsgCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 反馈的问题
     */
    private String problem;
    /**
     * 反馈的系统通知
     */
    private String content;
    /**
     * 平台回复
     */
    private String reply;
    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 类型：1=我的反馈 2=系统消息
     */
    private Integer type;
    /**
     * 标题
     */
    private String title;
    /**
     * 问题反馈时间
     */
    private Timestamp create_time;
    /**
     * 回答时间？更新时间
     */
    private Timestamp update_time;
    /**
     * 标记状态   0=未读  1=已读 2=已清除
     */
    private Integer status;
}
