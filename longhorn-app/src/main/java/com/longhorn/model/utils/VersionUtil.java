package com.longhorn.model.utils;

public final class VersionUtil {
    //比较两版本号大小，若大则返回true,否则返回false
    public static Boolean equalVersion(String myVersion,String baseVersion){
        String[] str1=myVersion.split("\\.");
        String[] str2=baseVersion.split("\\.");
        for (int i=0;i<str1.length;i++){
            if (Integer.parseInt(str1[i])<Integer.parseInt(str2[i])){
                return true;
            }
            if (Integer.parseInt(str1[i])>Integer.parseInt(str2[i])){
                return false;
            }
        }
        return false;
    }
}
