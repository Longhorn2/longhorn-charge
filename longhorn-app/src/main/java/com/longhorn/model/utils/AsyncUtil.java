package com.longhorn.model.utils;

import com.google.gson.Gson;
import com.longhorn.client.ImClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.dto.mapper.PileManagementMapper;
import com.longhorn.model.vo.MsgCenterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class AsyncUtil {
    @Autowired
    ImClient imClient;
    @Autowired
    PileManagementMapper pileManagementMapper;
    @Async("AsyncExecutor")
    public void addMegCenter(Long stationId, String code, MsgCenterVo msgCenterVo) throws BusinessException {
        imClient.saveUserId(stationId, code);
        List<Long> list = new Gson().fromJson(imClient.getKey("pile::station::userId::" + code).getData().toString(), List.class);
        msgCenterVo.setUserIds(list);
        //调用本逻辑类的添加Msg方法开始向系统通知表及其读取状态表中插入数据

        pileManagementMapper.addMsgCenter(msgCenterVo);
        pileManagementMapper.addUserMsg(msgCenterVo);
    }
}
