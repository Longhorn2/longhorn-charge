package com.longhorn.model.utils;

import lombok.Data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class UrlUtil {
    @Value("${minioUrl}")
    public  String url;
}
