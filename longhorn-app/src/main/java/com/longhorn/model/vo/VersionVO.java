package com.longhorn.model.vo;

import com.longhorn.model.dto.Version;
import lombok.Data;

import java.io.Serializable;
@Data
public class VersionVO extends Version implements Serializable {
    /**
     * 更新内容
     */
    private String update_content;
    /**
     * 是否强制更新
     */
    private Boolean flag=false;
}
