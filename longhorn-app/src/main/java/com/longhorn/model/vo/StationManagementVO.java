package com.longhorn.model.vo;

import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.dto.SysDataType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StationManagementVO extends StationManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 开始页码
     */
    private Integer pageStart;
    /**
     * 页面容量
     */
    private Integer pageSize;
    /**
     * 数据总条数
     */
    private Integer total;

    private List<StationManagement> stationManagements;

}
