package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.common.ocpp1_6.dot.StatusNotification;
import lombok.Data;

import java.io.Serializable;
@Data
public class StatusNotificationVO extends StatusNotification implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 新增枪当前订单号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;
}
