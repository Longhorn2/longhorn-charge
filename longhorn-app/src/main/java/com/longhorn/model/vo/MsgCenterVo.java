package com.longhorn.model.vo;

import com.longhorn.model.dto.MsgCenter;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MsgCenterVo extends MsgCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pageStart;
    private Integer pageSize;
    private Integer total;
    private Integer unreadNum;
    private List<Long> userIds;
    private List<MsgCenter> msgCenterList;
}
