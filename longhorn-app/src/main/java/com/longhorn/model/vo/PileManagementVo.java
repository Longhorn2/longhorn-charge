package com.longhorn.model.vo;

import com.longhorn.model.dto.PileManagement;
import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class PileManagementVo extends PileManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 开始页码
     */
    private Integer pageStart;
    /**
     * 页面容量
     */
    private Integer pageSize;
    /**
     * 查询总数
     */
    private Integer total;
    /**
     * 页面内容
     */
    private List<PileManagement> pileManagementList;
}
