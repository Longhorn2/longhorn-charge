package com.longhorn.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class ChargingTrendVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 时间
     */
    private Timestamp days;
    /**
     * 当日充电次数
     */
    private Integer chargeNum;
    /**
     * 当日充电量
     */
    private BigDecimal capacitySum;
    /**
     * 当日充电时常
     */
    private BigDecimal chargeTimeSum;
    /**
     * 充电桩编号
     */
    private String pileCode;
}
