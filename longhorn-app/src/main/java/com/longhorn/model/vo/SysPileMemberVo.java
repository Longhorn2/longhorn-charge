package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysPileMember;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class SysPileMemberVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 操作人id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long user_id;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 是否主账号 0=是，1=否
     */
    private Integer master;
    /**
     * 充电桩管理id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pile_management_id;
    /**
     * 开始页码
     */
    private Integer pageStart;
    /**
     * 页面个数
     */
    private Integer pageSize;
    /**
     * 所查总数
     */
    private Integer total;
    /**
     * 当页数据
     */
    private List<SysPileMember> sysPileMemberList;

}
