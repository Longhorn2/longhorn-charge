package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysChargeOrder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class SysChargeOrderVo extends SysChargeOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 在这个桩的身份0=桩主  1=子账号
     */
    private Integer master;
    /**
     * 桩id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pileId;
    /**
     * 当月充电次数
     */
    private Integer chargeCount;
    /**
     * 当月充电总量
     */
    private BigDecimal capacitySum;
    /**
     * 开始页码
     */
    private Integer pageStart;
    /**
     * 页面个数
     */
    private Integer pageSize;
    /**
     * 查询总数
     */
    private Integer total;
    /**
     * 实体数据
     */
    List<SysChargeOrder> sysChargeOrderList;
}
