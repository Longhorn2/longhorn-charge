package com.longhorn.model.vo;

import com.longhorn.common.ocpp1_6.dot.StatusNotification;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


@Data
public class ChargingDataVO implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 充电桩状态 默认0=在线 1=离线
     */
    private Integer status;
    /**
     * 充电度数
     */
    private BigDecimal capacity;
    /**
     * 充电时长
     */
    private BigDecimal chargeTime;
    /**
     * 充电时间
     */
    private String startTime;

    /**
     * 枪数量
     */
    private Integer gunNum;
    /**
     * 枪状态集合
     */
    private List<StatusNotificationVO> gunStatus;

}
