package com.longhorn.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * 过滤路径
 * @author gmding
 * @date 2021-7-16
 * */

public class BasePathPatterns {


    /**
     * 过滤拦截器路径,适合正则
     * @apiParam string
     *
     * */
    public final static String[] URL = {
            "/api/v1/longhorn-app/swagger-resources(.*)",
            "/api/v1/longhorn-app/error",
            "/api/v1/longhorn-app/sysUser/addUser",
            "/api/v1/longhorn-app/sysUser/login",
            "/api/v1/longhorn-app/sysUser/cronFind",
            "/api/v1/longhorn-app/sysUser/updatePwdByEmail",
            "/api/v1/longhorn-app/sysUser/getEmailCode",
            "/api/v1/longhorn-app/version/queryAppVersion"
            //"/api/v1/longhorn-app/(.*)"

    };

}
