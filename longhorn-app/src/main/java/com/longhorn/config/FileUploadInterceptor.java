package com.longhorn.config;


import com.longhorn.common.business.BusinessException;
import com.longhorn.common.enums.ErrorCodeEnum;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FileUploadInterceptor implements HandlerInterceptor {
    /**
     * 设置最大图片的大小
     */
    private final long AVATAR_MAX_SIZE = 30 * 1024 * 1024;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (null != request && ServletFileUpload.isMultipartContent(request)) {
            long requestSize = new ServletRequestContext(request).contentLength();
            if (requestSize > AVATAR_MAX_SIZE) {
                throw new BusinessException(ErrorCodeEnum.IMAGE_SIZE_ERROR);
            }
        }
        return true;
    }
}
