package com.longhorn.controller.sys;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.HelpCenterImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "帮助中心")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/helpCenter")
public class HelpCenterController {
    @Resource
    private HelpCenterImpl pileOwnerServiceImpl;
    @ApiOperation(value = "进入帮助中心")
    @RequestMapping(value = "/findHelpType",method = RequestMethod.GET)
    public ResponseVO findHelpType(@RequestParam("userId") Long userId,HttpServletRequest request) throws BusinessException {
        return pileOwnerServiceImpl.findHelpType(userId,request);
    }
    @ApiOperation(value = "查看分类下的标题内容")
    @RequestMapping(value="/findTitle",method = RequestMethod.POST)
    public ResponseVO findTitle(@RequestParam("typeId")Long typeId,@RequestParam("userId")Long userId,HttpServletRequest request) throws BusinessException{
        return pileOwnerServiceImpl.findTitle(typeId,userId,request);
    }
    @ApiOperation(value = "点击标题查询问题内容")
    @RequestMapping(value = "/findAnswer",method = RequestMethod.POST)
    public ResponseVO findAnswer(Long id,HttpServletRequest request) throws BusinessException{
        return pileOwnerServiceImpl.findAnswer(id,request);
    }


}
