package com.longhorn.controller.sys;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.service.StationManagementService;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.vo.StationManagementVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "运维站点信息管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/stationManagement")
public class StationManagementController {
    @Autowired
    StationManagementService stationManagementImpl;
    @ApiOperation(value = "运维站点信息查询")
    @RequestMapping(value = "/queryStation",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO queryStation(@RequestBody StationManagementVO stationManagement, HttpServletRequest request){
        return stationManagementImpl.queryStation(stationManagement,request);
    }
    @ApiOperation(value = "点击查看站点详细信息")
    @RequestMapping(value = "/StationInformation",method = RequestMethod.GET)
    public ResponseVO StationInformation(@RequestParam("stationId") Long stationId, HttpServletRequest request){
        return stationManagementImpl.stationInformation(stationId,request);
    }
}
