package com.longhorn.controller.sys;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.business.WebSocketSessionVo;
import com.longhorn.config.mq.RabbitMQUtils;
import com.longhorn.dto.impl.PileManagementImpl;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.PileManagementVo;
import com.longhorn.model.vo.SysChargeOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "充电桩信息管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/pileManagement")
public class PileManagementController {
    @Autowired
    PileManagementImpl pileManagementImpl;
    @Autowired
    RabbitMQUtils rabbitMQUtils;

    @ApiOperation(value = "扫码后桩状态检验")
    @RequestMapping(value = "/checkBindCode", method = RequestMethod.GET)
    public ResponseVO checkBind(@RequestParam("code") String code, HttpServletRequest request) throws BusinessException {
        log.info("code:{}", code);
        return pileManagementImpl.checkBindCode(code, request);
    }

    @ApiOperation("验证通过后绑定，新设站点")
    @RequestMapping(value = "/binding/{code}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO binding(@PathVariable("code") String code, @RequestBody StationManagement stationManagement, HttpServletRequest request) throws BusinessException {
        log.info("id:{},stationManagement:{}", code, stationManagement);
        return pileManagementImpl.binding(code, stationManagement, request);
    }

    @ApiOperation(value = "扫码绑定,将桩添加到已有站点")
    @RequestMapping(value = "/bind/{code}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO bind(@PathVariable("code") String code, @RequestBody SysPileMember sysPileMember, HttpServletRequest request) throws BusinessException {
        log.info("id:{},user_id:{}", code, sysPileMember);
        return pileManagementImpl.bind(code, sysPileMember, request);
    }

    @ApiOperation(value = "解绑桩")
    @RequestMapping(value = "/clearPile", method = RequestMethod.GET)
    public ResponseVO clearPile(@RequestParam("pileCode") String pileCode, @RequestParam("userId") Long userId, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.clearPile(pileCode, userId, request);
    }

    @ApiOperation("充电桩信息查看")
    @RequestMapping(value = "/pileInformation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO pileInformation(@RequestBody PileManagementVo vo, HttpServletRequest request) throws BusinessException {
        log.info("vo:{}", vo);
        return pileManagementImpl.pileInformation(vo, request);
    }

    @ApiOperation("修改充电桩基础信息")
    @RequestMapping(value = "/updatePileInformation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updatePileInformation(@RequestBody PileManagement pileManagement, HttpServletRequest request) throws BusinessException {
        log.info("充电桩信息：pileManagement:{}", pileManagement);
        return pileManagementImpl.updatePileInformation(pileManagement, request);
    }

    @ApiOperation("即插即充开关")
    @RequestMapping(value = "/isPNP", method = RequestMethod.GET)
    public ResponseVO isPNP(@RequestParam("PNP") Boolean PNP, @RequestParam("pileCode") String pileCode, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.isPNP(PNP, pileCode, request);
    }
//    @ApiOperation("查看该充电桩历史订单，包括关联用户")
//    @RequestMapping(value = "/history",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO history(@RequestBody SysChargeOrderVo vo, HttpServletRequest request) throws BusinessException{
//        return pileManagementImpl.history(vo,request);
//    }

//    @ApiOperation("查看充电趋势")
//    @RequestMapping(value = "/chargingTrends",method = RequestMethod.GET)
//    public ResponseVO chargingTrends(@RequestParam("pileCode")String pileCode,@RequestParam("time")Integer time,HttpServletRequest request){
//        log.info("充电桩编号：pileCode:{},趋势周期：time:{}",pileCode,time);
//        return pileManagementImpl.chargingTrends(pileCode,time,request);
//    }


    @ApiOperation("充电手册下载")
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public ResponseVO download(@RequestParam("pileCode") String pileCode, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.download(pileCode, request);
    }

    //    @ApiOperation("添加关联亲友到该桩")
//    @RequestMapping(value = "/addMember",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO addMember(@RequestBody SysPileMember sysPileMember,HttpServletRequest request){
//        log.info("添加的成员信息：sysPileMember:{}",sysPileMember);
//        return pileManagementImpl.addMember(sysPileMember,request);
//    }
//    @ApiOperation("从该桩删除关联亲友")
//    @RequestMapping(value = "delMember",method = RequestMethod.GET)
//    public ResponseVO delMember(@RequestParam("userId") Long userId,@RequestParam("pileId") Long pileId,HttpServletRequest request){
//        return pileManagementImpl.delMember(userId,pileId,request);
//    }
    @ApiOperation("wifi設置")
    @RequestMapping(value = "/setWifi", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO setWifi(@RequestBody Wifi wifi, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.setWifi(wifi, request);
    }

    @ApiOperation("ocpp设置")
    @RequestMapping(value = "/setOcpp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO setOcpp(@RequestBody OCPP ocpp, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.setOcpp(ocpp, request);
    }

    @ApiOperation("时间校准")
    @RequestMapping(value = "/setTime", method = RequestMethod.GET)
    public ResponseVO setTime(@RequestParam("pileCode") String pileCode, HttpServletRequest request) throws BusinessException {
        return pileManagementImpl.setTime(pileCode, request);
    }

    @ApiOperation("初始化参数")
    @RequestMapping(value = "/initialization", method = RequestMethod.GET)
    public ResponseVO initialization(@RequestParam("pileCode") String pileCode, HttpServletRequest request) throws BusinessException {
        return null;
    }

    @ApiOperation("设置最大电流")
    public ResponseVO setMaxA(@RequestParam("pileCode") String pileCode, @RequestParam("A") Integer A, HttpServletRequest request) throws BusinessException {
        return null;
    }
}
