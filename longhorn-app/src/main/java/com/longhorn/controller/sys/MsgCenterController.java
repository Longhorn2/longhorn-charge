package com.longhorn.controller.sys;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;

import com.longhorn.dto.impl.MsgCenterImpl;
import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.vo.MsgCenterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "系统通知管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/msgCenter")
public class MsgCenterController {
    @Autowired
    MsgCenterImpl msgCenterImpl;
    @ApiOperation(value = "查看系统通知列表")
    @RequestMapping(value = "/showNotification",method = RequestMethod.POST)
    public ResponseVO showNotification(@RequestBody MsgCenterVo vo, HttpServletRequest request) throws BusinessException {
        return msgCenterImpl.showNotification(vo,request);
    }
    @ApiOperation(value = "查看系统通知")
    @RequestMapping(value = "/checkNotification",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO checkNotification(@RequestBody MsgCenter msgCenter, HttpServletRequest request) throws BusinessException{
        return msgCenterImpl.checkNotification(msgCenter,request);
    }
    @ApiOperation(value = "一键已读")
    @RequestMapping(value = "/readAll",method = RequestMethod.POST)
    public ResponseVO readAll(@RequestBody List<MsgCenter> msgCenterList, HttpServletRequest request) throws BusinessException{
        return msgCenterImpl.readAll(msgCenterList,request);
    }
}
