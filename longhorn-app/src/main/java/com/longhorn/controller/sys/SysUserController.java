package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysUserImpl;
import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.dto.MsgConfig;
import com.longhorn.model.dto.SysEmailCode;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.MsgCenterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "首页")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    private SysUserImpl sysUserImpl;


    @ApiOperation(value = "注册用户")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addUser(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException {
        log.info("注册用户:{}",new Gson().toJson(sysUser));
         return sysUserImpl.addUser(sysUser,request);

    }
    @ApiOperation(value = "查询")
    @GetMapping("/get")
    public ResponseVO get(@RequestBody SysUser sysUser, HttpServletRequest request) throws BusinessException {
        log.info("查询:{},语言:{}",new Gson().toJson(sysUser),request.getSession().getAttribute("language"));
        return sysUserImpl.queryById(sysUser,request);
    }
    @ApiOperation(value = "登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO login(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException {
        log.info("登录:{}",new Gson().toJson(sysUser));
        return sysUserImpl.login(sysUser,request);

    }
//
//    @ApiOperation(value = "修改用户")
//    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO updRole(@RequestBody SysUser data) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(data));
//        return sysUserService.updateRecord(data);
//    }
//
//    @ApiOperation(value = "是否禁用")
//    @GetMapping("/status")
//    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(id));
//        log.info("用户管理:{}",new Gson().toJson(status));
//        return sysUserService.status(id,status);
//    }
//
//    @ApiOperation(value = "分页查询")
//    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(dto));
//        return sysUserService.pageByName(dto.getP(),dto.getSize(),dto.getIndex());
//    }
//
//    @ApiOperation(value = "用户拥有的组织列表")
//    @RequestMapping(value = "/selfOrg/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO findSelfOrg(@PathVariable("userId") Long userId) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(userId));
//        return sysUserService.findSelfOrgIds(userId);
//    }
//
//    @ApiOperation(value = "用户权限——菜单列表")
//    @GetMapping("/selfMenu/{userId}")
//    public ResponseVO findSelfMenu(@PathVariable("userId") Long userId) throws BusinessException {
//        log.info("用户管理:{}",userId);
//        return sysUserService.findSelfMenu(userId);
//    }

//    @ApiOperation(value = "验证密码")
//    @RequestMapping(value = "/checkPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO checkPassword(@RequestBody SysUser sysUser) throws BusinessException{
//        log.info("验证密码:{}",new Gson().toJson(sysUser));
//        return sysUserImpl.checkPassword(sysUser);
//    }
    @ApiOperation(value = "修改密码")
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO changePassword(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException{
        log.info("更改密码：{}",new Gson().toJson(sysUser));
        return sysUserImpl.changePassword(sysUser,request);
    }
    @ApiOperation(value = "修改个人信息")
    @RequestMapping(value = "/changeInformation",method = RequestMethod.POST)
    public ResponseVO changeInformation(@RequestParam(value = "username",required = false)String username, @RequestParam(value = "sex",required = false)Integer sex, @RequestParam(value = "birthday",required = false)String birthday, @RequestParam(value = "country",required = false)String country,
                                        @RequestParam(value = "id",required = false) Long id,@RequestParam(value = "code",required = false)String code, @RequestPart(value = "file", required = false) MultipartFile file, HttpServletRequest request) throws BusinessException{
//        return sysUserImpl.changeInformation(sysUser,token,fileUpload);
        log.info("修改个人信息：{}",id);
        return sysUserImpl.changeInformation(username,sex,birthday,country,id,code,file,request);
    }
    @ApiOperation(value = "显示个人信息")
    @RequestMapping(value = "/showInformation",method = RequestMethod.POST)
    public ResponseVO showInformation(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException{
        log.info("查询个人信息:{}",new Gson().toJson(sysUser));
        return sysUserImpl.showInformation(sysUser,request);
    }
    @ApiOperation(value = "注销账号，七天冷静期，登录则取消注销")
    @RequestMapping(value = "/logOff",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO logOff(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException{
        log.info("注销账户：{}",new Gson().toJson(sysUser));
        return sysUserImpl.logOff(sysUser,request);
    }

    @ApiOperation(value = "退出当前会话")
    @RequestMapping(value = "/logout",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO logout(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException{
        log.info("退出登录：sysUser:{}",sysUser);
        return sysUserImpl.logout(sysUser,request);
    }
    @ApiOperation(value = "查看我反馈的问题")
    @RequestMapping(value = "/myFeedback",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO myFeedback(@RequestBody MsgCenterVo vo,HttpServletRequest request) throws BusinessException{
        log.info("查看反馈的问题: msgCenterVo: {}",vo);
        return sysUserImpl.myFeedback(vo,request);
    }
    @ApiOperation(value = "查看反馈问题详情")
    @RequestMapping(value = "/checkFeedback",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO checkFeedback(@RequestBody MsgCenter msgCenter,HttpServletRequest request) throws BusinessException{
        log.info("反馈详情：msgCenter: {}",msgCenter);
        return sysUserImpl.checkFeedback(msgCenter,request);
    }

    @ApiOperation(value = "问题反馈")
    @RequestMapping(value = "/feedback",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO feedback(@RequestBody MsgCenter msgCenter,HttpServletRequest request) throws BusinessException{
        log.info("反馈问题:msgCenter: {}",msgCenter);
        return sysUserImpl.feedback(msgCenter,request);
    }
    @ApiOperation(value = "查看系统通知列表")
    @RequestMapping(value = "/showNotification",method = RequestMethod.POST)
    public ResponseVO showNotifications(@RequestBody MsgCenterVo msgCenterVo, HttpServletRequest request) throws BusinessException{
        log.info("系统通知分页信息：msgCenterVo：{},msgCenter:{}",msgCenterVo,msgCenterVo.getUser_id());
        return sysUserImpl.showNotification(msgCenterVo,request);
    }
    @ApiOperation(value = "查看系统通知")
    @RequestMapping(value = "/checkNotification",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO checkNotification(@RequestBody MsgCenter msgCenter,HttpServletRequest request) throws BusinessException{
        log.info("系统通知：msgCenter: {}",msgCenter);
        return sysUserImpl.checkNotification(msgCenter,request);
    }
    @ApiOperation(value = "一键已读")
    @RequestMapping(value = "/readAll",method = RequestMethod.GET)
    public ResponseVO readAll(HttpServletRequest request) throws BusinessException{
        return sysUserImpl.readAll(request);
    }

    @ApiOperation(value = "获取邮箱验证码")
    @RequestMapping(value = "/getEmailCode",method = RequestMethod.GET)
    public ResponseVO getEmailCode(@RequestParam String email,HttpServletRequest request)throws BusinessException{
        log.info("邮箱验证码：email:{}",email);
        return sysUserImpl.getEmailCode(email,request);
    }
    @ApiOperation(value = "通过邮箱改密")
    @RequestMapping(value = "/updatePwdByEmail",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updatePwdByEmail (@RequestBody SysEmailCode sysEmailCode,HttpServletRequest request) throws BusinessException{
        log.info("邮箱改密：code:{}",sysEmailCode);
        return sysUserImpl.updatePwdByEmail(sysEmailCode,request);
    }
    @ApiOperation(value = "通过邮箱验证码换绑邮箱")
    @RequestMapping(value = "/updateEmail",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updateEmail(@RequestBody SysEmailCode sysEmailCode,HttpServletRequest request) throws BusinessException{
        log.info("换绑邮箱：code:{}",sysEmailCode);
        return sysUserImpl.updateEmail(sysEmailCode,request);
    }
    @ApiOperation(value = "定时扫表时调用的接口")
    @RequestMapping(value = "/cronFind",method = RequestMethod.GET)
    public ResponseVO cronFind(HttpServletRequest request)throws BusinessException{
        return sysUserImpl.cronFind(request);
    }
    @ApiOperation(value = "修改上传头像")
    @RequestMapping(value = "/updateAvatar",method = RequestMethod.POST)
    public ResponseVO updateAvatar(@RequestPart(value = "file") MultipartFile file, HttpServletRequest request) throws BusinessException{
        return sysUserImpl.updateAvatar(file,request);
    }
    @ApiOperation(value = "修改个人基本信息")
    @RequestMapping(value = "/updateInformation",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updateInformation(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException{
        return sysUserImpl.updateInformation(sysUser,request);
    }
}

