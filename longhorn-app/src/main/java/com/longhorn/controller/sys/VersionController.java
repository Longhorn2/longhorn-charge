package com.longhorn.controller.sys;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.VersionImpl;
import com.longhorn.model.dto.PileManagement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "版本管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/version")
public class VersionController {
    @Autowired
    VersionImpl versionImpl;
    @ApiOperation("App版本更新查询")
    @RequestMapping(value = "/queryAppVersion",method = RequestMethod.GET)
    public ResponseVO queryAppVersion(@RequestParam("version")String version, @RequestParam("type")Integer type, HttpServletRequest request)throws BusinessException {
        return versionImpl.queryAppVersion(version,type,request);
    }
    @ApiOperation("固件版本升级")
    @RequestMapping(value = "/queryPileVersion",method = RequestMethod.GET)
    public ResponseVO queryPileVersion(@RequestParam("version")String version,@RequestParam("type")Integer type,HttpServletRequest request)throws BusinessException{
        return versionImpl.queryPileVersion(version,type,request);
    }
    @ApiOperation("版本升级")
    public ResponseVO upgrade(@RequestBody PileManagement pileManagement, HttpServletRequest request) throws BusinessException{
        return versionImpl.upgrade(pileManagement,request);
    }
}
