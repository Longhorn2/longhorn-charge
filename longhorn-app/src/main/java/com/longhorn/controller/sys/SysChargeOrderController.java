package com.longhorn.controller.sys;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.ocpp1_6.dot.CancelReservation;
import com.longhorn.common.ocpp1_6.dot.RemoteStartTransaction;
import com.longhorn.common.ocpp1_6.dot.ReserveNow;
import com.longhorn.dto.impl.SysChargeOrderImpl;
import com.longhorn.dto.service.SysChargeOrderService;
import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.vo.SysChargeOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "充电管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysChargeOrder")
public class SysChargeOrderController {
    @Autowired
    SysChargeOrderImpl sysChargeOrderImpl;
    @ApiOperation("按月查看充电记录")
    @RequestMapping(value = "/chargeHistory",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO chargeHistory(@RequestBody SysChargeOrderVo vo, HttpServletRequest request) throws BusinessException {
        return sysChargeOrderImpl.chargeHistory(vo,request);
    }

    @ApiOperation("查看充电趋势")
    @RequestMapping(value = "/chargingTrends",method = RequestMethod.GET)
    public ResponseVO chargingTrends(@RequestParam("pileCode")String pileCode,HttpServletRequest request) throws BusinessException{
        log.info("充电桩编号：pileCode:{}",pileCode);
        return sysChargeOrderImpl.chargingTrends(pileCode,request);
    }
    @ApiOperation("开启充电")
    @RequestMapping(value = "startCharge",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO startCharge(@RequestBody SysChargeOrder sysChargeOrder, HttpServletRequest request)throws BusinessException{
        return sysChargeOrderImpl.startCharge(sysChargeOrder,request);
    }
    @ApiOperation("查看即时充电桩详情")
    @RequestMapping(value = "/requestChargingPileInfo",method = RequestMethod.GET)
    public ResponseVO requestChargingPileInfo(@RequestParam("pileCode")String pileCode,HttpServletRequest request) throws BusinessException{
        log.info("桩编号：pileCode:{}",pileCode);
        return sysChargeOrderImpl.requestChargingPileInfo(pileCode,request);
    }
    @ApiOperation("充电实时状态")
    @RequestMapping(value = "/queryMeterValues",method = RequestMethod.GET)
    public ResponseVO queryMeterValues(@RequestParam("pileCode")String pileCode,@RequestParam("connectorId")Integer connectorId,HttpServletRequest request)throws BusinessException{
        return sysChargeOrderImpl.queryMeterValues(pileCode,connectorId,request);
    }


    @ApiOperation("停止充电")
    @RequestMapping(value = "/stopCharge",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO stopCharge(@RequestBody SysChargeOrder sysChargeOrder,HttpServletRequest request)throws BusinessException{
        return sysChargeOrderImpl.stopCharge(sysChargeOrder,request);
    }
    @ApiOperation("远程启动")
    public ResponseVO sendRemoteStartTransaction(@RequestBody RemoteStartTransaction remoteStartTransaction,@PathVariable String pileCode,HttpServletRequest request) throws BusinessException{
        log.info("remoteStartTransaction:{}",remoteStartTransaction);
        return sysChargeOrderImpl.sendRemoteStartTransaction(remoteStartTransaction,pileCode,request);
    }
    @ApiOperation("查看充电过程数据")
    @RequestMapping(value = "/showChargeData",method = RequestMethod.GET)
    public ResponseVO showChargeData(@RequestParam("orderId")Long orderId,HttpServletRequest request)throws BusinessException{
        return sysChargeOrderImpl.showChargeData(orderId,request);
    }

}
