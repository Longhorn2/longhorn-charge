package com.longhorn.controller.sys;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysPileMemberImpl;
import com.longhorn.model.dto.SysPileMember;
import com.longhorn.model.vo.SysPileMemberVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Api(tags = "充电桩成员管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/pileMember")
public class SysPileMemberController {
    @Autowired
    SysPileMemberImpl sysPileMemberImpl;

    @ApiOperation("添加关联亲友到该桩")
    @RequestMapping(value = "/addMember",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addMember(@RequestBody SysPileMember sysPileMember, HttpServletRequest request){
        log.info("添加的成员信息：sysPileMember:{}",sysPileMember);
        return sysPileMemberImpl.addMember(sysPileMember,request);
    }

    @ApiOperation(("修改昵称"))
    @RequestMapping(value = "/updateNickname", method = RequestMethod.GET)
    public ResponseVO updateNickname(@RequestParam("id") Long id, @RequestParam("nickname") String nickname, HttpServletRequest request) {
        return sysPileMemberImpl.updateNickname(id, nickname, request);
    }

    @ApiOperation("从该桩删除关联亲友")
    @RequestMapping(value = "delMember", method = RequestMethod.GET)
    public ResponseVO delMember(@RequestParam("memberId") String email , @RequestParam("pileId") Long pileId, HttpServletRequest request) {
        return sysPileMemberImpl.delMember(email, pileId, request);
    }

    @ApiOperation("查看该桩的关联用户")
    @RequestMapping(value = "/showMember", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO showMember(@RequestBody SysPileMemberVo sysPileMemberVo, HttpServletRequest request) {
        return sysPileMemberImpl.showMember(sysPileMemberVo, request);
    }
}
