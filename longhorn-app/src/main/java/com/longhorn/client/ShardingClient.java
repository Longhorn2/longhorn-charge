package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.OrderVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@FeignClient(value = "longhorn-sharding")
public interface ShardingClient {
    @ApiOperation("充电数据实时查询")
    @RequestMapping(value = "/api/v1/longhorn-sharding/sharding/showChargeData",method = RequestMethod.GET)
    ResponseVO showChargeData(@RequestParam("orderId")Long orderId) throws BusinessException;
}
