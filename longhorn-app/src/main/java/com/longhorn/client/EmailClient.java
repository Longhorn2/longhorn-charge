package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.vo.EmailVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "longhorn-email")
public interface EmailClient {
    @RequestMapping(value = "/api/v1/longhorn-email/email/sendMail",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO sendMail(@RequestBody EmailVo vo)throws BusinessException;
}
