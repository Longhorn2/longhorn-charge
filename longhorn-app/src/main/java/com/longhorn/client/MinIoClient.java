package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;


import com.longhorn.model.vo.FileVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@FeignClient(value = "longhorn-minio")
public interface MinIoClient {
    //上传文件
    @PostMapping(value = "/api/v1/longhorn-minio/minio/uploadFile",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseVO uploadFile(@RequestPart("file") MultipartFile file) throws BusinessException;
    //自定义文件上传，自定义文件桶
    @PostMapping(value = "/api/v1/longhorn-minio/minio/uploadFileByName",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseVO uploadFileByName(@RequestPart("file") MultipartFile file,@RequestParam("fileName")String name)throws BusinessException;
    @ApiOperation(value = "根据文件名获取文件地址")
    @RequestMapping(value = "/api/v1/longhorn-minio/minio/getFileUrlByName",method = RequestMethod.POST)
    ResponseVO getFileUrlByName(@RequestParam("bucketName") String bucketName,@RequestBody(required = false) FileVo vo)throws BusinessException;

    @ApiOperation(value = "根据文件名删除文件")
    @RequestMapping(value = "/api/v1/longhorn-minio/minio/delFileByName",method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseVO delFileByName(FileVo vo)throws BusinessException;
    @ApiOperation(value = "下载文件")
    @RequestMapping(value = "/api/v1/longhorn-minio/minio/downloadFileByName",method = RequestMethod.GET,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    void downloadFileByName(@RequestParam("fileName")String name,@RequestParam("bucketName")String bucketName)throws BusinessException;
    @ApiOperation(value = "根据涌删除文件")
    @RequestMapping(value = "/api/v1/longhorn-minio/minio/delFileByFileName",method = RequestMethod.GET)
    @ResponseBody
    ResponseVO delFileByFileName(@RequestParam("fileName")String name,@RequestParam("bucketName") String bucketName)throws BusinessException;
}
