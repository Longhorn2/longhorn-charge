package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.ocpp1_6.dot.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "longhorn-im")
public interface ImClient {
    @ApiOperation(value = "存储通知信息id")
    @GetMapping("/api/v1/longhorn-im/sysUser/saveUserId/{stationId}/{pileCode}")
    ResponseVO saveUserId(@PathVariable("stationId") Long stationId, @PathVariable("pileCode")String pileCode) throws BusinessException;

    @ApiOperation(value = "通过redis key获取value")
    @RequestMapping(value = "/api/v1/longhorn-im/redisUtils/getKey/{key}", method = RequestMethod.GET)
    ResponseVO getKey(@PathVariable(value = "key") String key) throws BusinessException;
    @ApiOperation(value = "开启充电")
    @RequestMapping(value = "/api/v1/longhorn-im/cmd/startCharge/{pileCode}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO startCharge(@RequestBody ReserveNow reserveNow,@PathVariable("pileCode") String pileCode)throws BusinessException;
    @ApiOperation(value = "停止充电")
    @RequestMapping(value = "/api/v1/longhorn-im/cmd/stopCharge/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO stopCharge(@RequestBody CancelReservation cancelReservation, @PathVariable("pileCode")String pileCode) throws BusinessException;
    @ApiOperation(value = "充电枪状态解锁锁定")
    @RequestMapping(value = "/api/v1/longhorn-im/cmd/changeAvailability/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO changeAvailability(@RequestBody ChangeAvailability changeAvailability, @PathVariable("pileCode")String pileCode) throws BusinessException;

    @ApiOperation(value = "远程启动")
    @RequestMapping(value = "/api/v1/longhorn-im/cmd/remoteStartTransaction/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO sendRemoteStartTransaction(@RequestBody RemoteStartTransaction remoteStartTransaction, @PathVariable("pileCode")String pileCode) throws BusinessException;
    @ApiOperation(value = "远程停止")
    @RequestMapping(value = "/api/v1/longhorn-im/cmd/remoteStopTransaction/{pileCode}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseVO sendRemoteStopTransaction(@RequestBody RemoteStopTransaction remoteStopTransaction, @PathVariable("pileCode")String pileCode) throws BusinessException;
}
