package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.dto.SysOrderLog;
import com.longhorn.model.dto.SysPileMember;
import com.longhorn.model.vo.ChargingTrendVo;
import com.longhorn.model.vo.SysChargeOrderVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
@Component
public interface SysChargeOrderMapper {
    List<SysChargeOrder> chargeHistory(SysChargeOrderVo vo);

    Integer getOrderTotal(SysChargeOrderVo vo);

    BigDecimal getCapacitySum(SysChargeOrderVo vo);

    List<ChargingTrendVo> chargingTrends(@Param("pileCode") String pileCode);

    void addOrder(SysChargeOrder sysChargeOrder);

    void stopCharge(@Param("orderId") Long orderId);

    void addLog(SysOrderLog sysOrderLog);

    Integer queryPileByCode(@Param("pileCode") String pileCode);

    SysPileMember queryMember(@Param("pile_code") String pile_code,@Param("user_id") Long user_id);

    List<SysChargeOrder> queryChargeOrderByAll(@Param("pileCode") String pileCode,@Param("connectorId") Integer connectorId,@Param("userId") Long userId);
}
