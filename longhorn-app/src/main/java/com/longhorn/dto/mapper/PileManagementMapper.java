package com.longhorn.dto.mapper;

import com.longhorn.model.dto.*;
import com.longhorn.model.vo.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PileManagementMapper {
    void bind(@Param("code") String code,@Param("userId") Long userId);
    List<PileManagement> pileInformation(PileManagementVo vo);

    Integer getPileTotal(PileManagementVo vo);

    Integer updatePileInformation(PileManagement pileManagement);

//    Integer checkMaster(Long user_id);
//
//    SysPileMember queryMember(SysPileMember sysPileMember);

    Integer addMember(SysPileMember sysPileMember);

    PileManagement queryPile(String code);

    List<SysChargeOrder> history(SysChargeOrderVo vo);

    Integer getTotal(SysChargeOrderVo vo);

    SysPileMember queryMember(@Param("userId")Long userId,@Param(("pileId")) Long pileId);

    void addMsgCenter(MsgCenterVo msgCenterVo);

    Integer addUserMsg(MsgCenterVo msgCenterVo);

    List<ChargingTrendVo> chargingTrends(@Param("pileCode") String pileCode,@Param("time") Integer time);

    void addStation(StationManagement stationManagement);

    SysUser queryUser(@Param("userId") Long userId);

    void binding(@Param("stationId")Long PileId,@Param("userId") Long userId,@Param("pileCode") String code);

    void addUserStation(@Param("user_id") Long user_id,@Param("station_management_id") Long id);

    void delMember(@Param("pileId") Long pileId);

    void updatePileMaster(@Param("pileId") Long pileId,@Param("userId") Long userId);

}
