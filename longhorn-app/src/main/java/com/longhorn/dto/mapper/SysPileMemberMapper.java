package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysPileMember;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysPileMemberMapper {
    int checkMaster(@Param("userId") Long sysUserId,@Param("pileId") Long pile_management_id);

    SysPileMember queryMember(SysPileMember sysPileMember);


    void addMember(SysPileMember sysPileMember);

    void delMember(@Param("email") String email,@Param("pileId") Long pileId);

//    List<SysPileMember> showMember(@Param("pileId") Long pileId);

    List<SysPileMember> getMemberList(@Param("pileManagementId") Long pileManagementId,@Param("pageStart") Integer pageStart,@Param("pageSize") Integer pageSize);

    Integer getTotal(@Param("pileManagementId") Long pileManagementId);

    SysUser queryUser(String email);

    void updateNickname(@Param("id") Long id,@Param("nickname") String nickname);

}
