package com.longhorn.dto.mapper;

import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.vo.StationManagementVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StationManagementMapper {
    List<StationManagement> queryStation(StationManagementVO vo);

    StationManagement stationInformation(@Param("stationId") Long stationId);

    Integer getStationTotal(StationManagementVO vo);
}
