package com.longhorn.dto.mapper;

import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.vo.MsgCenterVo;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface MsgCenterMapper {

    List<MsgCenter> showNotification(MsgCenterVo vo);

    Integer getTotal(MsgCenterVo vo);

    void addUserMsg(List<MsgCenter> msgCenters);

    void updateStatus(List<MsgCenter> msgCenters);
}
