package com.longhorn.dto.mapper;

import com.longhorn.model.dto.*;
import com.longhorn.model.vo.MsgCenterVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Component
public interface SysUserMapper {
    SysUser queryById(@Param("id")Long id);
    SysUser queryByEmail(@Param("email")String email);
    SysUser queryByEmailAndPwd(SysUser sysUser);
    int addUser(SysUser sysUser);

    SysUser queryByAll(SysUser sysUser);

    int changePassword(SysUser sysUser);

    int changeInformation(@Param("username") String username, @Param("sex") Integer sex, @Param("birthday")String birthday, @Param("country") String country, @Param("id") Long id, @Param("code") String code, @Param("avatar") String avatar);

    List<SysUser> findOut(@Param("create_time") Timestamp create_time);

    int realLogOof(@Param("adminsId") List<Long> adminsId);

    int logOff(SysUser sysUser);

    void realDelete(@Param(("usersId")) List<Long> usersId);

    SysUser showInformation(SysUser sysUser);

    List<MsgCenter> myFeedback(MsgCenter msgCenter);

    MsgCenter checkFeedback(MsgCenter msgCenter);

    void feedback(MsgCenter msgCenter);

    List<MsgCenter> showNotification(MsgCenterVo msgCenterVo);

    void setEmailCode(SysEmailCode sysEmailCode);

    SysEmailCode checkCode(@Param("email")String email,@Param("code")Integer code);

    void updateEmailStatus(@Param("email")String email,@Param("code")Integer code);

    void updateEmail(SysUser sysUser);

    void updateStatus(SysUser sysUserInfo);

    MsgCenter checkNotification(MsgCenter msgCenter);

    void updateMsgStatus(@Param("id") Long id,@Param("status")Integer status,@Param("sysUserId") Long sysUserId);

    void readAll(@Param("sysUserId") Long sysUserId);

    int updateAvatar(@Param("avatar") String avatar,@Param("id") Long id);

    int updateInformation(SysUser sysUser);

    Integer queryMsgTotal(MsgCenterVo msgCenterVo);
    Integer queryFeedTotal(MsgCenterVo vo);

    Integer queryUnreadNum(MsgCenterVo msgCenterVo);
}
