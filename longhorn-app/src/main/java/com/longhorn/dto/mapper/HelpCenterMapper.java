package com.longhorn.dto.mapper;

import com.longhorn.model.dto.HelpCenter;
import com.longhorn.model.dto.HelpCenterType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface HelpCenterMapper {
    HelpCenter findAnswer(@Param("id") Long id);

    List<HelpCenterType> findHelpType(@Param("userId")Long userId);

    List<HelpCenter> findTitle(@Param(("typeId")) Long typeId, @Param("userId") Long userId);


}
