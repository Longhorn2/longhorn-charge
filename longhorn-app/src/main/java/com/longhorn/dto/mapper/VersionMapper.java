package com.longhorn.dto.mapper;

import com.longhorn.model.vo.VersionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface VersionMapper {
    VersionVO queryNewVersion(Integer type);

    String queryMustVersion(Integer type);

    List<VersionVO> queryPileVersion(@Param("type") Integer type,@Param("version") String version);
}
