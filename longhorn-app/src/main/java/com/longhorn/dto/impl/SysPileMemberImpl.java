package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.SysPileMemberMapper;
import com.longhorn.dto.service.SysPileMemberService;
import com.longhorn.model.dto.SysPileMember;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.SysPileMemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
@Service
public class SysPileMemberImpl implements SysPileMemberService {
    @Autowired
    SysPileMemberMapper sysPileMemberMapper;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;
    @Override
    public ResponseVO addMember(SysPileMember sysPileMember,HttpServletRequest request) throws BusinessException {
//        System.out.println(request.getHeader("Authorization"));
        //验证邮箱是否符合要求
        if (!Utils.isValidEmail(sysPileMember.getEmail())){
            return new ResponseVO(ErrorCodeEnum.EMAIL_Valid,null,request.getSession().getAttribute("language").toString());
        }
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");
        //验证本机用户是否是该桩主账号
        if (sysPileMemberMapper.checkMaster(sysUserInfo.getId(),sysPileMember.getPile_management_id())==1){
            return new ResponseVO(ErrorCodeEnum.USER_NOT_ROLE,null,request.getSession().getAttribute("language").toString());
        }
        //验证用户是否已在关联用户中
        if(sysPileMemberMapper.queryMember(sysPileMember)!=null){
            return new ResponseVO(ErrorCodeEnum.USER_IS_MEMBER,null,request.getSession().getAttribute("language").toString());
        }
        //验证邮箱是否以注册，若注册则关联上，未绑定则先关联
        SysUser sysUser = sysPileMemberMapper.queryUser(sysPileMember.getEmail());
        if (sysUser!=null){
            sysPileMember.setUser_id(sysUser.getId());
        }
        sysPileMember.setMaster(1);
        sysPileMember.setId(IdWorker.getId());
        sysPileMemberMapper.addMember(sysPileMember);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO delMember(String email, Long pileId, HttpServletRequest request) throws BusinessException {
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");
        if (sysPileMemberMapper.checkMaster(sysUserInfo.getId(),pileId)==1){
            return new ResponseVO(ErrorCodeEnum.USER_NOT_ROLE,null,request.getSession().getAttribute("language").toString());
        }
        sysPileMemberMapper.delMember(email,pileId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO showMember(SysPileMemberVo sysPileMemberVo, HttpServletRequest request) throws BusinessException {
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");
        if (sysPileMemberMapper.checkMaster(sysUserInfo.getId(), sysPileMemberVo.getPile_management_id())==1){
            return new ResponseVO(ErrorCodeEnum.USER_NOT_ROLE,null,request.getSession().getAttribute("language").toString());
        }
        if (sysPileMemberVo.getPageStart() == null || sysPileMemberVo.getPageSize() == null) {
            sysPileMemberVo.setPageStart(pageStart);
            sysPileMemberVo.setPageSize(pageSize);
        }
        sysPileMemberVo.setSysPileMemberList(sysPileMemberMapper.getMemberList(sysPileMemberVo.getPile_management_id(),(sysPileMemberVo.getPageStart()-1)*sysPileMemberVo.getPageSize(),sysPileMemberVo.getPageSize()));
        sysPileMemberVo.setTotal(sysPileMemberMapper.getTotal(sysPileMemberVo.getPile_management_id()));

        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysPileMemberVo,request.getSession().getAttribute("language").toString());
    }
    @Override
    public ResponseVO updateNickname(Long id, String nickname, HttpServletRequest request) {
        sysPileMemberMapper.updateNickname(id,nickname);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

}
