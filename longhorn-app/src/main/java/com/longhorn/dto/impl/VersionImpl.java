package com.longhorn.dto.impl;

import com.google.gson.Gson;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.dto.mapper.VersionMapper;
import com.longhorn.dto.service.VersionService;
import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.utils.VersionUtil;
import com.longhorn.model.vo.VersionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class VersionImpl implements VersionService {
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    VersionMapper versionMapper;

    /**
     * @Description 先从redis中获取最新版本和最新强制更新版本信息，如果为最新版本则无法选择更新，若小于最新版本大于最新强制更新版本则提醒更新，若小于两者则强制更新至最新版本
     * @param version
     * @param request
     * @return
     */
    @Override
    public ResponseVO queryAppVersion(String version, Integer type, HttpServletRequest request) {
        //定义最新版本和最新强制更新版本号,在中台不传最新版本号的情况下自己查询
        String newVersion;
        String mustVersion;
        VersionVO versionVO;
        if (!redisUtil.hasKey("version:newVersion")){
            versionVO = versionMapper.queryNewVersion(type);
            newVersion=versionVO.getVersion();
            redisUtil.set("version:newVersion",new Gson().toJson(versionVO));
        }else {
            versionVO=new Gson().fromJson(redisUtil.get("version:newVersion"),VersionVO.class);
            newVersion=versionVO.getVersion();
        }
        if (!redisUtil.hasKey("version:mustVersion")){
            mustVersion=versionMapper.queryMustVersion(type);
            redisUtil.set("version:mustVersion",mustVersion);
        }else {
            mustVersion=redisUtil.get("version:mustVersion");
        }
        //调用版本号比较大小工具类比较版本号大小以便确定是否更新
        if (VersionUtil.equalVersion(version,mustVersion)){
            versionVO.setFlag(true);
        }
        if (VersionUtil.equalVersion(version,newVersion)){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,versionVO,request.getSession().getAttribute("language").toString());
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     * @description 查询最新固件版本是否与本地固件版本相同
     * @param version
     * @param type
     * @param request
     * @return
     */
    @Override
    public ResponseVO queryPileVersion(String version, Integer type, HttpServletRequest request) {
        List<VersionVO> versionVOList = versionMapper.queryPileVersion(type,version);
        if (versionVOList.isEmpty()){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,versionVOList,request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description 进行固件版本升级，待确定调用中台的方法
     * @param pileManagement
     * @param request
     * @return
     */
    @Override
    public ResponseVO upgrade(PileManagement pileManagement, HttpServletRequest request) {
        return null;
    }


}
