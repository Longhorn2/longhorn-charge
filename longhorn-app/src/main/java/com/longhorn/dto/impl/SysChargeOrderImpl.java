package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.client.ImClient;
import com.longhorn.client.ShardingClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.business.WebSocketSessionVo;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.ocpp1_6.dot.*;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.SysChargeOrderMapper;
import com.longhorn.dto.service.SysChargeOrderService;
import com.longhorn.model.dto.*;
import com.longhorn.model.utils.DateUtil;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.vo.ChargingDataVO;
import com.longhorn.model.vo.ChargingTrendVo;
import com.longhorn.model.vo.StatusNotificationVO;
import com.longhorn.model.vo.SysChargeOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class SysChargeOrderImpl implements SysChargeOrderService {
    /**
     * 根据前端传来的master判断是否为桩主，若是则查看此桩所有用户的订单，若不是则只看自己
     *
     * @param vo
     * @param request
     * @return
     */
    @Autowired
    SysChargeOrderMapper sysChargeOrderMapper;
    @Autowired
    ImClient imClient;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    ShardingClient shardingClient;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;

    @Override
    public ResponseVO chargeHistory(SysChargeOrderVo vo, HttpServletRequest request) {
        if (vo.getPageStart()==null||vo.getPageSize()==null){
            vo.setPageStart(pageStart);
            vo.setPageSize(pageSize);
        }
        int pageStart = vo.getPageStart();
        vo.setPageStart((pageStart - 1) * vo.getPageSize());
        List<SysChargeOrder> sysChargeOrderList = sysChargeOrderMapper.chargeHistory(vo);
        vo.setSysChargeOrderList(sysChargeOrderList);
        vo.setChargeCount(sysChargeOrderList.size());
        vo.setCapacitySum(sysChargeOrderMapper.getCapacitySum(vo));
        vo.setTotal(sysChargeOrderMapper.getOrderTotal(vo));
        vo.setPageStart(pageStart);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 返回近三十天每天的订单数据，时间不可选
     *
     * @param pileCode
     * @param request
     * @return
     */
    @Override
    public ResponseVO chargingTrends(String pileCode, HttpServletRequest request) {
        List<ChargingTrendVo> vo = sysChargeOrderMapper.chargingTrends(pileCode);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 传入枪号和桩号，验证枪桩可用后生成订单号，注入reserveNow并调用开始充电方法
     *
     * @param request
     * @return
     */
    @Override
    public ResponseVO startCharge(SysChargeOrder sysChargeOrder, HttpServletRequest request) {
//        WebSocketSessionVo vo = new Gson().fromJson(imClient.getKey("status::"+sysChargeOrder.getPile_code()).getData().toString(), WebSocketSessionVo.class);
//        if (vo.getStatus()==0){
//            return new ResponseVO(ErrorCodeEnum.PILE_OFFLINE,null,request.getSession().getAttribute("language").toString());
//        }
//        imClient.getKey("StatusNotification::"+sysChargeOrder.getPile_code()+"::"+sysChargeOrder.getConnectorId()).getData();
//        StatusNotification statusNotification = new Gson().fromJson(imClient.getKey("StatusNotification::"+sysChargeOrder.getPile_code()+"::"+sysChargeOrder.getConnectorId()).getData().toString(),StatusNotification.class);
//        if (!(statusNotification.getErrorCode().equals("NoError"))){
//            return new ResponseVO();
//        }


        if (!this.checkStatus(sysChargeOrder.getPile_code(), sysChargeOrder.getConnectorId(), sysChargeOrder.getUser_id())) {
            return new ResponseVO(ErrorCodeEnum.PILE_OFFLINE, null, request.getSession().getAttribute("language").toString());
        }
        //
        //启动成员身份校验
        SysPileMember member = sysChargeOrderMapper.queryMember(sysChargeOrder.getPile_code(), sysChargeOrder.getUser_id());
        if (member == null) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_MEMBER, null, request.getSession().getAttribute("language").toString());
        }

        //验证完毕，生成订单
        sysChargeOrder.setOrder_id(Utils.getOrderId());
        sysChargeOrder.setId(IdWorker.getId());
        RemoteStartTransaction remoteStartTransaction = new RemoteStartTransaction();
        remoteStartTransaction.setIdTag(String.valueOf(sysChargeOrder.getOrder_id()));
        remoteStartTransaction.setConnectorId(sysChargeOrder.getConnectorId());


        //调用im模块startCharge()，并将订单写入数据库
        sysChargeOrderMapper.addOrder(sysChargeOrder);
        imClient.sendRemoteStartTransaction(remoteStartTransaction, sysChargeOrder.getPile_code());
        //将操作记录到日志,暂时写一起，等多种启动方式确定后再单独提出来
        SysOrderLog sysOrderLog = new SysOrderLog();
        sysOrderLog.setId(IdWorker.getId());
        sysOrderLog.setOrder_id(sysChargeOrder.getOrder_id());
        sysOrderLog.setTransaction_name(1);
        if (redisUtil.hasKey("userInfo:" + sysChargeOrder.getUser_id())) {
            sysOrderLog.setUser_id(new Gson().fromJson(redisUtil.get("userInfo:" + sysChargeOrder.getUser_id()).toString(), SysUser.class).getId());
        } else {
            sysOrderLog.setUser_id(((SysUser) request.getSession().getAttribute("sysUserInfo")).getId());
        }

        sysChargeOrderMapper.addLog(sysOrderLog);

        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * 停止充电，传入桩编号，调用模块获取预约id，调用im停止充电模块。将数据库订单状态改为停止中
     *
     * @param request
     * @return
     */
    @Override
    public ResponseVO stopCharge(SysChargeOrder sysChargeOrder, HttpServletRequest request) {
        RemoteStopTransaction remoteStopTransaction = new RemoteStopTransaction();
        int transactionId;
        try {
            transactionId = Integer.parseInt(new Gson().fromJson(imClient.getKey("StartTransaction::" + sysChargeOrder.getPile_code() + "::" + sysChargeOrder.getConnectorId()).getData().toString(), String.class));
        } catch (Exception e) {
            transactionId = 0;
        }

        if (transactionId == 0) {
            MeterValues meterValues = new Gson().fromJson(imClient.getKey("MeterValues::" + sysChargeOrder.getPile_code() + "::" + sysChargeOrder.getConnectorId()).getData().toString(), MeterValues.class);
            transactionId = meterValues.getTransactionId();
        }
        remoteStopTransaction.setTransactionId(transactionId);
        imClient.sendRemoteStopTransaction(remoteStopTransaction, sysChargeOrder.getPile_code());

        //利用唯一transactionId将开始信息查出来
        StartTransaction startTransaction = new Gson().fromJson(imClient.getKey("StartTransaction::" + sysChargeOrder.getPile_code() + "::" + sysChargeOrder.getConnectorId() + transactionId).getData().toString(), StartTransaction.class);
        sysChargeOrderMapper.stopCharge(Long.valueOf(startTransaction.getIdTag()));
        //将操作记录到日志,暂时写一起，等多种启动方式确定后再单独提出来
        SysOrderLog sysOrderLog = new SysOrderLog();
        sysOrderLog.setId(IdWorker.getId());
        sysOrderLog.setOrder_id(Long.valueOf(startTransaction.getIdTag()));
        sysOrderLog.setTransaction_name(2);
        if (redisUtil.hasKey("userInfo:" + sysChargeOrder.getUser_id())) {
            sysOrderLog.setUser_id(new Gson().fromJson(redisUtil.get("userInfo:" + sysChargeOrder.getUser_id()).toString(), SysUser.class).getId());
        } else {
            sysOrderLog.setUser_id(((SysUser) request.getSession().getAttribute("sysUserInfo")).getId());
        }
        sysChargeOrderMapper.addLog(sysOrderLog);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * 查看该订单下的充电趋势
     *
     * @param orderId
     * @param request
     * @return
     */
    @Override
    public ResponseVO showChargeData(Long orderId, HttpServletRequest request) {
 //       List<Order> orders = shardingClient.showChargeData(orderId);
        List<OrderVO> orderList =(List<OrderVO>) shardingClient.showChargeData(orderId).getData();
        return new ResponseVO(ErrorCodeEnum.SUCCESS, orderList, request.getSession().getAttribute("language").toString());
    }

    /**
     * 远程启动，调用im接口
     *
     * @param remoteStartTransaction
     * @param request
     * @return
     */
    @Override
    public ResponseVO sendRemoteStartTransaction(RemoteStartTransaction remoteStartTransaction, String pileCode, HttpServletRequest request) {
//        if (!this.checkStatus(pileCode, remoteStartTransaction.getConnectorId())){
//            return new ResponseVO(ErrorCodeEnum.PILE_OFFLINE,null,request.getSession().getAttribute("language").toString());
//        }
//        SysChargeOrder sysChargeOrder = new SysChargeOrder();
//        sysChargeOrder.setOrder_id(Utils.getOrderId());
//        sysChargeOrder.setId(IdWorker.getId());
//        sysChargeOrder.setConnectorId(remoteStartTransaction.getConnectorId());
//        sysChargeOrder.setPile_code(pileCode);
//        sysChargeOrder.setStart_method(6);
//        remoteStartTransaction.setIdTag(sysChargeOrder.getOrder_id().toString());
//        sysChargeOrderMapper.addOrder(sysChargeOrder);
//        imClient.sendRemoteStartTransaction(remoteStartTransaction,pileCode);
//
//
//
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
        return null;
    }

    /**
     * 当前充电桩状态从redis里取，暂时手动插入数据
     *
     * @param pileCode
     * @param request
     * @return
     */
    @Override
    public ResponseVO requestChargingPileInfo(String pileCode, HttpServletRequest request) {
        ChargingDataVO vo = new ChargingDataVO();
        //从redis里取的数据，先禁用

        WebSocketSessionVo webSocketSessionVo;
        try {
            webSocketSessionVo=new Gson().fromJson(imClient.getKey("status::" + pileCode).getData().toString(), WebSocketSessionVo.class);
        }catch (Exception e){
            return new ResponseVO(ErrorCodeEnum.PILE_NOT_INFORMATION,null,request.getSession().getAttribute("language").toString());
        }

        vo.setStatus(webSocketSessionVo.getStatus());
        vo.setGunNum(sysChargeOrderMapper.queryPileByCode(pileCode));
        List<StatusNotificationVO> statusNotificationList = new ArrayList<>();
        //从redis缓存里取出的枪状态信息
        for (int i = 0; i < vo.getGunNum(); i++) {
            StatusNotificationVO statusNotification;
            try {
                statusNotification = new Gson().fromJson(imClient.getKey("StatusNotification::" + pileCode + "::" + i).getData().toString(), StatusNotificationVO.class);
                //获取充电桩事务的唯一标识
                String transactionId;
                try {
                    transactionId=imClient.getKey("StartTransaction::"+pileCode+"::"+i).getData().toString();
                }catch (Exception e){
                    transactionId=null;
                }
                //通过唯一事务标识在启动充电事务中获取订单号
                if (transactionId==null){
                    statusNotification.setOrderId(null);
                }else {
                    statusNotification.setOrderId(Long.valueOf(new Gson().fromJson(imClient.getKey("StartTransaction::"+pileCode+"::"+i+"::"+transactionId).getData().toString(),StartTransaction.class).getIdTag()));
                }

            } catch (Exception e) {
                log.info("Exception:{}",e);
                continue;
            }

            statusNotificationList.add(statusNotification);
        }

//        StatusNotificationVO statusNotification = new StatusNotificationVO();
//        statusNotification.setConnectorId(0);
//        statusNotification.setStatus("Available");
//        statusNotification.setErrorCode("NoError");
//        statusNotification.setOrderId(16861946782004259L);
//        statusNotificationList.add(statusNotification);
//        statusNotification.setConnectorId(1);
//        statusNotification.setStatus("Charging");
//        statusNotification.setErrorCode("NoError");
//        statusNotification.setOrderId(16861947300531209L);
//        statusNotificationList.add(statusNotification);


        vo.setGunStatus(statusNotificationList);


        vo.setChargeTime(BigDecimal.valueOf(10.22));
        vo.setStartTime("10:00");
        vo.setCapacity(BigDecimal.valueOf(10.08));
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 查询redis中的充电实时状态
     *
     * @param pileCode
     * @param connectorId
     * @return
     */
    @Override
    public ResponseVO queryMeterValues(String pileCode, Integer connectorId, HttpServletRequest request) {
        MeterValues meterValues = new Gson().fromJson(imClient.getKey("MeterValues::" + pileCode + "::" + connectorId).getData().toString(), MeterValues.class);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, meterValues, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param pileCode
     * @param connectorId
     * @param userId
     * @return
     * @Describle 启动充电前验证：验证桩是否在线，验证枪状态是否可用，验证枪是否已准备，验证枪是否已被占用。
     */
    public Boolean checkStatus(String pileCode, Integer connectorId, Long userId) throws BusinessException {
        WebSocketSessionVo vo = new Gson().fromJson(imClient.getKey("status::" + pileCode).getData().toString(), WebSocketSessionVo.class);
        if (vo.getStatus() == 0) {
            return false;
        }
        imClient.getKey("StatusNotification::" + pileCode + "::" + connectorId).getData();
        StatusNotification statusNotification = new Gson().fromJson(imClient.getKey("StatusNotification::" + pileCode + "::" + connectorId).getData().toString(), StatusNotification.class);
        if (!(statusNotification.getErrorCode().equals("NoError"))) {
            return false;
        }
        if (!statusNotification.getStatus().equals("Preparing")) {
            return false;
        }
        if (sysChargeOrderMapper.queryChargeOrderByAll(pileCode, connectorId, userId) != null) {
            return false;
        }
        return true;
    }

}
