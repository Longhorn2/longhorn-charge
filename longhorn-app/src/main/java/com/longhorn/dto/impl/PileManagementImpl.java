package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.client.ImClient;
import com.longhorn.client.MinIoClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.dto.mapper.PileManagementMapper;
import com.longhorn.dto.service.PileManagementService;
import com.longhorn.model.dto.*;
import com.longhorn.model.utils.AsyncUtil;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.utils.VersionUtil;
import com.longhorn.model.vo.MsgCenterVo;
import com.longhorn.model.vo.PileManagementVo;
import com.longhorn.model.vo.VersionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Service
public class PileManagementImpl implements PileManagementService {
    @Autowired
    PileManagementMapper pileManagementMapper;
    @Autowired
    ImClient imClient;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    MinIoClient minIoClient;
    @Autowired
    AsyncUtil asyncUtil;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;

    /**
     * @param
     * @param
     * @param request
     * @return
     * @Description 扫码绑定，若前端传解析后的加密字符串则需添加解密算法，若传二维码图片则需写二维码解析工具类
     * 绑定成功后调用im模块方法获取应当范围广播的用户id进行广播
     */
    @Override
    public ResponseVO bind(String code, SysPileMember sysPileMember, HttpServletRequest request) throws BusinessException {
        //检验要绑定的桩是否在公共桩库中存在
        PileManagement pileManagement = pileManagementMapper.queryPile(code);
        if (pileManagement == null) {
            return new ResponseVO(ErrorCodeEnum.PILE_NOT_EXIST, null, request.getSession().getAttribute("language").toString());
        }
        //判断该桩是否已绑定
        if (pileManagement.getUser_id() != null) {
            return new ResponseVO(ErrorCodeEnum.PILE_HAS_BOUND, null, request.getSession().getAttribute("language").toString());
        }
        //关联桩与用户，并在桩成员中添加桩主为master
        pileManagementMapper.bind(code, sysPileMember.getUser_id());
        sysPileMember.setId(IdWorker.getId());
        sysPileMember.setMaster(0);
        sysPileMember.setPile_management_id(pileManagement.getId());
        pileManagementMapper.addMember(sysPileMember);
        //在这里调用im模块获取范围广播对象，添加系统通知

        imClient.saveUserId(pileManagement.getStation_management_id(), pileManagement.getPile_code());
        List<Long> list = new Gson().fromJson(imClient.getKey("pile::station::userId::" + pileManagement.getPile_code()).getData().toString(), List.class);

        //调用本逻辑类的添加Msg方法开始向系统通知表及其读取状态表中插入数据
        MsgCenterVo msgCenterVo = new MsgCenterVo();
        msgCenterVo.setUserIds(list);
        msgCenterVo.setProblem("尊敬的用户您好，现于" + pileManagement.getStationName() + "新设充电桩" + pileManagement.getPile_code());
        msgCenterVo.setId(IdWorker.getId());
        msgCenterVo.setStatus(0);
        msgCenterVo.setType(2);
        msgCenterVo.setTitle("新增充电桩");


        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }


    /**
     * @param vo
     * @param request
     * @return
     * @Description 展示用户所拥有的充电桩列表信息
     */
    @Override
    public ResponseVO pileInformation(PileManagementVo vo, HttpServletRequest request) {
        if (vo.getPageStart() == null || vo.getPageSize() == null) {
            vo.setPageStart(pageStart);
            vo.setPageSize(pageSize);
        }
        int pageStart = vo.getPageStart();
        vo.setPageStart((pageStart - 1) * vo.getPageSize());
        vo.setPileManagementList(pileManagementMapper.pileInformation(vo));
        vo.setTotal(pileManagementMapper.getPileTotal(vo));
        vo.setPageStart(pageStart);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param pileManagement
     * @param request
     * @return
     * @Description 修改该充电桩基础信息
     */
    @Override
    public ResponseVO updatePileInformation(PileManagement pileManagement, HttpServletRequest request) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, pileManagementMapper.updatePileInformation(pileManagement), request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO upgrade(PileManagement pileManagement, HttpServletRequest request) {
        pileManagement.getVersion();

        return null;
    }

    /**
     * @param PNP
     * @param request
     * @return
     * @Description 仅实现点击滑块改变boolean值功能
     */
    @Override
    public ResponseVO isPNP(Boolean PNP,String pileCode, HttpServletRequest request) {
        log.info("是否开启充电：PNP:{}", PNP);
        //待确定调用的接口或与桩的约定
        return new ResponseVO(ErrorCodeEnum.SUCCESS, !PNP, request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description 暂时通过桩编号查出所有使用过此私桩的角色订单（未跟桩主关联的角色无法使用此桩，应当不存在外来用户使用）
     * @param vo
     * @param request
     * @return
     */
//    @Override
//    public ResponseVO history(SysChargeOrderVo vo, HttpServletRequest request) {
// //       log.info("该桩的订单信息：order:{}",pileManagementMapper.history(vo));
//        //验证用户是否为该桩桩主
//        SysPileMember sysPileMember = pileManagementMapper.queryMember(vo.getUser_id(),vo.getPileId());
//        vo.setMaster(sysPileMember.getMaster());
//        int pageStart = vo.getPageStart();
//        vo.setPageStart((pageStart-1)*vo.getPageSize());
//        vo.setSysChargeOrderList(pileManagementMapper.history(vo));
//        vo.setTotal(pileManagementMapper.getTotal(vo));
//        vo.setPageStart(pageStart);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//    }

    /**
     * @Description 点击开始充电时生成订单，并将桩要的信息传给桩，得到反馈后将订单信息写入数据库
     * @param order
     * @param request
     * @return
     */
//    @Override
//    public ResponseVO startCharge(SysChargeOrder order, HttpServletRequest request) {
//        //定义一个boolean值默认为false用于判断桩端是否充电成功
//
//        //这里是传信息给桩的代码
//        return null;
//    }
//
//    @Override
//    public ResponseVO chargingTrends(String pileCode, Integer time, HttpServletRequest request) {
//        List<ChargingTrendVo> vo= pileManagementMapper.chargingTrends(pileCode,time);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//    }

    /**
     * 检验code码是否为公司产品，且是否已被绑定
     *
     * @param code
     * @param request
     * @return
     */
    @Override
    public ResponseVO checkBindCode(String code, HttpServletRequest request) {
        //检验要绑定的桩是否在公共桩库中存在
        PileManagement pileManagement = pileManagementMapper.queryPile(code);
        if (pileManagement == null) {
            return new ResponseVO(ErrorCodeEnum.PILE_NOT_EXIST, null, request.getSession().getAttribute("language").toString());
        }
        //判断该桩是否已绑定
        if (pileManagement.getUser_id() != null) {
            return new ResponseVO(ErrorCodeEnum.PILE_HAS_BOUND, null, request.getSession().getAttribute("language").toString());
        }
        redisUtil.set("pile:" + code, new Gson().toJson(pileManagement));
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO binding(String code, StationManagement stationManagement, HttpServletRequest request) {
        stationManagement.setId(IdWorker.getId());
        SysUser sysUser;
        if (!redisUtil.hasKey("pile:" + code)) {
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        }
        if (redisUtil.hasKey("userInfo:" + stationManagement.getUser_id())) {
            sysUser = new Gson().fromJson(redisUtil.get("userInfo:" + stationManagement.getUser_id()), SysUser.class);
        } else {
            sysUser = pileManagementMapper.queryUser(stationManagement.getUser_id());
        }
        stationManagement.setContact_person(sysUser.getUser_name());
        stationManagement.setContact_phone(sysUser.getContact_phone());
        //新增站点
        pileManagementMapper.addStation(stationManagement);
        //修改公共桩信息为已绑定
        pileManagementMapper.binding(stationManagement.getId(), stationManagement.getUser_id(), code);
        //添加桩主为master到桩成员表
        SysPileMember sysPileMember = new SysPileMember();
        sysPileMember.setId(IdWorker.getId());
        sysPileMember.setUser_id(stationManagement.getUser_id());
        sysPileMember.setPile_management_id((Long) new Gson().fromJson(redisUtil.get("pile:" + code), PileManagement.class).getId());
        sysPileMember.setMaster(0);
        sysPileMember.setEmail(sysUser.getEmail());
        sysPileMember.setNickname(sysUser.getUser_name());
        pileManagementMapper.addMember(sysPileMember);
        redisUtil.del("pile:" + code);
        //向用户站点中间表添加用户id和站点id
        pileManagementMapper.addUserStation(stationManagement.getUser_id(), stationManagement.getId());
        //调用通知模块，将绑定成功信息写入消息中心表
        MsgCenterVo msgCenterVo = new MsgCenterVo();
        msgCenterVo.setProblem("尊敬的用户您好，现于" + stationManagement.getName() + "新设充电桩" + code);
        msgCenterVo.setId(IdWorker.getId());
        msgCenterVo.setStatus(0);
        msgCenterVo.setType(2);
        msgCenterVo.setTitle("新增充电桩");
        asyncUtil.addMegCenter(stationManagement.getId(), code,msgCenterVo);



        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * 充电手册下载，调用minio模块下载到本地
     *
     * @param pileCode
     * @param request
     * @return
     */
    @Override
    public ResponseVO download(String pileCode, HttpServletRequest request) {
        minIoClient.downloadFileByName(pileCode, "avatar");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * 先删除桩成员，再将桩的user_id设为null并清除站点关联,最后下发通知
     *
     * @param pileCode
     * @param userId
     * @param request
     * @return
     */
    @Override
    public ResponseVO clearPile(String pileCode, Long userId, HttpServletRequest request) {
        PileManagement pileManagement = pileManagementMapper.queryPile(pileCode);
        if (!pileManagement.getUser_id().equals(userId)) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_ROLE, null, request.getSession().getAttribute("language").toString());
        }
        pileManagementMapper.delMember(pileManagement.getId());
        pileManagementMapper.updatePileMaster(pileManagement.getId(), userId);
        //发布通知


        //调用本逻辑类的添加Msg方法开始向系统通知表及其读取状态表中插入数据
        MsgCenterVo msgCenterVo = new MsgCenterVo();
        msgCenterVo.setProblem("尊敬的用户您好，现将充电桩" + pileCode + pileManagement.getName() + "从您名下解绑");
        msgCenterVo.setId(IdWorker.getId());
        msgCenterVo.setStatus(0);
        msgCenterVo.setType(2);
        msgCenterVo.setTitle("解绑充电桩");
        asyncUtil.addMegCenter(pileManagement.getStation_management_id(),pileCode,msgCenterVo);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description 设置桩的wifi，需跟桩联动，接口待确定
     * @param wifi
     * @param request
     * @return
     */
    @Override
    public ResponseVO setWifi(Wifi wifi, HttpServletRequest request) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     * 设置桩的ocpp，需跟桩联动，接口待确定
     * @param ocpp
     * @param request
     * @return
     */
    @Override
    public ResponseVO setOcpp(OCPP ocpp, HttpServletRequest request) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     * 请求此方法时获取服务器时间戳，并发送至桩设备
     * @param pileCode
     * @param request
     * @return
     */
    @Override
    public ResponseVO setTime(String pileCode, HttpServletRequest request) {
        long times = System.currentTimeMillis();
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }


//    @Override
//    public ResponseVO addMember(SysPileMember sysPileMember,HttpServletRequest request) {
////        System.out.println(request.getHeader("Authorization"));
//        //验证用户是否是该桩主账号
//        if (pileManagementMapper.checkMaster((Long) request.getSession().getAttribute("sysUserId"))==1){
//            return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
//        }
//        //验证用户是否已在关联用户中
//        if(pileManagementMapper.queryMember(sysPileMember)!=null){
//            return new ResponseVO();
//        }
//        sysPileMember.setId(IdWorker.getId());
//        pileManagementMapper.addMember(sysPileMember);
//
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
//    }
//
//    @Override
//    public ResponseVO delMember(Long userId, Long pileId, HttpServletRequest request) {
//        return null;
//    }

    /**
     * 添加系统通知方法，无controller接口
     *
     * @param
     * @return
     */
    public void addMegCenter(Long stationId, String code,MsgCenterVo msgCenterVo) throws BusinessException {
        imClient.saveUserId(stationId, code);
        List<Long> list = new Gson().fromJson(imClient.getKey("pile::station::userId::" + code).getData().toString(), List.class);
        msgCenterVo.setUserIds(list);
        //调用本逻辑类的添加Msg方法开始向系统通知表及其读取状态表中插入数据

        pileManagementMapper.addMsgCenter(msgCenterVo);
        pileManagementMapper.addUserMsg(msgCenterVo);
    }
}
