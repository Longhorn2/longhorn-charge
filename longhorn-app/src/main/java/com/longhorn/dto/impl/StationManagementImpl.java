package com.longhorn.dto.impl;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.dto.mapper.StationManagementMapper;
import com.longhorn.dto.service.StationManagementService;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.vo.StationManagementVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Service
public class StationManagementImpl implements StationManagementService {
    @Autowired
    StationManagementMapper stationManagementMapper;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;
    @Override
    public ResponseVO queryStation(StationManagementVO vo, HttpServletRequest request) {
        if (vo.getPageStart() == null || vo.getPageSize() == null) {
            vo.setPageStart(pageStart);
            vo.setPageSize(pageSize);
        }
        int pageStart=vo.getPageStart();
        vo.setPageStart((pageStart-1)*vo.getPageSize());
        System.out.println(vo.getDistance()+"传入的参数");
        List<StationManagement> stationManagements=stationManagementMapper.queryStation(vo);
        vo.setStationManagements(stationManagements);
        vo.setTotal(stationManagementMapper.getStationTotal(vo));
        vo.setPageStart(pageStart);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO stationInformation(Long stationId, HttpServletRequest request) {
        StationManagement stationManagement= stationManagementMapper.stationInformation(stationId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,stationManagement,request.getSession().getAttribute("language").toString());
    }
}
