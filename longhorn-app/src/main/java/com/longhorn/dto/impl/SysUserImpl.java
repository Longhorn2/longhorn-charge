package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.client.EmailClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.SysUserMapper;
import com.longhorn.client.MinIoClient;
import com.longhorn.model.dto.*;
import com.longhorn.dto.service.SysUserService;
import com.longhorn.model.utils.DesUtils;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.vo.AuthInfoVO;
import com.longhorn.model.vo.EmailVo;
import com.longhorn.model.vo.MsgCenterVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.*;

@Service
@Slf4j
public class SysUserImpl implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired(required = false)
    private MinIoClient minIoClient;
    @Autowired(required = false)
    EmailClient emailClient;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;

    @Override
    public ResponseVO queryById(SysUser sysUser, HttpServletRequest request) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, sysUserMapper.queryById(sysUser.getId()), request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO addUser(SysUser sysUser, HttpServletRequest request) {
        if (!Utils.isValidEmail(sysUser.getEmail())) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_Valid, null, request.getSession().getAttribute("language").toString());
        }
        if (!Utils.isValidPwd(sysUser.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid, null, request.getSession().getAttribute("language").toString());
        }

        if (sysUserMapper.queryByEmail(sysUser.getEmail()) != null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_REG, null, request.getSession().getAttribute("language").toString());
        }
        sysUser.setId(IdWorker.getId());
        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));
        sysUser.setRegister_time(new Timestamp(System.currentTimeMillis()));
        sysUser.setStatus(1);
        sysUser.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysUserMapper.addUser(sysUser);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description 用户登录
     */
    @Override
    public ResponseVO login(SysUser sysUser, HttpServletRequest request) {
        AuthInfoVO authInfoVo = new AuthInfoVO();
        authInfoVo.setTokenType("bearer");

        //登录验证
        SysUser sysUserInfo = sysUserMapper.queryByEmail(sysUser.getEmail());
        if (sysUserInfo == null) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        } else {
            if (!sysUser.getPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))) {
                return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR, null, request.getSession().getAttribute("language").toString());
            }
            if (sysUserInfo.getStatus() == 2) {
                sysUserMapper.updateStatus(sysUserInfo);
            }
            long times = System.currentTimeMillis();

            String token = sysUserInfo.getEmail() + "|" + sysUserInfo.getPwd() + "|" + times;
            authInfoVo.setAccessToken(DesUtils.encrypt(token));
            authInfoVo.setRefreshToken(DesUtils.encrypt(sysUserInfo.getEmail() + "|" + times));
            authInfoVo.setExpiresIn(DesUtils.EXPIRESIN);
            authInfoVo.setRefreshExpires(DesUtils.REFRESHEXPIRES);
            authInfoVo.setUserInfo(sysUserInfo);

            Map<String, Object> map = new HashMap<>();
            map.put("accessToken", authInfoVo.getAccessToken());
            map.put("refreshToken", authInfoVo.getRefreshToken());
            map.put("expiresIn", authInfoVo.getExpiresIn() + "");
            map.put("userinfo", new Gson().toJson(sysUserInfo));
            map.put("times", times + "");
            redisUtil.hmset(sysUserInfo.getEmail(), map);
            redisUtil.set("userinfo:" + sysUserInfo.getId().toString(), new Gson().toJson(sysUserInfo));
            return new ResponseVO(ErrorCodeEnum.SUCCESS, authInfoVo, request.getSession().getAttribute("language").toString());
        }
    }

    /**
     * 修改密码后更改token及相关信息，也可写进工具类
     *
     * @param sysUserInfo
     * @return
     * @throws BusinessException
     */
    public AuthInfoVO getTokenByUser(SysUser sysUserInfo) throws BusinessException {
        AuthInfoVO authInfoVo = new AuthInfoVO();
        authInfoVo.setTokenType("bearer");
        long times = System.currentTimeMillis();

        String token = sysUserInfo.getEmail() + "|" + sysUserInfo.getPwd() + "|" + times;
        authInfoVo.setAccessToken(DesUtils.encrypt(token));
        authInfoVo.setRefreshToken(DesUtils.encrypt(sysUserInfo.getEmail() + "|" + times));
        authInfoVo.setExpiresIn(DesUtils.EXPIRESIN);
        authInfoVo.setRefreshExpires(DesUtils.REFRESHEXPIRES);
        authInfoVo.setUserInfo(sysUserInfo);

        Map<String, Object> map = new HashMap<>();
        map.put("accessToken", authInfoVo.getAccessToken());
        map.put("refreshToken", authInfoVo.getRefreshToken());
        map.put("expiresIn", authInfoVo.getExpiresIn() + "");
        map.put("userinfo", new Gson().toJson(sysUserInfo));
        map.put("times", times + "");
        redisUtil.hmset(sysUserInfo.getEmail(), map);
        redisUtil.set("userinfo:" + sysUserInfo.getId().toString(), new Gson().toJson(sysUserInfo));
        return authInfoVo;
    }

//    /**
//     * @Description 修改密码前校验
//     * @param sysUser
//     * @return
//     */
//    @Override
//    public ResponseVO checkPassword(SysUser sysUser) {
//        SysUser sysUserInfo = sysUserMapper.queryByAll(sysUser);
//        log.info("对应用户信息:{}",new Gson().toJson(sysUserInfo));
//        if (!sysUser.getPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))){
//            return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR,null);
//        }
//        redisUtil.set(sysUserInfo.getEmail()+"oldPassword",sysUserInfo.getPwd(),600);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,true);
//    }

    /**
     * @param sysUser
     * @return
     * @Description 修改密码
     */
    @Override
    public ResponseVO changePassword(SysUser sysUser, HttpServletRequest request) {
        SysUser sysUserInfo = sysUserMapper.queryById(sysUser.getId());
        if (sysUser.getOldPwd().equals((sysUser.getPwd()))) {
            return new ResponseVO(ErrorCodeEnum.PASSWORD_CANNOT_IDENTICAL, null, request.getSession().getAttribute("language").toString());
        }
        if (!Utils.isValidPwd(sysUser.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid, null, request.getSession().getAttribute("language").toString());
        }
        if (!sysUser.getOldPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))) {
            return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR, null, request.getSession().getAttribute("language").toString());

        }
        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));
        sysUserMapper.changePassword(sysUser);
        sysUserInfo.setPwd(sysUser.getPwd());
        this.getTokenByUser(sysUserInfo);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());

    }

    /**
     * @param username
     * @param sex
     * @param country
     * @param id
     * @param file
     * @return
     * @Description修改个人信息及头像
     */
    @Override
    public ResponseVO changeInformation(String username, Integer sex, String birthday, String country, Long id, String code, MultipartFile file, HttpServletRequest request) {
//        final int AVATAR_MAX_SIZE = 10 * 1024 * 1024;
        String fileName = null;
        SysUser sysUser;
        if (redisUtil.hasKey("userinfo:" + id.toString())) {
            sysUser = new Gson().fromJson(redisUtil.get("userinfo:" + id), SysUser.class);
        } else {
            sysUser = sysUserMapper.queryById(id);
        }
        try {
            fileName = sysUser.getAvatar().substring(sysUser.getAvatar().lastIndexOf("=") + 1);
        } catch (Exception e) {
            fileName = null;
        }


        if (file == null || file.isEmpty()) {
            sysUserMapper.changeInformation(username, sex, birthday, country, id, code, null);
            redisUtil.del("userinfo:" + id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
        }
//        String fileName = file.getOriginalFilename();
//        String suffixName = fileName.substring(fileName.lastIndexOf("."));
//        if (!token.equals(redisUtil.hmget(sysUser.getEmail()).get("accessToken"))){
//            return new ResponseVO(ErrorCodeEnum.OFFSITELOGIN,null);
//        }
        //待了解这里能传什么类型的图片


//        if (file.getSize() > AVATAR_MAX_SIZE) {
//            return new ResponseVO(ErrorCodeEnum.IMAGE_SIZE_ERROR, null, request.getSession().getAttribute("language").toString());
//        }
        if (fileName != null && !fileName.isEmpty()) {

            minIoClient.delFileByFileName(fileName, "avatar");
        }
        String avatar = ((Map) ((minIoClient.uploadFileByName(file, "avatar").getData()))).get("fileName").toString();
        if (sysUserMapper.changeInformation(username, sex, birthday, country, id, code, avatar) == 0) {
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        }
        redisUtil.del("userinfo:" + id);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());

    }

    /**
     * 展示个人信息页面，在登录信息失效后调用，头像通过头像名调用minio模块查询
     *
     * @param sysUser
     * @param request
     * @return
     */
    @Override
    public ResponseVO showInformation(SysUser sysUser, HttpServletRequest request) {
        if (redisUtil.hasKey("userinfo:" + sysUser.getId())) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, new Gson().fromJson(redisUtil.get("userinfo:" + sysUser.getId()), SysUser.class), request.getSession().getAttribute("language").toString());
        }
        SysUser sysUserInfo = sysUserMapper.showInformation(sysUser);
        //       FileVo fileVo= new FileVo();

//        fileVo.setFileName(sysUserInfo.getAvatar());
//
//        if (sysUserInfo.getAvatar()!=null&&!sysUserInfo.getAvatar().isEmpty()){
//            String bucketName="avatar";
//            String url = ((Map)(minIoClient.getFileUrlByName(bucketName,fileVo).getData())).get("fileUrl").toString();
//            fileVo.setFileUrl(url);
//            sysUserInfo.setFileVo(fileVo);
//        }
        redisUtil.set("userinfo:" + sysUserInfo.getId(), new Gson().toJson(sysUserInfo).toString());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, sysUserInfo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 退出功能，退出时清空redis暂存的用户信息，并跳转登录页面
     *
     * @param sysUser
     * @param request
     * @return
     */
    @Override
    public ResponseVO logout(SysUser sysUser, HttpServletRequest request) {
        redisUtil.del("userinfo:" + sysUser.getId().toString());
        redisUtil.del(sysUser.getEmail());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }


    /**
     * @param sysUser
     * @return
     * @Description 注销:七天未登录注销账号，否则重新激活。注销申请后改变状态为注销，若在规定时间内登录则恢复状态为正常。开启定时扫描
     * 七天后无登录动作则改变状态为禁用
     */
    @Override
    public ResponseVO logOff(SysUser sysUser, HttpServletRequest request) {
        SysUser user;
        if (redisUtil.hasKey("userinfo:" + sysUser.getId().toString())) {
            user = new Gson().fromJson(redisUtil.get("userinfo:" + sysUser.getId().toString()), SysUser.class);
            sysUser.setType(user.getType());
        } else {
            user = sysUserMapper.queryById(sysUser.getId());
            sysUser.setType(user.getType());
        }
        if (sysUser.getType() == 1) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_LOGOFF, null, request.getSession().getAttribute("language").toString());
        }
        if (sysUserMapper.logOff(sysUser) != 0) {
            this.feedback(sysUser.getMsgCenter(), request);
            System.out.println(sysUser + "咋回事啊");
            redisUtil.del(user.getEmail());
            redisUtil.del("userinfo:" + sysUser.getId().toString());
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
        }

        return new ResponseVO(ErrorCodeEnum.USER_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description开启定时扫描，每天0点扫表将注销状态下且注销时间超过七天的用户改为禁用
     */
    @Override
    public ResponseVO cronFind(HttpServletRequest request) {
        List<SysUser> list = sysUserMapper.findOut(new Timestamp(System.currentTimeMillis()));
        List<Long> adminsId = new ArrayList<>();
        List<Long> usersId = new ArrayList<>();
        for (SysUser sysUer : list) {
            if (sysUer.getType() == 10) {
                adminsId.add(sysUer.getId());
            } else if (sysUer.getType() == 2) {
                usersId.add(sysUer.getId());
            }
        }
        if (!adminsId.isEmpty()) {
            sysUserMapper.realLogOof(adminsId);
        }
        if (!usersId.isEmpty()) {
            sysUserMapper.realDelete(usersId);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS, list, request.getSession().getAttribute("language").toString());
    }

    /**
     * 修改头像
     *
     * @param file
     * @param request
     * @return
     */
    @Override
    public ResponseVO updateAvatar(MultipartFile file, HttpServletRequest request) {
//        final int AVATAR_MAX_SIZE = 10 * 1024 * 1024;
        String fileName = null;
        SysUser sysUser = (SysUser) request.getSession().getAttribute("sysUserInfo");
//        if (redisUtil.hasKey("userinfo:"+request.getSession().getAttribute("sysUserId"))){
//            sysUser= new Gson().fromJson(redisUtil.get("userinfo:"+request.getSession().getAttribute("sysUserId")),SysUser.class);
//        }else {
//            sysUser = sysUserMapper.queryById((Long) request.getSession().getAttribute("sysUserId"));
//        }
        fileName = sysUser.getAvatar().substring(sysUser.getAvatar().lastIndexOf("=") + 1);
//        if (file.getSize()>AVATAR_MAX_SIZE){
//            return new ResponseVO(ErrorCodeEnum.IMAGE_SIZE_ERROR,null,request.getSession().getAttribute("language").toString());
//        }
        if (fileName != null && !fileName.isEmpty()) {
            minIoClient.delFileByFileName(fileName, "avatar");
        }
        String avatar = ((Map) ((minIoClient.uploadFileByName(file, "avatar").getData()))).get("fileName").toString();
        if (sysUserMapper.updateAvatar(avatar, sysUser.getId()) == 0) {
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        }
        redisUtil.del("userinfo:" + sysUser.getId());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());

    }

    /**
     * 修改除头像外的个人信息
     *
     * @param sysUser
     * @param request
     * @return
     */
    @Override
    public ResponseVO updateInformation(SysUser sysUser, HttpServletRequest request) {
        if (sysUserMapper.updateInformation(sysUser) == 0) {
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        }
        redisUtil.del("userinfo:" + sysUser.getId());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description 重新获取token
     */
    public ResponseVO getRefreshToken(AuthInfoVO info) throws BusinessException {
        log.info("请求数据:{}", new Gson().toJson(info.getRefreshToken()));
        ResponseVO responseVo = new ResponseVO();


        info.setTokenType("bearer");

        info.setExpiresIn(DesUtils.EXPIRESIN);
        info.setRefreshExpires(DesUtils.REFRESHEXPIRES);
        String str = DesUtils.decrypt(info.getRefreshToken());

        long times = System.currentTimeMillis();
        info.setAccessToken(DesUtils.encrypt(str + "|" + times));

        info.setRefreshToken(DesUtils.encrypt(str));

        Map<Object, Object> vo = redisUtil.hmget(str);

        Map<String, Object> map = new HashMap<>();
        map.put("accessToken", info.getAccessToken());
        map.put("refreshToken", info.getRefreshToken());
        map.put("expiresIn", info.getExpiresIn() + "");
        map.put("userinfo", vo.get("userinfo"));
        map.put("times", times + "");

        redisUtil.hmset(str, map);

        responseVo.setData(info);
        return responseVo;
    }

    /**
     * 查看我反馈的问题
     *
     * @param vo
     * @param request
     * @return
     */
    @Override
    public ResponseVO myFeedback(MsgCenterVo vo, HttpServletRequest request) {
        Map map = new HashMap<>();
//        if (redisUtil.hasKey("feedback:"+vo.getUser_id()+"feedback")){
//             map = redisUtil.hmget("feedback:"+vo.getUser_id()+"feedback");
//             List<MsgCenter> list = new ArrayList<>();
//            for (Object key:map.keySet()) {
//                list.add(new Gson().fromJson(map.get(key).toString(),MsgCenter.class));
//            }
//            return new ResponseVO(ErrorCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
//        }
        if (vo.getPageStart() == null || vo.getPageSize() == null) {
            vo.setPageStart(pageStart);
            vo.setPageSize(pageSize);
        }
        int pageStart = vo.getPageStart();
        vo.setPageStart((pageStart - 1) * pageSize);
        List<MsgCenter> msgCenterList = sysUserMapper.myFeedback(vo);
        vo.setMsgCenterList(msgCenterList);
        vo.setTotal(sysUserMapper.queryFeedTotal(vo));
        vo.setPageStart(pageStart);


//        redisUtil.hmset("feedback:"+vo.getUser_id()+"feedback",map);
//        redisUtil.hmget("feedback:"+vo.getUser_id()+"feedback");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 点击查看问题详情
     *
     * @param msgCenter
     * @param request
     * @return
     */
    @Override
    public ResponseVO checkFeedback(MsgCenter msgCenter, HttpServletRequest request) {
        if ((redisUtil.hasKey("feedback:" + msgCenter.getUser_id() + "feedback") && (redisUtil.hmget("feedback:" + msgCenter.getUser_id() + "feedback").containsKey(msgCenter.getId().toString())))) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, new Gson().fromJson(redisUtil.hmget("feedback:" + msgCenter.getUser_id() + "feedback").get(msgCenter.getId()).toString(), MsgCenter.class), request.getSession().getAttribute("language").toString());
        }
        MsgCenter msgCenterInfo = sysUserMapper.checkFeedback(msgCenter);
        if ((redisUtil.hasKey("feedback:" + msgCenter.getUser_id() + "feedback"))) {
            Map map = redisUtil.hmget("feedback:" + msgCenter.getUser_id() + "feedback");
            map.put(msgCenter.getId().toString(), new Gson().toJson(msgCenterInfo));
            redisUtil.hmset("feedback:" + msgCenter.getUser_id() + "feedback", map);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, msgCenterInfo, request.getSession().getAttribute("language").toString());
        }
        //此时redis中该用户的反馈问题内容已失效，是否跳转至未进入myFeedback页面？
        return new ResponseVO(ErrorCodeEnum.SUCCESS, msgCenterInfo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 添加反馈问题
     *
     * @param msgCenter
     * @param request
     * @return
     */
    @Override
    public ResponseVO feedback(MsgCenter msgCenter, HttpServletRequest request) {
        if (msgCenter == null || msgCenter.getProblem() == null || msgCenter.getProblem().isEmpty()) {
            return new ResponseVO(ErrorCodeEnum.FEEDBACK_ERROR, null, request.getSession().getAttribute("language").toString());
        }
        msgCenter.setId(IdWorker.getId());
        sysUserMapper.feedback(msgCenter);
        redisUtil.del("feedback:" + msgCenter.getUser_id() + "feedback");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * 系统通知接口
     *
     * @param request
     * @return
     */
    @Override
    public ResponseVO showNotification(MsgCenterVo msgCenterVo, HttpServletRequest request) {

        if (msgCenterVo.getPageStart() == null || msgCenterVo.getPageSize() == null) {
            msgCenterVo.setPageStart(pageStart);
            msgCenterVo.setPageSize(pageSize);
        }

        int pageStart = msgCenterVo.getPageStart();
        msgCenterVo.setPageStart((pageStart - 1) * msgCenterVo.getPageSize());
        List<MsgCenter> msgCenters = sysUserMapper.showNotification(msgCenterVo);
        msgCenterVo.setMsgCenterList(msgCenters);
        msgCenterVo.setUnreadNum(sysUserMapper.queryUnreadNum(msgCenterVo));
        msgCenterVo.setPageStart(pageStart);
        msgCenterVo.setTotal(sysUserMapper.queryMsgTotal(msgCenterVo));

        return new ResponseVO(ErrorCodeEnum.SUCCESS, msgCenterVo, request.getSession().getAttribute("language").toString());
    }

    /**
     * 生成六位数随机验证码，调用email模块发送至前端，同时存入数据库
     *
     * @param email
     * @param request
     * @return
     */
    @Override
    public ResponseVO getEmailCode(String email, HttpServletRequest request) {
        SysEmailCode sysEmailCode = new SysEmailCode();
        sysEmailCode.setCode((int) ((Math.random() * 9 + 1) * 100000));
        EmailVo vo = new EmailVo();
        vo.setToEmail(email);
        vo.setTitle("您的邮箱验证码");
        vo.setMessage(sysEmailCode.getCode().toString());
        emailClient.sendMail(vo);
        sysEmailCode.setId(IdWorker.getId());
        sysEmailCode.setEmail(email);
        sysUserMapper.setEmailCode(sysEmailCode);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param sysEmailCode
     * @param request
     * @return
     * @Description 通过邮箱验证码改密，验证码五分钟未使用过期
     */
//    @Override
//    public ResponseVO updatePwdByEmail(SysEmailCode sysEmailCode, HttpServletRequest request) {
//        SysUser sysUser = sysUserMapper.checkCode(sysEmailCode);
//        if (sysUser==null){
//            return new ResponseVO(ErrorCodeEnum.EMAIL_CODE_ERROR,null,request.getSession().getAttribute("language").toString());
//        }
//        if (!Utils.isValidPwd(sysEmailCode.getPwd())) {
//            return new ResponseVO(ErrorCodeEnum.PWD_Valid,null,request.getSession().getAttribute("language").toString());
//        }
//        if (sysUser.getPwd().equals(DesUtils.encrypt(sysEmailCode.getPwd()))){
//            return new ResponseVO(ErrorCodeEnum.PASSWORD_CANNOT_IDENTICAL,null,request.getSession().getAttribute("language").toString());
//        }
//        sysUser.setPwd(DesUtils.encrypt(sysEmailCode.getPwd()));
//        sysUserMapper.changePassword(sysUser);
//        sysUserMapper.updateEmailStatus(sysEmailCode);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
//    }
    @Override
    public ResponseVO updatePwdByEmail(SysEmailCode sysEmailCode, HttpServletRequest request) {
        SysEmailCode emailCode = sysUserMapper.checkCode(sysEmailCode.getEmail(), sysEmailCode.getCode());
        if (emailCode == null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_CODE_ERROR, null, request.getSession().getAttribute("language").toString());
        }
        if (!Utils.isValidPwd(sysEmailCode.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid, null, request.getSession().getAttribute("language").toString());
        }
        SysUser sysUser = sysUserMapper.queryByEmail(sysEmailCode.getEmail());
        if (sysUser.getPwd().equals(DesUtils.encrypt(sysEmailCode.getPwd()))) {
            return new ResponseVO(ErrorCodeEnum.PASSWORD_CANNOT_IDENTICAL, null, request.getSession().getAttribute("language").toString());
        }
        sysUser.setPwd(DesUtils.encrypt(sysEmailCode.getPwd()));
        sysUserMapper.changePassword(sysUser);
        sysUserMapper.updateEmailStatus(sysEmailCode.getEmail(), sysEmailCode.getCode());
        redisUtil.del(sysUser.getEmail());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param sysEmailCode
     * @param request
     * @return
     * @Description 通过验证码换绑邮箱, 验证新邮箱是否已绑定
     */
    public ResponseVO updateEmail(SysEmailCode sysEmailCode, HttpServletRequest request) {
        SysUser user = sysUserMapper.queryByEmail(sysEmailCode.getNewEmail());
        if (user != null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_IS_USING, null, request.getSession().getAttribute("language").toString());
        }
        SysEmailCode emailCode = sysUserMapper.checkCode(sysEmailCode.getNewEmail(), sysEmailCode.getCode());
        if (emailCode == null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_CODE_ERROR, null, request.getSession().getAttribute("language").toString());
        }
        SysUser sysUser = sysUserMapper.queryByEmail(sysEmailCode.getEmail());
        sysUser.setEmail(sysEmailCode.getNewEmail());
        if (sysUser == null) {
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND, null, request.getSession().getAttribute("language").toString());
        }
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");
        if (!sysUser.getId().toString().equals(sysUserInfo.getId().toString())) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_BELONG_OTHER, null, request.getSession().getAttribute("language").toString());
        }

        sysUserMapper.updateEmail(sysUser);
        sysUserMapper.updateEmailStatus(sysEmailCode.getNewEmail(), sysEmailCode.getCode());
        redisUtil.del(sysEmailCode.getEmail());
        redisUtil.del("userinfo:" + sysUserInfo.getId().toString());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param msgCenter
     * @param request
     * @return
     * @Description 点击查看详情并从未读标记为已读
     */
    @Override
    public ResponseVO checkNotification(MsgCenter msgCenter, HttpServletRequest request) {
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");

        sysUserMapper.updateMsgStatus(msgCenter.getId(), msgCenter.getStatus(), sysUserInfo.getId());
        //           redisUtil.del("notification:"+(Long) request.getSession().getAttribute("sysUserId")+"notification");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }

    /**
     * @param request
     * @return
     * @Description 一键查看未读
     */
    @Override
    public ResponseVO readAll(HttpServletRequest request) {
        SysUser sysUserInfo = (SysUser) request.getSession().getAttribute("sysUserInfo");
        sysUserMapper.readAll(sysUserInfo.getId());
//        redisUtil.del("notification:"+(Long) request.getSession().getAttribute("sysUserId")+"notification");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null, request.getSession().getAttribute("language").toString());
    }


}