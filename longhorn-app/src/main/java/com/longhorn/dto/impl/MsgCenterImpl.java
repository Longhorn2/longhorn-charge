package com.longhorn.dto.impl;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.dto.mapper.MsgCenterMapper;
import com.longhorn.dto.service.MsgCenterService;
import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.vo.MsgCenterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class MsgCenterImpl implements MsgCenterService {
    @Autowired
    MsgCenterMapper msgCenterMapper;
    @Value("${page.pageStart}")
    Integer pageStart;
    @Value("${page.pageSize}")
    Integer pageSize;

    /**
     * 通过联查出中间表user_id为自己和为null的情况确定系统通知已读未读状况
     * @param vo
     * @param request
     * @return
     */
    @Override
    public ResponseVO showNotification(MsgCenterVo vo, HttpServletRequest request) {
        if (vo.getPageStart() == null || vo.getPageSize() == null) {
            vo.setPageStart(pageStart);
            vo.setPageSize(pageSize);
        }
        int pageStart=vo.getPageStart();
        vo.setPageStart((pageStart-1)*vo.getPageSize());
        vo.setMsgCenterList(msgCenterMapper.showNotification(vo));
        vo.setTotal(msgCenterMapper.getTotal(vo));
        vo.setPageStart(pageStart);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }

    /**
     * 通过前端传递过来的MsgCenter对象参数通过其中status执行判断，若为null或空字符串则在中间表添加已读数据，若为0则修改中间表status字段状态
     * @param msgCenter
     * @param request
     * @return
     */
    @Override
    public ResponseVO checkNotification(MsgCenter msgCenter, HttpServletRequest request) {
        List<MsgCenter> msgCenterAdd = new ArrayList<>();
        List<MsgCenter> msgCenterUpdate = new ArrayList<>();
        if (msgCenter.getStatus()==null){
            msgCenterAdd.add(msgCenter);
            msgCenterMapper.addUserMsg(msgCenterAdd);
        } else if (msgCenter.getStatus()==0) {
            msgCenterUpdate.add(msgCenter);
            msgCenterMapper.updateStatus(msgCenterUpdate);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     *  一键已读，也分两种情况，1.status为null，则向状态表里添加状态为1的数据，若为0，则将状态表中的status由0改为1
     * @param request
     * @return
     */
    @Override
    public ResponseVO readAll(List<MsgCenter> msgCenterList,HttpServletRequest request) {
        List<MsgCenter> msgCenterAdd = new ArrayList<>();
        List<MsgCenter> msgCenterUpdate = new ArrayList<>();
        for (MsgCenter msg:msgCenterList) {
            if (msg.getStatus()==null){
                msgCenterAdd.add(msg);
            } else if (msg.getStatus()==0) {
                msgCenterUpdate.add(msg);
            }
        }
        if (!msgCenterAdd.isEmpty()){
            msgCenterMapper.addUserMsg(msgCenterAdd);
        }
        if (!msgCenterUpdate.isEmpty()){
            msgCenterMapper.updateStatus(msgCenterUpdate);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }


}
