package com.longhorn.dto.impl;


import com.google.gson.Gson;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.dto.mapper.HelpCenterMapper;
import com.longhorn.dto.service.HelpCenterService;
import com.longhorn.model.dto.HelpCenter;
import com.longhorn.model.dto.HelpCenterType;
import com.longhorn.model.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Service
@Slf4j
public class HelpCenterImpl implements HelpCenterService {
    @Autowired
    private HelpCenterMapper pileOwnerMapper;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 查看具体帮助手册答案
     * @param id
     * @param request
     * @return
     */
    @Override
    public ResponseVO findAnswer(Long id,HttpServletRequest request) {
        if (redisUtil.hasKey("helpCenter:"+id.toString())){
            HelpCenter helpCenter = new Gson().fromJson(redisUtil.get("helpCenter:"+id), HelpCenter.class);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,helpCenter,request.getSession().getAttribute("language").toString());
        }
        if (pileOwnerMapper.findAnswer(id)==null){
            return new ResponseVO(ErrorCodeEnum.RESOURCE_NOT_FOUND,null,request.getSession().getAttribute("language").toString());
        }
        HelpCenter helpCenter= pileOwnerMapper.findAnswer(id);
        String jsonObjectStr = new Gson().toJson(helpCenter).toString();
        redisUtil.set("helpCenter:"+id,jsonObjectStr);
//        redisUtil.set(title,helpCenter);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,helpCenter,request.getSession().getAttribute("language").toString());
    }

    /**
     * 查看帮助中心分类
     * @param userId
     * @param request
     * @return
     */
    @Override
    public ResponseVO findHelpType(Long userId,HttpServletRequest request) {
        List<HelpCenterType> helpCenterTypes = pileOwnerMapper.findHelpType(userId);
//        for (HelpCenter helpCenter:helpCenters) {
//            String jsonObjectStr = JSONObject.toJSONString(helpCenter);
//            redisUtil.set(helpCenter.getTitle(),jsonObjectStr);
////            redisUtil.set(helpCenter.getTitle(),helpCenter);
//        }
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,pileOwnerMapper.findTitle(),request.getSession().getAttribute("language").toString());
        return new ResponseVO(ErrorCodeEnum.SUCCESS,helpCenterTypes,request.getSession().getAttribute("language").toString());
    }

    /**
     * 查看每类下的标题
     * @param typeId
     * @param userId
     * @param request
     * @return
     */
    @Override
    public ResponseVO findTitle(Long typeId, Long userId, HttpServletRequest request) {
        List<HelpCenter> helpCenters=pileOwnerMapper.findTitle(typeId,userId);
        for (HelpCenter helpCenter:helpCenters) {
            redisUtil.set("helpCenter:"+helpCenter.getId(),new Gson().toJson(helpCenter).toString());
            System.out.println(redisUtil.get("helpCenter:"+helpCenter.getId()));
       }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,helpCenters,request.getSession().getAttribute("language").toString());
    }


}
