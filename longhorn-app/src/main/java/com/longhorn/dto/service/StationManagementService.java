package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.vo.StationManagementVO;

import javax.servlet.http.HttpServletRequest;

public interface StationManagementService {
    ResponseVO queryStation(StationManagementVO stationManagement, HttpServletRequest request);

    ResponseVO stationInformation(Long stationId, HttpServletRequest request);
}
