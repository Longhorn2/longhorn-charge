package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.ocpp1_6.dot.CancelReservation;
import com.longhorn.common.ocpp1_6.dot.RemoteStartTransaction;
import com.longhorn.common.ocpp1_6.dot.ReserveNow;
import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.vo.SysChargeOrderVo;

import javax.servlet.http.HttpServletRequest;

public interface SysChargeOrderService {
    ResponseVO chargeHistory(SysChargeOrderVo vo, HttpServletRequest request);

    ResponseVO chargingTrends(String pileCode,HttpServletRequest request);

    ResponseVO startCharge(SysChargeOrder sysChargeOrder,HttpServletRequest request);

    ResponseVO stopCharge(SysChargeOrder sysChargeOrder,HttpServletRequest request);

    ResponseVO showChargeData(Long orderId, HttpServletRequest request);

    ResponseVO sendRemoteStartTransaction(RemoteStartTransaction remoteStartTransaction,String pileCode, HttpServletRequest request);

    ResponseVO requestChargingPileInfo(String pileCode, HttpServletRequest request);

    ResponseVO queryMeterValues(String pileCode, Integer connectorId,HttpServletRequest request);
}
