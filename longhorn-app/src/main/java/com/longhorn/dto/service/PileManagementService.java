package com.longhorn.dto.service;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.PileManagementVo;
import com.longhorn.model.vo.SysChargeOrderVo;

import javax.servlet.http.HttpServletRequest;

public interface PileManagementService {
    ResponseVO bind(String code, SysPileMember sysPileMember, HttpServletRequest request) throws BusinessException;

    ResponseVO pileInformation(PileManagementVo vo, HttpServletRequest request);

    ResponseVO updatePileInformation(PileManagement pileManagement, HttpServletRequest request);

    ResponseVO upgrade(PileManagement pileManagement, HttpServletRequest request);

    ResponseVO isPNP(Boolean PNP,String pileCode,HttpServletRequest request);

//    ResponseVO history(SysChargeOrderVo vo, HttpServletRequest request);
//
//    ResponseVO startCharge(SysChargeOrder order, HttpServletRequest request);
//
//    ResponseVO chargingTrends(String pileCode, Integer time, HttpServletRequest request);

    ResponseVO checkBindCode(String code, HttpServletRequest request);

    ResponseVO binding(String code, StationManagement stationManagement,HttpServletRequest request);

    ResponseVO download(String pileCode, HttpServletRequest request);

    ResponseVO clearPile(String pileCode, Long userId, HttpServletRequest request);

    ResponseVO setWifi(Wifi wifi, HttpServletRequest request);

    ResponseVO setOcpp(OCPP ocpp, HttpServletRequest request);

    ResponseVO setTime(String pileCode, HttpServletRequest request);


//    ResponseVO addMember(SysPileMember sysPileMember,HttpServletRequest request);
//    ResponseVO delMember(Long userId, Long pileId, HttpServletRequest request);
}
