package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysPileMember;
import com.longhorn.model.vo.SysPileMemberVo;

import javax.servlet.http.HttpServletRequest;

public interface SysPileMemberService {
    ResponseVO addMember(SysPileMember sysPileMember, HttpServletRequest request);

    ResponseVO delMember(String email, Long pileId, HttpServletRequest request);

    ResponseVO showMember(SysPileMemberVo sysPileMemberVo, HttpServletRequest request);
    ResponseVO updateNickname(Long id, String nickname, HttpServletRequest request);

}
