package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.MsgCenterVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MsgCenterService {

    ResponseVO showNotification(MsgCenterVo vo, HttpServletRequest request);

    ResponseVO checkNotification(MsgCenter msgCenter, HttpServletRequest request);

    ResponseVO readAll(List<MsgCenter> msgCenterList,HttpServletRequest request);
}
