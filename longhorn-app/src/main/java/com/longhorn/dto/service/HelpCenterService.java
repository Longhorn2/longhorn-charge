package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;

import javax.servlet.http.HttpServletRequest;

public interface HelpCenterService {
    ResponseVO findAnswer(Long id,HttpServletRequest request);

    ResponseVO findHelpType(Long userId,HttpServletRequest request);

    ResponseVO findTitle(Long typeId, Long userId, HttpServletRequest request);


}
