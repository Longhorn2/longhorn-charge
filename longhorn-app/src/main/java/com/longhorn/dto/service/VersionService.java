package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.PileManagement;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

public interface VersionService {
    ResponseVO queryAppVersion(String version, Integer type, HttpServletRequest request);

    ResponseVO queryPileVersion(String version, Integer type, HttpServletRequest request);

    ResponseVO upgrade(PileManagement pileManagement, HttpServletRequest request);
}
