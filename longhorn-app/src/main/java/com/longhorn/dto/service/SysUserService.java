package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.MsgCenter;
import com.longhorn.model.dto.MsgConfig;
import com.longhorn.model.dto.SysEmailCode;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.MsgCenterVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public interface SysUserService {
    ResponseVO queryById(SysUser sysUser,HttpServletRequest request);
    ResponseVO addUser(SysUser sysUser,HttpServletRequest request);
    ResponseVO login(SysUser sysUser,HttpServletRequest request);

 //   ResponseVO checkPassword(SysUser sysUser);

    ResponseVO changePassword(SysUser sysUser,HttpServletRequest request);


    ResponseVO logOff(SysUser sysUser,HttpServletRequest request);

    ResponseVO changeInformation(String username, Integer sex, String birthday, String country, Long id, String code, MultipartFile file, HttpServletRequest request);

    ResponseVO logout(SysUser sysUser,HttpServletRequest request);

    ResponseVO showInformation(SysUser sysUser, HttpServletRequest request);

    ResponseVO myFeedback(MsgCenterVo vo, HttpServletRequest request);

    ResponseVO checkFeedback(MsgCenter msgCenter, HttpServletRequest request);

    ResponseVO feedback(MsgCenter msgCenter,HttpServletRequest request);

    ResponseVO showNotification(MsgCenterVo msgCenterVo, HttpServletRequest request);

    ResponseVO getEmailCode(String email, HttpServletRequest request);

    ResponseVO updatePwdByEmail(SysEmailCode sysEmailCode, HttpServletRequest request);

    ResponseVO updateEmail(SysEmailCode sysEmailCode, HttpServletRequest request);

    ResponseVO checkNotification(MsgCenter msgCenter, HttpServletRequest request);

    ResponseVO readAll(HttpServletRequest request);
    ResponseVO cronFind(HttpServletRequest request);

    ResponseVO updateAvatar(MultipartFile file, HttpServletRequest request);

    ResponseVO updateInformation(SysUser sysUser, HttpServletRequest request);
}
