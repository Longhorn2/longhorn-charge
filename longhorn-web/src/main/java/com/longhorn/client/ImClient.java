package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.vo.FileVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@FeignClient(value = "longhorn-im")
public interface ImClient {
    //上传文件
    @GetMapping(value = "/api/v1/longhorn-im/sysUser/saveUserId/{stationId}/{pileCode}")
    ResponseVO saveUserId(@PathVariable("stationId") Long stationId,@PathVariable("pileCode")String pileCode) throws BusinessException;

}
