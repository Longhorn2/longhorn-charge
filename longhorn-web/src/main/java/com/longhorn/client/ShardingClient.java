package com.longhorn.client;

import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "longhorn-sharding")
public interface ShardingClient {
    @ApiOperation("saas充电过程查询")
    @RequestMapping(value = "/api/v1/longhorn-sharding/sharding/saasChargeData",method = RequestMethod.GET)
    ResponseVO saasChargeData(@RequestParam("orderId")Long orderId, HttpServletRequest request) throws BusinessException;
}
