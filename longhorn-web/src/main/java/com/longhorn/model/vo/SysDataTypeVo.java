package com.longhorn.model.vo;


import com.longhorn.model.dto.SysDataType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysDataTypeVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private Integer p;

    private Integer size;

    private String index;


    private Integer total;
    private List<SysDataType> list;
}
