package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysUserAuthVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户登录id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    private String user_name;


}
