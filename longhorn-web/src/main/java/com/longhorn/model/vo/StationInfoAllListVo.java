package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.StationManagement;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StationInfoAllListVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 站点名称 id
     * length 0~20
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 站点名称
     * */
    private String name;




}
