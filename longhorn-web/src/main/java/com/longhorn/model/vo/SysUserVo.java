package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysUserVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户登录id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    private Integer p;

    private Integer size;

    private String user_name;

    private String phone;

    private Integer total;

    private List<SysUser> userInfoList;

}
