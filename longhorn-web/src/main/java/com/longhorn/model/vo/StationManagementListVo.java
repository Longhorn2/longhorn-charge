package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.StationImg;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.dto.SysDataType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StationManagementListVo extends StationManagement implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;


    /**
     * 站点管理——站点特色(数据字典id)
     * */
    private List<SysDataType> stationCharacteristicList;

    /**
     * 站点管理——充电口类型(数据字典id)
     * */
    private List<SysDataType> stationChargeTypeList;

    /**
     * 站点管理——照片
     * */
    private List<StationImg> stationImgList;



}
