package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysChargeOrder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysChargeOrderVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     *  订单号
     */
    private String orderId;

    /**
     *  开始创建时间
     */
    private String startCreateTime;
    /**
     *  结束创建时间
     */
    private String endCreateTime;

    /**
     *  开始充电时间
     */
    private String startTime;
    /**
     *  结束充电时间
     */
    private String endTime;
    /**
     *  订单来源
     */
    private String source;

    /**
     *  启动方式  1=扫码启动 2=插枪自动启动 3=定时启动
     * 4.刷卡启动 5.vin自动充电 6.saas(web)远程启动 7.蓝牙启动
     */
    private Integer startMethod;

    /**
     * 运营商
     * length 0~50
     */
    private String operator;
    /**
     * 品牌商
     * length 0~50
     */
    private String brand_owner;
    /**
     * 代理商
     * length 0~50
     */
    private String agent;

    /**
     * 站点id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;

    /**
     * 设备编号
     */
    private String pileCode;

    /**
     * 交直模式  交流  直流
     */
    private String mode;

    /**
     * 资产归属方
     * */
    private String vesting_party;

    /**
     * 邮箱
     * */
    private String email;

    /**
     * 是否正常停止 1是 2 否
     * */
    private Integer isNormal;

    /**
     * 服务商
     * length 0~50
     */
    private String service_provider;

    /**
     * 车牌号
     */
    private String car_number;

    /**
     * VIN码
     */
    private String vin;
    /**
     * 电卡编号
     */
    private String card_code;
    /**
     * 姓名
     */
    private String userName;
    /**
     * 订单状态 默认0=启动中  1=充电中 2=停止中 3=结算中 4=已完结
     */
    private Integer status;




    private Integer p;

    private Integer size;

    private Integer total;

    /**
     * 用户id 集合
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> ids;


    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;

    private List<SysChargeOrderListVo> list;
}
