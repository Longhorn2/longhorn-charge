package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class RoleMenuVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     *  角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long role_id;

    /**
     *  菜单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] menu_id;

}
