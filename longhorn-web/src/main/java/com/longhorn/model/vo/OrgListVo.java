package com.longhorn.model.vo;

import com.longhorn.model.dto.SysOrg;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OrgListVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    List<SysOrg> list;
    OrgListVo node;
}
