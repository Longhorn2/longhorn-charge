package com.longhorn.model.vo;

import com.longhorn.model.dto.SysAlarm;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysAlarmListVo extends SysAlarm implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 站点名称
     * length 0~30
     */
    private String stationName;
    /**
     * 设备名称
     * length 0~30
     */

    private String pileName;
    /**
     * 设备型号
     */
    private String model;

}
