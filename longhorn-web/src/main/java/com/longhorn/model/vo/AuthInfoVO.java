package com.longhorn.model.vo;

import com.longhorn.model.dto.SysUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author gmding
 * @Description 授权返回实体类
 * @date 2023-5-09
 * */
@Data
@NoArgsConstructor
public class AuthInfoVO implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * token类别
     */
    private String tokenType;

    /**
     * access_token有效期
     */
    private Integer expiresIn;

    /**
     * refresh_token有效期
     */
    private Integer refreshExpires;

    /**
     * token
     */
    private String accessToken;
    /**
     * 刷新token
     *
     */
    private String refreshToken;

    /**
     * 用户实体类
     */
    private SysUser userInfo;



}
