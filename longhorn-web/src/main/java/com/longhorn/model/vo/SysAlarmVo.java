package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.StationManagement;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysAlarmVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private Integer p;

    private Integer size;
    private Integer total;

    /**
     * 运营商
     * length 0~50
     */
    private String operator;

    /**
     * 站点 id
     * length 0~20
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 设备名称
     * */
    private String name;
    /**
     * 设备编号
     * */
    private String pile_code;
    /**
     * 设备型号
     * */
    private String model;


    List<SysAlarmListVo> list;



}
