package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class StationListByOrgVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;

//    /**
//     * 国家
//     * */
//    private String country;
//    /**
//     * 省
//     * */
//    private String province;
//    /**
//     * 市
//     * */
//    private String city;

    /**
     * 区
     * */
    private String area;

    /**
     * 站点名称
     * */
    private String stationName;

}
