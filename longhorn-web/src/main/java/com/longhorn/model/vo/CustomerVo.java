package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CustomerVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private Integer p;

    private Integer size;

    private String country;

    private String province;

    private String customer_type;

    private Integer status;

    private Integer sex;

    private String email;

    private Integer total;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    private List<SysUser> userInfoList;

}
