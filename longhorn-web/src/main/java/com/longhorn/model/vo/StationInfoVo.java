package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.StationManagement;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StationInfoVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private Integer p;

    private Integer size;

    /**
     * 运营商
     * length 0~50
     */
    private String operator;
    /**
     * 品牌商
     * length 0~50
     */
    private String brand_owner;
    /**
     * 代理商
     * length 0~50
     */
    private String agent;
    /**
     * 服务商
     * length 0~50
     */
    private String service_provider;

    /**
     * 站点名称 id
     * length 0~20
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 国家
     * */
    private String country;
    /**
     * 省
     * */
    private String province;

    private Integer total;


    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long>userIds;

    List<StationManagement> list;



}
