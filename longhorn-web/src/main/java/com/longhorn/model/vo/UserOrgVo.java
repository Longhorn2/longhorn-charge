package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 设置用户组织请求实体类
 * */

@Data
public class UserOrgVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 组织id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] org_id;

    /**
     * 站点 id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] station_management_id;
}
