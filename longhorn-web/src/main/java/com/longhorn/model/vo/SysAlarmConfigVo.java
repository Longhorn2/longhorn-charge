package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysAlarmConfig;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysAlarmConfigVo  implements Serializable {
        private static final long serialVersionUID = 1L;

        private Integer p;

        private Integer size;

        /**
         * 联系电话
         * */
        private String contact_phone;
        /**
         * 邮箱
         * */
        private String email;

        /**
         * 站点id
         */
        @JsonSerialize(using= ToStringSerializer.class)
        private Long station_management_id;

        private Integer total;

        List<SysAlarmConfig> list;

}
