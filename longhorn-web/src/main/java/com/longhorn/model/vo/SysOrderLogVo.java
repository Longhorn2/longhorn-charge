package com.longhorn.model.vo;

import com.longhorn.model.dto.SysOrderLog;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysOrderLogVo extends SysOrderLog implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    private String userName;
}
