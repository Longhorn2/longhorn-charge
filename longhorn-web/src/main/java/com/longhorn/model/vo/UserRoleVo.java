package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.io.Serializable;

/**
 * 用户角色请求实体类
 * */

@Data
public class UserRoleVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] role_id;
}
