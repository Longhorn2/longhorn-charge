package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.StationManagement;
import lombok.Data;

import java.io.Serializable;

@Data
public class StationManagementVo implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;


    private StationManagement stationManagement;

    /**
     * 站点管理——站点特色(数据字典id)
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] stationCharacteristicId;

    /**
     * 站点管理——充电口类型(数据字典id)
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] stationChargeTypeId;
    /**
     * 站点管理——照片
     * */
    private String[] stationImg;



}
