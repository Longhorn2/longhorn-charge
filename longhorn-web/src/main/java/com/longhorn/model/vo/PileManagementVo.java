package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.PileManagement;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PileManagementVo extends PileManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer p;

    private Integer size;

    private Integer total;

    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long>userIds;

    List<PileManagement> list;

}
