package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.longhorn.model.dto.SysChargeOrder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysChargeOrderListVo extends SysChargeOrder {

    private String userName;

    private String stationName;

    /**
     * 运营商
     * length 0~50
     */
    private String operator;
    /**
     * 品牌商
     * length 0~50
     */
    private String brand_owner;
    /**
     * 服务商
     * length 0~50
     */
    private String service_provider;

    /**
     * 代理商
     * length 0~50
     */
    private String agent;

    private String email;

    /**
     * 交直模式  交流  直流
     */
    private String mode;

    /**
     * 资产归属方
     * */
    private String vesting_party;
    /**
     * 充电桩名称
     * */
    private String pileName;
    /**
     * 性别  0=女  1=男
     * */
    private Integer sex;

    /**
     * 联系电话
     * */
    private String  contact_phone;

    /**
     * 号码区号 (例+86)
     * */
    private String  contact_phone_code;
    /**
     * 详细地址
     * */
    private String  address;




}
