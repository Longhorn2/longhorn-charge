package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统用户站点管理表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class SysUserStationManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 权限id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 站点id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;
}
