package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统用户组织表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class SysUserOrg implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 权限id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 组织id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long org_id;
}
