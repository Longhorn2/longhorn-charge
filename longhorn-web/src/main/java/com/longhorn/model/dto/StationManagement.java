package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 站点管理
 * */
@Data
public class StationManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 站点名称
     * length 0~30
     */
    private String name;
    /**
     * 运营商
     * length 0~50
     */
    private String operator;
    /**
     * 品牌商
     * length 0~50
     */
    private String brand_owner;
    /**
     * 代理商
     * length 0~50
     */
    private String agent;
    /**
     * 服务商
     * length 0~50
     */
    private String service_provider;
    /**
     * 站点状态  对应sys_data_type数据字典id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long site_status;
    /**
     * 建设场所 对应sys_data_type数据字典id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long construction_site;
    /**
     * 运营时间
     * */
    private String first_operation_time;
    /**
     * 营业时间  json字符串
     * */
    private String business_hours;
    /**
     * 联系人
     * 0~30
     */
    private String contact_person;
    /**
     * 联系电话 0~20
     * */
    private String contact_phone;
    /**
     * 站点描述
     * */
    private String station_descript;
    /**
     * 国家
     * */
    private String country;
    /**
     * 省
     * */
    private String province;
    /**
     * 市
     * */
    private String city;
    /**
     * 区
     * */
    private String area;
    /**
     * 详细地址
     * */
    private String address;
    /**
     * 经度
     * */
    private String longitude;
    /**
     * 纬度
     * */
    private String latitude;
    /**
     * 操作人id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;

    private Timestamp create_time;

    private Timestamp update_time;

    private String station_code;

    public void setStation_code(String station_code){
        String m=station_code;
        for (int i=0;i<6-station_code.length();i++){
            m="0"+m;
        }
        this.station_code=m;
    }


    /**
     * 站点状态
     * */
    private String siteStatusName;
    /**
     * 建设场所
     * */
    private String constructionSiteName;

    /**
     * 添加人
     * */
    private String userName;
    /**
     * 桩数量
     * */
    private Integer pileNum;
    /**
     * 枪数量
     * */
    private Integer gunNum;

    /**
     * 总功率
     * */
    private Integer powerTotal;

    /**
     * 门牌号
     * */
    private String house_number;
    /**
     * 邮编
     * */
    private String zip_code;

    /**
     * 号码区号 (例+86)
     * */
    private String contact_phone_code;



}
