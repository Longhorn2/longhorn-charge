package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

/**
 * 充电桩管理
 * */
@Data
public class PileManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 设备名称
     * length 0~50
     */
    private String name;
    /**
     * 站点id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;

    /**
     * 协议管理表id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long protocol_management_id;
    /**
     * 设备编号
     * */
    private String pile_code;
    /**
     * 交直模式
     * */
    private String mode;
    /**
     * 设备型号
     * */
    private String model;

    /**
     * 状态 1=未安装 2=安装中 3=使用中 4=维修/维护 5=报废
     * */
    private Integer status;

    /**
     * 枪数
     * */
    private Integer gun_num;
    /**
     * 最大功率kW 0~10000
     * */
    private Integer max_power;

    /**
     * 资产归属方
     * */
    private String vesting_party;
    /**
     * 国标类型
     * */
    private String standard_type;
    /**
     * 充电口类型
     * */
    private String charge_type;
    /**
     * 质保日期
     * */
    private String warranty_date;
    /**
     * 生产日期
     * */
    private String production_date;

    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;


    private Timestamp create_time;

    private Timestamp update_time;

    /**
     * 当前版本
     * */
    private String version;

    private String userName;

    private String stationName;

    /**
     * 运营商
     * */
    private String operator;
    /**
     * 品牌商
     * length 0~50
     */
    private String brand_owner;
    /**
     * 代理商
     * length 0~50
     */
    private String agent;
    /**
     * 服务商
     * length 0~50
     */
    private String service_provider;




}
