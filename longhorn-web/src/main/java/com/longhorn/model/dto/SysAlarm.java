package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 历史告警表
 * @author gmding
 * @data 2023.07.25
 * */
@Data
public class SysAlarm implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 菜单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 告警开始时间
     * */
    private String alarm_start_time;
    /**
     * 告警结束时间
     * */
    private String alarm_end_time;
    /**
     * 告警信息
     * */
    private String alarm_msg;
    /**
     * 告警码
     * */
    private String alarm_code;
    /**
     * 桩编号
     * */
    private String pile_code;

    /**
     * 抢号
     **/
    private Integer connectorId;
    /**
     * 创建时间
     */
    private Timestamp create_time;


}
