package com.longhorn.model.dto;

import com.ctc.wstx.ent.IntEntity;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 充电订单实体类
 * @author gmding
 * @data 2023.08.03
 * */
@Data
public class SysChargeOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 菜单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 订单状态 默认0=启动中  1=充电中 2=停止中 3=结算中 4=已完结
     * */
    private Integer status;
    /**
     * 订单来源  Android，iOS，web ，app
     * */
    private String source;
    /**
     * 创建时间
     * */
    private Timestamp create_time;
    /**
     * 开始充电时间  为避免充电桩获取时间失败 使用字符串
     * */
    private String start_time;
    /**
     * 结束充电时间
     * */
    private String end_time;

    /**
     * 停止原因  1=app主动停止 2=充满自动停止 3=急停
     * 4.DeAuthorized(未授权) 5 刷卡停止 6.蓝牙停止  7. 其他  8. 拒绝启动
     **/
    private Integer stop_reason;
    /**
     * 启动方式  1=扫码启动 2=插枪自动启动 3=定时启动
     * 4.刷卡启动 5.vin自动充电 6.saas(web)远程启动 7.蓝牙启动
     */
    private Integer start_method;

    /**
     * 充电电量
     */
    private Double capacity;
    /**
     * 开始soc
     */
    private Integer start_soc;
    /**
     * 结束soc
     */
    private Integer end_soc;
    /**
     * 充电时常
     */
    private Double charge_time;
    /**
     * 车牌号
     */
    private String car_number;

    /**
     * VIN码
     */
    private String vin;

    /**
     * 充电卡编码
     */
    private String card_code;
    /**
     * 用户ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 充电桩编号
     */
    private String pile_code;

    /**
     * 充电桩抢号  连接器id
     */
    private Integer connectorId;
    /**
     * 订单号
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long order_id;

}
