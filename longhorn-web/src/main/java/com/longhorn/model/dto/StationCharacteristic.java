package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 站点管理——站点特色
 * */
@Data
public class StationCharacteristic implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 语言类型  数据字典id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long sys_data_type_id;
    /**
     * 站点管理id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;

}
