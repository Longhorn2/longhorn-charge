package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysAuth implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 数据id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> userIds;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
    /**
     * 类型   1客户管理；2站点管理
     */
    private Integer type;
}
