package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import jdk.internal.dynalink.linker.LinkerServices;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 系统菜单表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class SysMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 菜单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 菜单名称
     * */
    private String menu_name;
    /**
     * 是否可见 0=不可见 1=可见
     * */
    private Integer visible;
    /**
     * 父级id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parent_id;
    /**
     * 状态 0=可用 1=不可用
     * */
    private Integer status;
    /**
     * 序号  排序
     * */
    private Integer sort;

    /**
     * 图标
     **/
    private String icon;
    /**
     * 前端路由名称
     */
    private String router_name;

    /**
     * 前端路由路径
     */
    private String router_path;

    /**
     * 前端路由视图
     */
    private String router_views;

    /**
     * 菜单类型  0菜单 1按钮
     * */
    private Integer type;


    private List<SysMenu> children;


}
