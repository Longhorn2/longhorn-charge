package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 角色表
 * @author gmding
 * @data 2023.04.26
 * */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id 角色id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 角色名称
     * */
    private String role_name;
    /**
     * 描述
     * */
    private String description;

    /**
     * 备注
     * */
    private String remark;

    /**
     * 角色状态 0=禁用 1=启用
     * */
    private Integer status;

    private Timestamp create_time;

    private Timestamp update_time;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;

    private String user_name;
}
