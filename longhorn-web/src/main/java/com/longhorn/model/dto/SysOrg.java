package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 系统组织表
 * @author gmding
 * @data 2023.04.28
 * */
@Data
public class SysOrg implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 组织id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 组织名称
     */
    private String org_name;
    /**
     * 组织代码
     */
    private String org_code;
    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parent_id;

    private Timestamp create_time;

    private Timestamp update_time;

    /**
     * 创建用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;

    private List<SysOrg> sysOrg;
}
