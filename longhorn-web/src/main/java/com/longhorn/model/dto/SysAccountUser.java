package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * 系统账号管理表——子账号管理
 * @author gmding
 * @data 2023.07.24
 * */
@Data
@Component
public class SysAccountUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
}
