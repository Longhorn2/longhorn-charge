package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 系统角色列表权限
 * @author gmding
 * @data 2023.07.24
 * */
@Data
@Component
public class SysOrgAuth implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 组织id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long org_id;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long user_id;
}
