package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class SysOrderLog implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 订单id
     */
    private Long order_id;
    /**
     * 事务名称 1app启动充电 2 app结束充电
     */
    private Integer transaction_name;
    /**
     * 操作人  用户id    1 桩操作  2平台操作
     */
    private String user_id;
    /**
     * 创建时间
     */
    private Timestamp create_time;
}
