package com.longhorn.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 站点管理——站点照片
 * */
@Data
@Component
public class StationImg implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 站点管理id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long station_management_id;
    /**
     * 站点管理图片
     * */
    private String img;

    private String img1;

    public  static String minioUrl;
    @Value("${minioUrl}")
    public void setMinioUrl(String minioUrl) {
        SysUser.minioUrl = minioUrl;
    }

    public String getImg1(){
        if (SysUser.minioUrl!=null){
            this.img1=SysUser.minioUrl +"downloadFileByName?bucketName=stationimg&fileName="+this.img;
        }
        return this.img1;
    }

}
