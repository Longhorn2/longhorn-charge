package com.longhorn.controller.pileManagement;


import com.google.gson.Gson;
import com.longhorn.client.MinIoClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.PileManagementImpl;
import com.longhorn.dto.impl.StationManagementImpl;
import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.vo.PileManagementVo;
import com.longhorn.model.vo.StationInfoVo;
import com.longhorn.model.vo.StationManagementVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 充电桩管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "充电桩管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/pileManagement")
public class PileManagementController {
    @Resource
    private PileManagementImpl pileManagementImpl;

    @ApiOperation(value = "新增电桩")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO add(@RequestBody PileManagement vo, HttpServletRequest request) throws BusinessException {
        log.info("新增电桩:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return pileManagementImpl.addPileManagement(vo,request);
    }



    @ApiOperation(value = "编辑充电桩")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO edit(@RequestBody PileManagement vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑充电桩:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return pileManagementImpl.editPileManagement(vo,request);
    }

    @ApiOperation(value = "查询充电桩列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getList(@RequestBody PileManagementVo vo, HttpServletRequest request) throws BusinessException {
        log.info("查询充电桩列表:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return pileManagementImpl.getList(vo,request);
    }




}

