package com.longhorn.controller.alarm;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.CustomerManagementImpl;
import com.longhorn.dto.impl.SysAlarmImpl;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.CustomerVo;
import com.longhorn.model.vo.SysAlarmConfigVo;
import com.longhorn.model.vo.SysAlarmVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 客户管理
 * </p>
 *
 * @author gmding
 * @since 2023-07-25
 */
@Api(tags = "故障告警")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sys/alarm")
public class SysAlarmController {
    @Resource
    private SysAlarmImpl sysAlarmImpl;


    @ApiOperation(value = "查询历史告警列表")
    @RequestMapping(value = "/getHisAlarmList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getHisAlarmList(@RequestBody SysAlarmVo vo, HttpServletRequest request) throws BusinessException {
        log.info("查询历史告警列表:{}",new Gson().toJson(vo));
        return sysAlarmImpl.getHisAlarmList(vo,request);
    }


    @ApiOperation(value = "新增告警配置")
    @RequestMapping(value = "/addSysAlarmConfig", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addSysAlarmConfig(@RequestBody SysAlarmConfig vo, HttpServletRequest request) throws BusinessException {
        log.info("新增告警配置:{}",new Gson().toJson(vo));
        return sysAlarmImpl.addSysAlarmConfig(vo,request);
    }


    @ApiOperation(value = "获取告警配置列表")
    @RequestMapping(value = "/getSysAlarmConfigList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getSysAlarmConfigList(@RequestBody SysAlarmConfigVo vo, HttpServletRequest request) throws BusinessException {
        log.info("获取告警配置列表:{}",new Gson().toJson(vo));
        return sysAlarmImpl.getSysAlarmConfigList(vo,request);
    }

    @ApiOperation(value = "编辑告警配置")
    @RequestMapping(value = "/updSysAlarmConfig", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updSysAlarmConfig(@RequestBody SysAlarmConfig vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑告警配置:{}",new Gson().toJson(vo));
        return sysAlarmImpl.updSysAlarmConfig(vo,request);
    }
    @ApiOperation(value = "删除告警配置")
    @RequestMapping(value = "/delSysAlarmConfig/{id}", method = RequestMethod.GET)
    public ResponseVO delSysAlarmConfig(@PathVariable("id")Long id, HttpServletRequest request) throws BusinessException {
        log.info("删除告警配置:{}",id);
        return sysAlarmImpl.delSysAlarmConfig(id,request);
    }
}

