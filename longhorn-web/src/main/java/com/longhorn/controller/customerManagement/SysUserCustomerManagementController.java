package com.longhorn.controller.customerManagement;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.CustomerManagementImpl;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.CustomerVo;
import com.longhorn.model.vo.SysUserVo;
import com.longhorn.model.vo.UserOrgVo;
import com.longhorn.model.vo.UserRoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 客户管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-22
 */
@Api(tags = "客户管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/customer/management")
public class SysUserCustomerManagementController {
    @Resource
    private CustomerManagementImpl customerManagementImpl;


    @ApiOperation(value = "新增客户管理用户")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addUser(@RequestBody SysUser sysUser, HttpServletRequest request) throws BusinessException {
        log.info("新增客户管理用户:{}",new Gson().toJson(sysUser));
        return customerManagementImpl.addUser(sysUser,request);
    }

    @ApiOperation(value = "客户管理列表")
    @RequestMapping(value = "/getCustomerList",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getCustomerList(@RequestBody CustomerVo vo, HttpServletRequest request) throws BusinessException{
        log.info("客户管理列表：{}",new Gson().toJson(vo));
        return customerManagementImpl.getCustomerList(vo,request);
    }

    @ApiOperation(value = "编辑")
    @RequestMapping(value = "/editUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO editUser(@RequestBody SysUser vo,HttpServletRequest request) throws BusinessException {
        log.info("编辑:{}",new Gson().toJson(vo));
        return customerManagementImpl.editUser(vo,request);
    }

    /**
     * status 账户状态 0=禁用  1=正常  2=注销 3=冻结
     * */
    @ApiOperation(value = "设置客户账号状态")
    @RequestMapping(value = "/setUserStatus/{id}/{status}", method = RequestMethod.GET)
    public ResponseVO setUserStatus(@PathVariable("id")Long id,@PathVariable("status")Integer status, HttpServletRequest request) throws BusinessException {
        log.info("设置客户账号状态:{},{}",id,status);
        return customerManagementImpl.setUserStatus(id,status,request);
    }


    @ApiOperation(value = "查询客户管理所有——新增站点对应的条件选择")
    @RequestMapping(value = "/getCustomerAllList/{userId}/{customer_type}",method = RequestMethod.GET)
    public ResponseVO getCustomerAllList(@PathVariable("userId")Long userId,@PathVariable("customer_type")String customer_type, HttpServletRequest request) throws BusinessException{
        log.info("{}查询客户管理所有——新增站点对应的条件选择:{}",userId,customer_type);
        return customerManagementImpl.getCustomerAllList(userId,customer_type,request);
    }

}

