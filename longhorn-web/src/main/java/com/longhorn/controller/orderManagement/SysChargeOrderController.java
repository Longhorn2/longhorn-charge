package com.longhorn.controller.orderManagement;


import com.google.gson.Gson;
import com.longhorn.client.ShardingClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysChargeOrderImpl;
import com.longhorn.model.vo.SysChargeOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单管理
 * </p>
 *
 * @author gmding
 * @since 2023-08-03
 */
@Api(tags = "订单管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysChargeOrder")
public class SysChargeOrderController {
    @Resource
    private SysChargeOrderImpl sysChargeOrderImpl;
    @Resource
    private ShardingClient shardingClient;

    @ApiOperation(value = "充电订单列表")
    @RequestMapping(value = "/getChargeOrderList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getChargeOrderList(@RequestBody SysChargeOrderVo vo, HttpServletRequest request) throws BusinessException {
        log.info("充电订单列表:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return sysChargeOrderImpl.getChargeOrderList(vo,request);
    }


    @ApiOperation(value = "充电过程数据")
    @RequestMapping(value = "/getOrderList/{orderId}", method = RequestMethod.GET)
    public ResponseVO getOrderList(@PathVariable("orderId")Long orderId, HttpServletRequest request) throws BusinessException {
        log.info("充电订单列表:{},语言:{}",orderId,request.getSession().getAttribute("language"));
        return shardingClient.saasChargeData(orderId,request);
    }

    @ApiOperation(value = "订单日志")
    @RequestMapping(value = "/getSysOrderLog/{orderId}", method = RequestMethod.GET)
    public ResponseVO getSysOrderLog(@PathVariable("orderId")Long orderId, HttpServletRequest request) throws BusinessException {
        log.info("订单日志:{},语言:{}",orderId,request.getSession().getAttribute("language"));
        return sysChargeOrderImpl.getSysOrderLog(orderId,request);
    }

    @ApiOperation(value = "无效订单列表")
    @RequestMapping(value = "/getInvalidChargeOrderList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getInvalidChargeOrderList(@RequestBody SysChargeOrderVo vo, HttpServletRequest request) throws BusinessException {
        log.info("无效订单列表:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return sysChargeOrderImpl.getInvalidChargeOrderList(vo,request);
    }


}

