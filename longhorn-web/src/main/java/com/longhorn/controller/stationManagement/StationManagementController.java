package com.longhorn.controller.stationManagement;


import com.google.gson.Gson;
import com.longhorn.client.MinIoClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.StationManagementImpl;
import com.longhorn.model.vo.StationInfoVo;
import com.longhorn.model.vo.StationManagementVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 站点管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "站点管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/stationManagement")
public class StationManagementController {
    @Resource
    private StationManagementImpl stationManagementImpl;
    @Resource
    private MinIoClient minIoClient;

    @ApiOperation(value = "新增站点")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO add(@RequestBody StationManagementVo vo, HttpServletRequest request) throws BusinessException {
        log.info("新增站点:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return stationManagementImpl.addStationManagement(vo,request);
    }

    @ApiOperation(value = "站点上传照片")
    //自定义文件上传，自定义文件桶
    @PostMapping(value = "/uploadFileByName",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseVO uploadFileByName(@RequestPart("file") MultipartFile file,@RequestParam("fileName")String name)throws BusinessException{
        return minIoClient.uploadFileByName(file,name);
    }


    @ApiOperation(value = "编辑站点")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO edit(@RequestBody StationManagementVo vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑站点:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return stationManagementImpl.editStationManagement(vo,request);
    }

    @ApiOperation(value = "查询站点列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getList(@RequestBody StationInfoVo vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑站点:{},语言:{}",new Gson().toJson(vo),request.getSession().getAttribute("language"));
        return stationManagementImpl.getList(vo,request);
    }

    @ApiOperation(value = "获取所有站点名称")
    @RequestMapping(value = "/getAllList/{userId}", method = RequestMethod.GET)
    public ResponseVO getAllList(@PathVariable("userId") Long userId, HttpServletRequest request) throws BusinessException {
        log.info("获取所有站点名称:{},语言:{}",userId,request.getSession().getAttribute("language"));
        return stationManagementImpl.getAllList(userId,request);
    }

    @ApiOperation(value = "根据站点id查询详情")
    @RequestMapping(value = "/getStationDetails/{id}", method = RequestMethod.GET)
    public ResponseVO getStationDetails(@PathVariable("id") Long id, HttpServletRequest request) throws BusinessException {
        log.info("根据站点id查询详情:{},语言:{}",id,request.getSession().getAttribute("language"));
        return stationManagementImpl.getStationDetails(id,request);
    }




}

