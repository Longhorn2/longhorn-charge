package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysOrgImpl;
import com.longhorn.dto.impl.SysRoleImpl;
import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.dto.SysOrg;
import com.longhorn.model.dto.SysRole;
import com.longhorn.model.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 组织管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-17
 */
@Api(tags = "组织管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysOrg")
public class SysOrgController {

    @Resource
    private SysOrgImpl sysOrgImpl;

    @ApiOperation(value = "获取组织列表")
    @RequestMapping(value = "/getList/{userId}", method = RequestMethod.GET)
    public ResponseVO getList(@PathVariable Long userId,HttpServletRequest request) throws BusinessException {
        log.info("获取组织列表:{}",userId);
         return sysOrgImpl.getList(userId,request);
    }

    @ApiOperation(value = "新增组织")
    @RequestMapping(value = "/addOrg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addOrg(@RequestBody SysOrg vo, HttpServletRequest request) throws BusinessException {
        log.info("获取组织列表:{}");
        return sysOrgImpl.addOrg(vo,request);
    }

    @ApiOperation(value = "编辑组织信息")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO edit(@RequestBody SysOrg vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑组织信息:{}",new Gson().toJson(vo));
        return sysOrgImpl.edit(vo,request);
    }

    @ApiOperation(value = "删除组织")
    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public ResponseVO del(@PathVariable Long id, HttpServletRequest request) throws BusinessException {
        log.info("删除组织:{}",id);
        return sysOrgImpl.del(id,request);
    }

    @ApiOperation(value = "根据组织查询站点列表")
    @RequestMapping(value = "/getStationListByOrg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getStationListByOrg(@RequestBody StationListByOrgVo vo, HttpServletRequest request) throws BusinessException {
        log.info("根据组织查询站点列表:{}",vo);
        return sysOrgImpl.getStationListByOrg(vo,request);
    }

    @ApiOperation(value = "根据用户id获取已选中站点列表")
    @RequestMapping(value = "/getStationListByUserId/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getStationListByUserId(@PathVariable Long userId, HttpServletRequest request) throws BusinessException {
        log.info("根据用户id获取已选中站点列表:{}",userId);
        return sysOrgImpl.getStationListByUserId(userId,request);
    }


    @ApiOperation(value = "根据id获取组织列表")
    @RequestMapping(value ={ "/getOrgListById/{userId}/{id}","/getOrgListById/{userId}"}, method = RequestMethod.GET)
    public ResponseVO getOrgListById(@PathVariable Long userId,@PathVariable(required = false) Long id, HttpServletRequest request) throws BusinessException {
        log.info("userId根据id获取组织列表:{}",userId,id);
        return sysOrgImpl.getOrgListById(userId,id,request);
    }
}

