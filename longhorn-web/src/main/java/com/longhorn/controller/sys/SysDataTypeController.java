package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysDataTypeImpl;
import com.longhorn.dto.impl.SysOrgImpl;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.vo.SysDataTypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 字典管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-17
 */
@Api(tags = "字典管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysDataType")
public class SysDataTypeController {

    @Resource
    private SysDataTypeImpl sysDataTypeImpl;

    @ApiOperation(value = "数据字典列表")
    @RequestMapping(value = "/getList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getList(@RequestBody SysDataTypeVo vo, HttpServletRequest request) throws BusinessException {
        log.info("数据字典列表:{}",new Gson().toJson(vo));
         return sysDataTypeImpl.getList(vo,request);
    }

    @ApiOperation(value = "新增数据字典")
    @RequestMapping(value = "/addSysDataType", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addSysDataType(@RequestBody SysDataType vo, HttpServletRequest request) throws BusinessException {
        log.info("新增数据字典:{}",new Gson().toJson(vo));
        return sysDataTypeImpl.addSysDataType(vo,request);
    }

    @ApiOperation(value = "编辑数据字典")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO edit(@RequestBody SysDataType vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑角色信息:{}",new Gson().toJson(vo));
        return sysDataTypeImpl.edit(vo,request);

    }
    @ApiOperation(value = "删除数据字典")
    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public ResponseVO del(@PathVariable Long id, HttpServletRequest request) throws BusinessException {
        log.info("删除数据字典:{}",id);
        return sysDataTypeImpl.del(id,request);
    }


    @ApiOperation(value = "数据字典根据code获取列表")
    @RequestMapping(value = "/getListByCode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getListByCode(@RequestBody SysDataType vo, HttpServletRequest request) throws BusinessException {
        log.info("数据字典根据code获取列表:{}",new Gson().toJson(vo));
        return sysDataTypeImpl.getListByCode(vo,request);
    }
}

