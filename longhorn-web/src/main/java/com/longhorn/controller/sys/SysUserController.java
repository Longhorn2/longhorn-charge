package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysUserImpl;
import com.longhorn.model.dto.SysAuth;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.SysUserVo;
import com.longhorn.model.vo.UserOrgVo;
import com.longhorn.model.vo.UserRoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "账号管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    private SysUserImpl sysUserImpl;


    @ApiOperation(value = "新增saas用户")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addUser(@RequestBody SysUser sysUser, HttpServletRequest request) throws BusinessException {
        log.info("新增saas用户:{}",new Gson().toJson(sysUser));
         return sysUserImpl.addUser(sysUser,request);

    }
    @ApiOperation(value = "查询")
    @GetMapping("/get")
    public ResponseVO get(@RequestParam("username") String username, HttpServletRequest request) throws BusinessException {
        log.info("查询:{},语言:{}",new Gson().toJson(username),request.getSession().getAttribute("language"));
        return sysUserImpl.queryByUserName(username,request);
    }
    @ApiOperation(value = "登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO login(@RequestBody SysUser sysUser,HttpServletRequest request) throws BusinessException {
        log.info("登录:{}",new Gson().toJson(sysUser));
        return sysUserImpl.login(sysUser,request);
    }

    @ApiOperation(value = "编辑")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO editUser(@RequestBody SysUser vo,HttpServletRequest request) throws BusinessException {
        log.info("编辑:{}",new Gson().toJson(vo));
        return sysUserImpl.editUser(vo,request);
    }

    @ApiOperation(value = "用户设置角色")
    @RequestMapping(value = "/setUserRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO setUserRole(@RequestBody UserRoleVo vo, HttpServletRequest request) throws BusinessException {
        log.info("用户设置角色:{}",new Gson().toJson(vo));
        return sysUserImpl.setUserRole(vo,request);
    }

    @ApiOperation(value = "账户设置组织")
    @RequestMapping(value = "/setUserOrg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO setUserOrg(@RequestBody UserOrgVo vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑:{}",new Gson().toJson(vo));
        return sysUserImpl.UserOrgVo(vo,request);
    }

    @ApiOperation(value = "用户权限——菜单列表")
    @GetMapping("/selfMenu/{userId}")
    public ResponseVO findSelfMenu(@PathVariable("userId") Long userId, HttpServletRequest request) throws BusinessException {
        log.info("用户权限——菜单列表:{}",userId);
        return sysUserImpl.findSelfMenu(userId,request);
    }
//
//    @ApiOperation(value = "是否禁用")
//    @GetMapping("/status")
//    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(id));
//        log.info("用户管理:{}",new Gson().toJson(status));
//        return sysUserService.status(id,status);
//    }
//
//    @ApiOperation(value = "分页查询")
//    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(dto));
//        return sysUserService.pageByName(dto.getP(),dto.getSize(),dto.getIndex());
//    }
//
//    @ApiOperation(value = "用户拥有的组织列表")
//    @RequestMapping(value = "/selfOrg/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO findSelfOrg(@PathVariable("userId") Long userId) throws BusinessException {
//        log.info("用户管理:{}",new Gson().toJson(userId));
//        return sysUserService.findSelfOrgIds(userId);
//    }
//


//    @ApiOperation(value = "验证密码")
//    @RequestMapping(value = "/checkPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseVO checkPassword(@RequestBody SysUser sysUser) throws BusinessException{
//        log.info("验证密码:{}",new Gson().toJson(sysUser));
//        return sysUserImpl.checkPassword(sysUser);
//    }
    @ApiOperation(value = "修改密码")
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO changePassword(@RequestBody SysUser sysUser,String token,HttpServletRequest request) throws BusinessException{
        log.info("更改密码：{}",new Gson().toJson(sysUser));
        return sysUserImpl.changePassword(sysUser,token,request);
    }
    @ApiOperation(value = "修改个人信息")
    @RequestMapping(value = "/changeInformation/{userName}/{contact_phone}/{contact_phone_code}",method = RequestMethod.GET)
    public ResponseVO changeInformation(@PathVariable("userName")String userName,@PathVariable("contact_phone")String contact_phone,
                                        @RequestParam("contact_phone_code")String contact_phone_code,HttpServletRequest request) throws BusinessException{
//        return sysUserImpl.changeInformation(sysUser,token,fileUpload);
        log.info("修改个人信息：{},{},{}",userName,contact_phone,contact_phone_code);
        return sysUserImpl.changeInformation(userName,contact_phone,contact_phone_code,request);
    }


    @ApiOperation(value = "查询账号管理列表")
    @RequestMapping(value = "/getUserInforList",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getUserInforList(@RequestBody SysUserVo vo, HttpServletRequest request) throws BusinessException{
        log.info("查询账号管理列表：{}",new Gson().toJson(vo));
        return sysUserImpl.getUserInforList(vo,request);
    }

    @ApiOperation(value = "查询账号组织列表")
    @RequestMapping(value = "/getUserOrgList/{id}",method = RequestMethod.GET)
    public ResponseVO getUserOrgList(@PathVariable Long id, HttpServletRequest request) throws BusinessException{
        log.info("查询账号组织列表:{}",new Gson().toJson(id));
        return sysUserImpl.getUserOrgList(id,request);
    }



    @ApiOperation(value = "查询本人创建的所有用户")
    @RequestMapping(value = "/getUserAuthList",method = RequestMethod.GET)
    public ResponseVO getUserAuthList(HttpServletRequest request) throws BusinessException{
        log.info("查询本人创建的所有用户");
        return sysUserImpl.getUserAuthList(request);
    }

    @ApiOperation(value = "查询数据分配已选择的用户")
    @RequestMapping(value = "/getUserAuth/{id}/{type}",method = RequestMethod.GET)
    public ResponseVO getUserAuth(@PathVariable("id") Long id,@PathVariable("type") Integer type, HttpServletRequest request) throws BusinessException{
        log.info("查询数据分配已选择的用户:{},{}",id,type);
        return sysUserImpl.getUserAuth(id,type,request);
    }


    @ApiOperation(value = "保存数据权限")
    @RequestMapping(value = "/saveSysAuth",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO saveSysAuth(@RequestBody SysAuth vo, HttpServletRequest request) throws BusinessException{
        log.info("保存数据权限:{}",new Gson().toJson(vo));
        return sysUserImpl.saveSysAuth(vo,request);
    }
}

