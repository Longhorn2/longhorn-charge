package com.longhorn.controller.sys;


import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysRoleImpl;
import com.longhorn.model.dto.SysRole;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 角色管理
 * </p>
 *
 * @author gmding
 * @since 2023-05-09
 */
@Api(tags = "角色管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysRole")
public class SysRoleController {
    @Resource
    private SysRoleImpl sysRoleImpl;


    @ApiOperation(value = "分页获取角色列表")
    @RequestMapping(value = "/getList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getList(@RequestBody RoleListVo vo, HttpServletRequest request) throws BusinessException {
        log.info("分页获取角色列表:{}",new Gson().toJson(vo));
         return sysRoleImpl.getList(vo,request);

    }

    @ApiOperation(value = "获取所有角色列表")
    @RequestMapping(value = "/getAllList", method = RequestMethod.GET)
    public ResponseVO getAllList( HttpServletRequest request) throws BusinessException {
        log.info("获取所有角色列表:{}");
        return sysRoleImpl.getAllList(request);

    }

    @ApiOperation(value = "编辑角色信息")
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO edit(@RequestBody SysRole vo, HttpServletRequest request) throws BusinessException {
        log.info("编辑角色信息:{}",new Gson().toJson(vo));
        return sysRoleImpl.edit(vo,request);

    }
    @ApiOperation(value = "新增角色")
    @RequestMapping(value = "/addRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody SysRole vo, HttpServletRequest request) throws BusinessException {
        log.info("新增角色:{}",new Gson().toJson(vo));
        return sysRoleImpl.addRole(vo,request);

    }
    @ApiOperation(value = "角色菜单权限设置")
    @RequestMapping(value = "/setRoleMenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO setRoleMenu(@RequestBody RoleMenuVo vo, HttpServletRequest request) throws BusinessException {
        log.info("角色菜单权限设置:{}",new Gson().toJson(vo));
        return sysRoleImpl.setRoleMenu(vo,request);
    }
    @ApiOperation(value = "角色删除")
    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public ResponseVO del(@PathVariable Long id, HttpServletRequest request) throws BusinessException {
        log.info("角色删除:{}",id);
        return sysRoleImpl.del(id,request);
    }

    @ApiOperation(value = "通过角色id查询菜单列表")
    @RequestMapping(value = "/getMenuByRole/{id}", method = RequestMethod.GET)
    public ResponseVO getMenuByRoleId(@PathVariable Long id, HttpServletRequest request) throws BusinessException {
        log.info("通过角色id查询菜单列表:{}",id);
        return sysRoleImpl.getMenuByRoleId(id,request);
    }

}

