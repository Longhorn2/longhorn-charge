package com.longhorn.controller.sys;
import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.dto.impl.SysMenuImpl;
import com.longhorn.dto.service.SysMenuService;
import com.longhorn.model.dto.SysMenu;
import com.longhorn.model.vo.MenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统菜单表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
@Api(tags = "菜单管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysMenu")
public class SysMenuController {
    @Autowired
    private SysMenuImpl sysMenuImpl;

    @ApiOperation(value = "菜单管理列表")
    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    public ResponseVO getList( HttpServletRequest request) throws BusinessException {
        log.info("菜单管理列表:{}","");
        return sysMenuImpl.menusList(request);
    }

    @ApiOperation(value = "删除菜单")
    @GetMapping("/del/{id}")
    public ResponseVO delMenu(@PathVariable("id") Long id,HttpServletRequest request) throws BusinessException {
        log.info("删除菜单:{}",new Gson().toJson(id));
        return sysMenuImpl.del(id,request);
    }

    @ApiOperation(value = "修改菜单")
    @RequestMapping(value = "/updMenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updMenu(@RequestBody SysMenu vo,HttpServletRequest request) throws BusinessException {
        log.info("修改菜单:{}",new Gson().toJson(vo));
        return sysMenuImpl.updateById(vo,request);
    }
//
//    @ApiOperation(value = "是否禁用")
//    @GetMapping("/status")
//    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
//        log.info("菜单管理:{}",new Gson().toJson(id));
//        log.info("菜单管理:{}",new Gson().toJson(status));
//        return sysMenuService.status(id,status);
//    }
//
    @ApiOperation(value = "新增菜单")
    @RequestMapping(value = "/addMenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addMenu(@RequestBody SysMenu vo,HttpServletRequest request) throws BusinessException {
        log.info("新增菜单:{}",new Gson().toJson(vo));
        return sysMenuImpl.addMenu(vo,request);
    }
}
