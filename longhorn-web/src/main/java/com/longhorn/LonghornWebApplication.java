package com.longhorn;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
@MapperScan("com.longhorn.dto")
@EnableScheduling
@EnableFeignClients(basePackages = { "com.longhorn.client" })
public class LonghornWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(LonghornWebApplication.class, args);
    }

}
