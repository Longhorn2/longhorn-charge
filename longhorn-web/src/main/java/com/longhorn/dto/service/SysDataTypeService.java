package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.vo.SysDataTypeVo;

import javax.servlet.http.HttpServletRequest;

public interface SysDataTypeService {

    ResponseVO getList(SysDataTypeVo vo, HttpServletRequest request);
    ResponseVO getListByCode(SysDataType vo, HttpServletRequest request);

    ResponseVO addSysDataType(SysDataType vo, HttpServletRequest request);

    ResponseVO edit(SysDataType vo, HttpServletRequest request);
    ResponseVO del(Long id, HttpServletRequest request);
}
