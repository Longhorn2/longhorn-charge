package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysAuth;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.SysUserVo;
import com.longhorn.model.vo.UserOrgVo;
import com.longhorn.model.vo.UserRoleVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

public interface SysUserService {
    ResponseVO queryByUserName(String userName,HttpServletRequest request);
    ResponseVO addUser(SysUser sysUser,HttpServletRequest request);

    ResponseVO login(SysUser sysUser,HttpServletRequest request);

 //   ResponseVO checkPassword(SysUser sysUser);

    ResponseVO changePassword(SysUser sysUser,String token,HttpServletRequest request);


    ResponseVO logout(SysUser sysUser,HttpServletRequest request);

    //ResponseVO changeInformation(String userName, Integer sex, String country, String province, Long id, MultipartFile file,HttpServletRequest request);
    ResponseVO changeInformation(String userName, String contact_phone, String contact_phone_code, HttpServletRequest request) ;
    ResponseVO getUserInforList(SysUserVo vo, HttpServletRequest req);

    ResponseVO editUser(SysUser sysUser,HttpServletRequest request);

    ResponseVO setUserRole(UserRoleVo vo, HttpServletRequest request);

    ResponseVO UserOrgVo(UserOrgVo vo, HttpServletRequest request);

    ResponseVO findSelfMenu(Long userId, HttpServletRequest request);


    ResponseVO getUserOrgList(Long userId, HttpServletRequest request);
    ResponseVO getUserAuthList(HttpServletRequest request);

    ResponseVO getUserAuth(Long id,Integer type, HttpServletRequest request);
    ResponseVO saveSysAuth(SysAuth vo, HttpServletRequest request);

}
