package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.vo.PileManagementVo;

import javax.servlet.http.HttpServletRequest;

public interface PileManagementService {
    ResponseVO addPileManagement(PileManagement vo, HttpServletRequest request);

    ResponseVO editPileManagement(PileManagement vo, HttpServletRequest request);
    ResponseVO getList(PileManagementVo vo, HttpServletRequest request);
//    ResponseVO getAllList(Long userId, HttpServletRequest request);




}
