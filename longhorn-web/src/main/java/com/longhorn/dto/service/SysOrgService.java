package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysOrg;
import com.longhorn.model.dto.SysRole;
import com.longhorn.model.vo.OrgListVo;
import com.longhorn.model.vo.RoleListVo;
import com.longhorn.model.vo.RoleMenuVo;
import com.longhorn.model.vo.StationListByOrgVo;

import javax.servlet.http.HttpServletRequest;

public interface SysOrgService {

    ResponseVO getList(Long userId,HttpServletRequest request);
    ResponseVO addOrg(SysOrg vo,HttpServletRequest request);

    ResponseVO edit(SysOrg vo, HttpServletRequest request);

    ResponseVO getStationListByOrg(StationListByOrgVo vo, HttpServletRequest request);

    ResponseVO getStationListByUserId(Long userId, HttpServletRequest request);

    ResponseVO getOrgListById(Long userId,Long id, HttpServletRequest request);

}
