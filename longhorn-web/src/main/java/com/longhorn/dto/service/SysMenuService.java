package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysMenu;
import com.longhorn.model.vo.MenuVo;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * <p>
 * 系统菜单表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
public interface SysMenuService  {

    /**
     * @param id 当前行的id
     * @return ResponseVO
     */
    //ResponseVO status(Long id, Boolean status, HttpServletRequest request);

    /**
     * @param menuDTO
     * @return ResponseVO
     */
    ResponseVO menusList(HttpServletRequest request);

    /**
     * @param id id
     * @return ResponseVO
     */
    ResponseVO del(Long id,HttpServletRequest request);

    ResponseVO addMenu(SysMenu vo,HttpServletRequest request);

    ResponseVO updateById(SysMenu vo,HttpServletRequest request);


}

