package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.vo.SysAlarmConfigVo;
import com.longhorn.model.vo.SysAlarmVo;

import javax.servlet.http.HttpServletRequest;

public interface SysAlarmService {
    ResponseVO getHisAlarmList(SysAlarmVo vo, HttpServletRequest request);

    ResponseVO addSysAlarmConfig(SysAlarmConfig vo, HttpServletRequest request);

    ResponseVO getSysAlarmConfigList(SysAlarmConfigVo vo, HttpServletRequest request);

    ResponseVO updSysAlarmConfig(SysAlarmConfig vo, HttpServletRequest request);

    ResponseVO delSysAlarmConfig(Long id, HttpServletRequest request);

}
