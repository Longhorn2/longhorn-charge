package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.vo.SysChargeOrderVo;

import javax.servlet.http.HttpServletRequest;

public interface SysChargeOrderService {

    ResponseVO getChargeOrderList(SysChargeOrderVo vo, HttpServletRequest request);


    ResponseVO getSysOrderLog(Long orderId, HttpServletRequest request);

    ResponseVO getInvalidChargeOrderList(SysChargeOrderVo vo, HttpServletRequest request);

}
