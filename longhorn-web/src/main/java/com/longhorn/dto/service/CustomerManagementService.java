package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.CustomerVo;
import com.longhorn.model.vo.SysUserVo;

import javax.servlet.http.HttpServletRequest;

public interface CustomerManagementService {
    ResponseVO addUser(SysUser sysUser, HttpServletRequest request);
    ResponseVO getCustomerList(CustomerVo vo, HttpServletRequest request);
    ResponseVO editUser(SysUser sysUser, HttpServletRequest request);

    ResponseVO setUserStatus(Long userId,Integer status, HttpServletRequest request);

    ResponseVO getCustomerAllList(Long userId,String customer_type, HttpServletRequest request);



}
