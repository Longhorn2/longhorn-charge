package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.SysRole;
import com.longhorn.model.vo.RoleListVo;
import com.longhorn.model.vo.RoleMenuVo;

import javax.servlet.http.HttpServletRequest;

public interface SysRoleService {

    ResponseVO getList(RoleListVo name, HttpServletRequest request);
    ResponseVO getAllList(HttpServletRequest request);

    ResponseVO edit(SysRole name, HttpServletRequest request);

    ResponseVO setRoleMenu(RoleMenuVo vo, HttpServletRequest request);

    ResponseVO del(Long id, HttpServletRequest request);
    ResponseVO addRole(SysRole vo, HttpServletRequest request);

    ResponseVO getMenuByRoleId(Long id, HttpServletRequest request);



}
