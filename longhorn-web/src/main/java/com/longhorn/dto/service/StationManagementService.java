package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.vo.StationInfoVo;
import com.longhorn.model.vo.StationManagementVo;

import javax.servlet.http.HttpServletRequest;

public interface StationManagementService {
    ResponseVO addStationManagement(StationManagementVo vo, HttpServletRequest request);

    ResponseVO editStationManagement(StationManagementVo vo, HttpServletRequest request);
    ResponseVO getList(StationInfoVo vo, HttpServletRequest request);
    ResponseVO getAllList(Long userId, HttpServletRequest request);

    ResponseVO getStationDetails(Long id, HttpServletRequest request);




}
