package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.SysDataTypeMapper;
import com.longhorn.dto.mapper.SysOrgMapper;
import com.longhorn.dto.service.SysDataTypeService;
import com.longhorn.dto.service.SysOrgService;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.dto.SysOrg;
import com.longhorn.model.vo.SysDataTypeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class SysDataTypeImpl implements SysDataTypeService {

    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;

    @Override
    public ResponseVO getList(SysDataTypeVo vo, HttpServletRequest request) {


        List<SysDataType> list=sysDataTypeMapper.queryByCode(vo.getIndex(),vo.getP()*vo.getSize(),vo.getSize());
        vo.setList(list);
        int total=sysDataTypeMapper.queryByCodeTotal(vo.getIndex());
        vo.setTotal(total);

        //List<SysOrg> orgList=getOrgList(list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }
    @Override
    public ResponseVO getListByCode(SysDataType vo, HttpServletRequest request) {
        List<SysDataType> list=sysDataTypeMapper.queryAllByCode(vo.getCode(),vo.getLanguage());
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO addSysDataType(SysDataType vo, HttpServletRequest request) {
        vo.setId(IdWorker.getId());
        vo.setCreate_time(new Timestamp(System.currentTimeMillis()));
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysDataTypeMapper.addSysDataType(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO edit(SysDataType vo, HttpServletRequest request) {
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysDataTypeMapper.editById(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO del(Long id, HttpServletRequest request) {
        sysDataTypeMapper.delById(id);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }
}
