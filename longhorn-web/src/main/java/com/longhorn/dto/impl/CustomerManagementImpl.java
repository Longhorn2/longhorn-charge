package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.SysUserMapper;
import com.longhorn.dto.service.CustomerManagementService;
import com.longhorn.model.dto.SysAccountUser;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.utils.DesUtils;
import com.longhorn.model.vo.CustomerVo;
import com.longhorn.model.vo.SysUserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class CustomerManagementImpl implements CustomerManagementService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Override
    public ResponseVO addUser(SysUser sysUser,HttpServletRequest request) {
        if (!Utils.isValidEmail(sysUser.getEmail())){
            return new ResponseVO(ErrorCodeEnum.EMAIL_Valid,null);
        }
        if (!Utils.isValidPwd(sysUser.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid,null);
        }

        if (sysUserMapper.queryByEmail(sysUser.getEmail())!=null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_REG,null);
        }
        long pid=sysUser.getId();

        sysUser.setId(IdWorker.getId());
        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));
        sysUser.setRegister_time(new Timestamp(System.currentTimeMillis()));
        sysUser.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysUser.setType(10);
        sysUser.setStatus(1);
        sysUserMapper.addCustomer(sysUser);

        SysAccountUser sysAccountUser=new SysAccountUser();
        sysAccountUser.setId(IdWorker.getId());
        sysAccountUser.setPid(pid);
        sysAccountUser.setUser_id(sysUser.getId());

        sysUserMapper.addSysAccountUser(sysAccountUser);


        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getCustomerList(CustomerVo vo, HttpServletRequest request) {

        List<SysUser> list=sysUserMapper.queryCustomerList(vo.getUserId(),vo.getCountry(),vo.getProvince(),vo.getCustomer_type(),vo.getStatus(),vo.getSex(),vo.getEmail(),vo.getP()*vo.getSize(),vo.getSize());

        vo.setUserInfoList(list);
        int total=sysUserMapper.queryCustomerTotal(vo.getUserId(),vo.getCountry(),vo.getProvince(),vo.getCustomer_type(),vo.getStatus(),vo.getSex(),vo.getEmail());
        vo.setTotal(total);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }


    @Override
    public ResponseVO getCustomerAllList(Long userId,String customer_type, HttpServletRequest request) {

        List<SysUser> list=sysUserMapper.queryCustomerByCustomerType(userId,customer_type);

        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());

    }


    @Override
    public ResponseVO editUser(SysUser sysUser,HttpServletRequest request) {
//        if (!Utils.isValidEmail(sysUser.getEmail())){
//            return new ResponseVO(ErrorCodeEnum.EMAIL_Valid,null);
//        }
//        if (!Utils.isValidPwd(sysUser.getPwd())) {
//            return new ResponseVO(ErrorCodeEnum.PWD_Valid,null);
//        }

//        if (sysUserMapper.queryByEmail(sysUser.getEmail())!=null) {
//            return new ResponseVO(ErrorCodeEnum.EMAIL_REG,null);
//        }
//        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));

        sysUser.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysUserMapper.updCustomer(sysUser);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO setUserStatus(Long userId, Integer status, HttpServletRequest request) {
        sysUserMapper.updCustomerById(status,userId,new Timestamp(System.currentTimeMillis()));
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }


}
