package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.SysDataTypeMapper;
import com.longhorn.dto.mapper.SysMenuMapper;
import com.longhorn.dto.service.SysDataTypeService;
import com.longhorn.dto.service.SysMenuService;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.dto.SysMenu;
import com.longhorn.model.vo.MenuVo;
import com.longhorn.model.vo.SysDataTypeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class SysMenuImpl implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

//    @Override
//    public ResponseVO getList(SysDataTypeVo vo, HttpServletRequest request) {
//
//
//        List<SysDataType> list=sysDataTypeMapper.queryByCode(vo.getIndex(),vo.getP()*vo.getSize(),vo.getSize());
//        vo.setList(list);
//        int total=sysDataTypeMapper.queryByCodeTotal(vo.getIndex());
//        vo.setTotal(total);
//
//        //List<SysOrg> orgList=getOrgList(list);
//        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//    }
//
//    @Override
//    public ResponseVO addSysDataType(SysDataType vo, HttpServletRequest request) {
//
//        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
//        sysDataTypeMapper.addSysDataType(vo);
//        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//
//    }
//
//    @Override
//    public ResponseVO edit(SysDataType vo, HttpServletRequest request) {
//        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
//        sysDataTypeMapper.editById(vo);
//        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//
//    }

    @Override
    public ResponseVO menusList( HttpServletRequest request) throws BusinessException {
        List<SysMenu> list=sysMenuMapper.sortEnd();

        List<SysMenu> list1=getSysMenuAll(list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list1,request.getSession().getAttribute("language").toString());
    }

    /**
     * 递归获取所有菜单
     * */

    public List<SysMenu> getSysMenuAll(List<SysMenu> list)throws BusinessException{
        for (SysMenu sysMenu:list) {
            List<SysMenu> lists=sysMenuMapper.menuByPidList(sysMenu.getId());
            sysMenu.setChildren(lists);
            if (lists!=null){
                getSysMenuAll(lists);
            }
        }
        return list;
    }

    @Override
    public ResponseVO del(Long id, HttpServletRequest request) {
        sysMenuMapper.delByInfo(id);
        delSysMenuAll(id);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }
    /**
     * 递归删除菜单
     * */

    public void delSysMenuAll(Long id)throws BusinessException{
        List<SysMenu> list=sysMenuMapper.menuByPidList(id);
        if (list!=null){
            for (SysMenu sysMenu:list) {
                sysMenuMapper.delByInfo(sysMenu.getId());
                delSysMenuAll(sysMenu.getId());
            }
        }

    }

    @Override
    public ResponseVO addMenu(SysMenu vo, HttpServletRequest request) {

        vo.setId(IdWorker.getId());
        sysMenuMapper.addMenu(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO updateById(SysMenu vo, HttpServletRequest request) {
        sysMenuMapper.updateById(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }


}
