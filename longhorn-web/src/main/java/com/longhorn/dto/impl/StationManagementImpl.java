package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.client.MinIoClient;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.*;
import com.longhorn.dto.service.StationManagementService;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.StationInfoAllListVo;
import com.longhorn.model.vo.StationInfoVo;
import com.longhorn.model.vo.StationManagementListVo;
import com.longhorn.model.vo.StationManagementVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class StationManagementImpl implements StationManagementService {
    @Autowired
    private StationManagementMapper stationManagementMapper;
    @Autowired
    private StationCharacteristicMapper stationCharacteristicMapper;
    @Autowired
    private StationChargeTypeMapper stationChargeTypeMapper;
    @Autowired
    private StationImgMapper stationImgMapper;

    @Autowired
    private MinIoClient minIoClient;
    @Autowired
    private SysUserStationManagementMapper sysUserStationManagementMapper;

    @Resource
    private SysUserImpl sysUserImpl;


    @Override
    public ResponseVO addStationManagement(StationManagementVo voJson, HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");

        voJson.getStationManagement().setId(IdWorker.getId());
        voJson.getStationManagement().setCreate_time(new Timestamp(System.currentTimeMillis()));
        voJson.getStationManagement().setUpdate_time(new Timestamp(System.currentTimeMillis()));
        voJson.getStationManagement().setUser_id(sysUser.getId());

        stationManagementMapper.addStationManagement(voJson.getStationManagement());

        for (Long id:voJson.getStationCharacteristicId()) {//添加站点管理——站点特色
            StationCharacteristic stationCharacteristic=new StationCharacteristic();
            stationCharacteristic.setId(IdWorker.getId());
            stationCharacteristic.setSys_data_type_id(id);
            stationCharacteristic.setStation_management_id(voJson.getStationManagement().getId());
            stationCharacteristicMapper.addStationCharacteristic(stationCharacteristic);
        }
        for (Long id:voJson.getStationChargeTypeId()) {//添加站点管理——充电口类型
            StationChargeType stationChargeType=new StationChargeType();
            stationChargeType.setStation_management_id(voJson.getStationManagement().getId());
            stationChargeType.setId(IdWorker.getId());
            stationChargeType.setSys_data_type_id(id);
            stationChargeTypeMapper.addStationChargeType(stationChargeType);
        }
        for (String img:voJson.getStationImg()) {//添加  站点管理——站点照片
            StationImg stationImg=new StationImg();
            stationImg.setImg(img);
            stationImg.setId(IdWorker.getId());
            stationImg.setStation_management_id(voJson.getStationManagement().getId());
            stationImgMapper.addStationImg(stationImg);
        }
//        SysUserStationManagement sysUserStationManagement=new SysUserStationManagement();
//        sysUserStationManagement.setUser_id(voJson.getStationManagement().getUser_id());
//        sysUserStationManagement.setStation_management_id(voJson.getStationManagement().getId());
//        sysUserStationManagementMapper.addSysUserStationManagement(sysUserStationManagement);


        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }


    @Override
    public ResponseVO editStationManagement(StationManagementVo voJson, HttpServletRequest request) {


        voJson.getStationManagement().setUpdate_time(new Timestamp(System.currentTimeMillis()));
        stationManagementMapper.updateStationManagement(voJson.getStationManagement());

        stationCharacteristicMapper.delStationCharacteristicByStationManagementId(""+voJson.getStationManagement().getId());
        for (Long id:voJson.getStationCharacteristicId()) {//添加站点管理——站点特色
            StationCharacteristic stationCharacteristic=new StationCharacteristic();
            stationCharacteristic.setId(IdWorker.getId());
            stationCharacteristic.setSys_data_type_id(id);
            stationCharacteristic.setStation_management_id(voJson.getStationManagement().getId());
            stationCharacteristicMapper.addStationCharacteristic(stationCharacteristic);
        }

        stationChargeTypeMapper.delStationChargeType(""+voJson.getStationManagement().getId());
        for (Long id:voJson.getStationChargeTypeId()) {//添加站点管理——充电口类型
            StationChargeType stationChargeType=new StationChargeType();
            stationChargeType.setStation_management_id(voJson.getStationManagement().getId());
            stationChargeType.setId(IdWorker.getId());
            stationChargeType.setSys_data_type_id(id);
            stationChargeTypeMapper.addStationChargeType(stationChargeType);
        }

        List<StationImg> list=stationImgMapper.queryStationImgById(voJson.getStationManagement().getId());
        for (StationImg imgs:list) {
            try {
                minIoClient.delFileByFileName(imgs.getImg(),"stationimg");
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        stationImgMapper.delStationImg(voJson.getStationManagement().getId());


        for (String img:voJson.getStationImg()) {//添加  站点管理——站点照片

            StationImg stationImg=new StationImg();
            stationImg.setImg(img);
            stationImg.setId(IdWorker.getId());
            stationImg.setStation_management_id(voJson.getStationManagement().getId());
            stationImgMapper.addStationImg(stationImg);
        }
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getList(StationInfoVo vo, HttpServletRequest request) {
        int p=vo.getP();
        vo.setP(vo.getP()*vo.getSize());
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        vo.setUser_id(sysUser.getId());

        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());
        vo.setUserIds(useridList);

        List<StationManagement> list=stationManagementMapper.queryByUserId(vo);

        Integer total=stationManagementMapper.queryByUserIdNum(vo);
        vo.setList(list);
        vo.setTotal(total);
        vo.setP(p);
        vo.setUserIds(null);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO getAllList(Long userId, HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());

        List<StationInfoAllListVo> vo=stationManagementMapper.queryAllByUserId(useridList,sysUser.getId());

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO getStationDetails(Long id, HttpServletRequest request) {


        StationManagementListVo vo=stationManagementMapper.queryStationById(id);

        vo.setStationCharacteristicList(stationCharacteristicMapper.queryStationCharacteristicById(id));
        vo.setStationChargeTypeList(stationChargeTypeMapper.queryStationChargeTypeById(id));
        vo.setStationImgList(stationImgMapper.queryStationImgById(id));

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }



}
