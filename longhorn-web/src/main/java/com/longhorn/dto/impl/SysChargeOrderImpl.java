package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.StationManagementMapper;
import com.longhorn.dto.mapper.SysChargeOrderMapper;
import com.longhorn.dto.service.SysChargeOrderService;
import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.dto.SysOrderLog;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.SysChargeOrderListVo;
import com.longhorn.model.vo.SysChargeOrderVo;
import com.longhorn.model.vo.SysOrderLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Slf4j
public class SysChargeOrderImpl implements SysChargeOrderService {

    @Autowired
    private SysChargeOrderMapper sysChargeOrderMapper;

    @Resource
    private SysUserImpl sysUserImpl;
    @Override
    public ResponseVO getChargeOrderList(SysChargeOrderVo vo, HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        int p=vo.getP();
        vo.setP(vo.getP()*vo.getSize());

        vo.setUser_id(sysUser.getId());
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());
        vo.setIds(useridList);

        List<SysChargeOrderListVo> list=sysChargeOrderMapper.getChargeOrderList(vo);

        vo.setTotal(sysChargeOrderMapper.getListNum(vo));
        vo.setList(list);
        vo.setP(p);
        vo.setIds(null);
        vo.setUser_id(null);

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getSysOrderLog(Long orderId, HttpServletRequest request) {
        List<SysOrderLogVo> list=sysChargeOrderMapper.querySysOrderLogByOrderId(orderId);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getInvalidChargeOrderList(SysChargeOrderVo vo, HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        int p=vo.getP();
        vo.setP(vo.getP()*vo.getSize());

        vo.setUser_id(sysUser.getId());
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());
        vo.setIds(useridList);

        List<SysChargeOrderListVo> list=sysChargeOrderMapper.getInvalidChargeOrderList(vo);

        vo.setTotal(sysChargeOrderMapper.getInvalidChargeOrderListNum(vo));
        vo.setList(list);
        vo.setP(p);
        vo.setIds(null);
        vo.setUser_id(null);

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }


}
