package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.client.MinIoClient;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.*;
import com.longhorn.dto.service.SysRoleService;
import com.longhorn.model.dto.*;
import com.longhorn.model.utils.DesUtils;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SysRoleImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Autowired
    private  SysMenuMapper sysMenuMapper;

    @Override
    public ResponseVO getList(RoleListVo vo, HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<SysRole>list=sysRoleMapper.queryAll(sysUser.getId(),vo.getIndex(), vo.getP()*vo.getSize(), vo.getSize());
        vo.setList(list);
        vo.setTotal(sysRoleMapper.queryAllTatol(sysUser.getId(),vo.getIndex()));

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }
    @Override
    public ResponseVO getAllList(HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<SysRole>list=sysRoleMapper.queryAllById(sysUser.getId());
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }


    @Override
    public ResponseVO edit(SysRole vo, HttpServletRequest request) {

        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysRoleMapper.editById(vo);

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO setRoleMenu(RoleMenuVo vo, HttpServletRequest request) {
        sysRoleMenuMapper.delByRoleId(vo.getRole_id());
        for (Long menuId:vo.getMenu_id()) {
            SysRoleMenu sysRoleMenu=new SysRoleMenu();
            sysRoleMenu.setMenu_id(menuId);
            sysRoleMenu.setRole_id(vo.getRole_id());
            sysRoleMenuMapper.addSysRoleMenu(sysRoleMenu);
        }
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO del(Long id, HttpServletRequest request) {
        sysRoleMenuMapper.delByRoleId(id);
        sysRoleMapper.delById(id);
        sysRoleMapper.delSysRoleAuth(id);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO addRole(SysRole vo, HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        vo.setCreate_time(new Timestamp(System.currentTimeMillis()));
        vo.setId(IdWorker.getId());
        vo.setStatus(1);
        vo.setUser_id(sysUser.getId());
        sysRoleMapper.addRole(vo);

//        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
//
//        SysRoleAuth sysRoleAuth=new SysRoleAuth();
//        sysRoleAuth.setId(IdWorker.getId());
//        sysRoleAuth.setRole_id(vo.getId());
//        sysRoleAuth.setUser_id(vo.getUser_id());
//        sysRoleMapper.addSysRoleAuth(sysRoleAuth);
//
//        sysRoleMapper.addSysUserRole(sysRoleAuth);


        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO getMenuByRoleId(Long id, HttpServletRequest request) {
        List<SysMenu> list=sysMenuMapper.getMenuByRoleId(id);
        getSysMenu(id,list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }



    public  List<SysMenu> getSysMenu(Long id,List<SysMenu> list){
        for (SysMenu sysMenu:list ) {
            List<SysMenu> vo =sysMenuMapper.getMenuByParentIdAndRoleId(id,sysMenu.getId());
            sysMenu.setChildren(vo);
            if (vo!=null){
                getSysMenu(id,vo);
            }
        }
        return list;
    }

}
