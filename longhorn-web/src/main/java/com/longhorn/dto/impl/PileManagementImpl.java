package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.client.ImClient;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.*;
import com.longhorn.dto.service.PileManagementService;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.PileManagementVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class PileManagementImpl implements PileManagementService {
    @Autowired
    private PileManagementMapper pileManagementMapper;

    @Autowired
    private ImClient imClient;

    @Resource
    private SysUserImpl sysUserImpl;

    @Override
    public ResponseVO addPileManagement(PileManagement vo, HttpServletRequest request) {

        if (pileManagementMapper.queryByPileCode(vo.getPile_code()).size()>0){
            return new ResponseVO(ErrorCodeEnum.PILE_EXIST_BOUND,null,request.getSession().getAttribute("language").toString());
        }
        vo.setId(IdWorker.getId());
        vo.setCreate_time(new Timestamp(System.currentTimeMillis()));
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        //stationManagementMapper.addStationManagement(voJson.getStationManagement());
        pileManagementMapper.addPileManagement(vo);
        imClient.saveUserId(vo.getStation_management_id(),vo.getPile_code());

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
    }


    @Override
    public ResponseVO editPileManagement(PileManagement vo, HttpServletRequest request) {

        PileManagement pileManagement=pileManagementMapper.queryByPileId(vo.getId());
        if (!pileManagement.getPile_code().equals(vo.getPile_code())){
            if (pileManagementMapper.queryByPileCode(vo.getPile_code()).size()>0){
                return new ResponseVO(ErrorCodeEnum.PILE_EXIST_BOUND,null,request.getSession().getAttribute("language").toString());
            }
        }

        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        pileManagementMapper.updatePileManagement(vo);
        imClient.saveUserId(vo.getStation_management_id(),vo.getPile_code());

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getList(PileManagementVo vo, HttpServletRequest request) {
        int p=vo.getP();
        vo.setP(vo.getP()*vo.getSize());

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());

        vo.setUserIds(useridList);
        vo.setUser_id(sysUser.getId());

        List<PileManagement> list=pileManagementMapper.getList(vo);

//        for (PileManagement pileManagement:list) {
//            pileManagement.setCharge_type(pileManagementMapper.queryById(pileManagement.getStation_management_id()));
//        }
        Integer total=pileManagementMapper.getListNum(vo);
        vo.setList(list);
        vo.setTotal(total);
        vo.setP(p);
        vo.setUserIds(null);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }
//
//    @Override
//    public ResponseVO getAllList(Long userId, HttpServletRequest request) {
//        List<StationInfoAllListVo> vo=stationManagementMapper.queryAllByUserId(userId);
//
//        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());
//
//    }

}
