package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.common.utils.Utils;
import com.longhorn.dto.mapper.*;
import com.longhorn.client.MinIoClient;
import com.longhorn.model.dto.*;
import com.longhorn.dto.service.SysUserService;
import com.longhorn.model.utils.DesUtils;
import com.longhorn.model.utils.RedisUtil;
import com.longhorn.model.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SysUserImpl implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired(required = false)
    private MinIoClient minIoClient;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysUserOrgMapper sysUserOrgMapper;
    @Autowired
    private SysUserStationManagementMapper sysUserStationManagementMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysOrgMapper sysOrgMapper;

    @Override
    public ResponseVO queryByUserName(String userName,HttpServletRequest request) {
        return new ResponseVO(StatusCodeEnum.SUCCESS,sysUserMapper.queryByUserName(userName));
    }

    @Override
    public ResponseVO addUser(SysUser sysUser,HttpServletRequest request) {
        if (!Utils.isValidEmail(sysUser.getEmail())){
            return new ResponseVO(ErrorCodeEnum.EMAIL_Valid,null);
        }
        if (!Utils.isValidPwd(sysUser.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid,null);
        }

        if (sysUserMapper.queryByEmail(sysUser.getEmail())!=null) {
            return new ResponseVO(ErrorCodeEnum.EMAIL_REG,null);
        }
        long pid=sysUser.getId();
        sysUser.setId(IdWorker.getId());
        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));
        sysUser.setRegister_time(new Timestamp(System.currentTimeMillis()));
        sysUser.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysUser.setType(3);
        sysUserMapper.addUser(sysUser);

        SysAccountUser sysAccountUser=new SysAccountUser();
        sysAccountUser.setId(IdWorker.getId());
        sysAccountUser.setPid(pid);
        sysAccountUser.setUser_id(sysUser.getId());

        sysUserMapper.addSysAccountUser(sysAccountUser);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     * @Description  用户登录
     *
     * */
    @Override
    public ResponseVO login(SysUser sysUser,HttpServletRequest request) {
        AuthInfoVO authInfoVo = new AuthInfoVO();
        authInfoVo.setTokenType("bearer");

        //登录验证
        SysUser sysUserInfo = sysUserMapper.queryByEmail(sysUser.getEmail());
        if (sysUserInfo == null) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_FOUND, null);
        } else {
            if (!sysUser.getPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))) {
                return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR, null);
            }
            long times = System.currentTimeMillis();

            String token = sysUserInfo.getEmail() + "|" +sysUserInfo.getPwd()+"|" + times;
            authInfoVo.setAccessToken(DesUtils.encrypt(token));
            authInfoVo.setRefreshToken(DesUtils.encrypt(sysUserInfo.getEmail()+"|" + times));
            authInfoVo.setExpiresIn(DesUtils.EXPIRESIN);
            authInfoVo.setRefreshExpires(DesUtils.REFRESHEXPIRES);
            authInfoVo.setUserInfo(sysUserInfo);

            Map<String, Object> map = new HashMap<>();
            map.put("accessToken", authInfoVo.getAccessToken());
            map.put("refreshToken", authInfoVo.getRefreshToken());
            map.put("expiresIn", authInfoVo.getExpiresIn() + "");
            map.put("userinfo", new Gson().toJson(sysUserInfo));
            map.put("times", times + "");

            redisUtil.hmset(sysUserInfo.getEmail(), map);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, authInfoVo,request.getSession().getAttribute("language").toString());
        }
    }

//    /**
//     * @Description 修改密码前校验
//     * @param sysUser
//     * @return
//     */
//    @Override
//    public ResponseVO checkPassword(SysUser sysUser) {
//        SysUser sysUserInfo = sysUserMapper.queryByAll(sysUser);
//        log.info("对应用户信息:{}",new Gson().toJson(sysUserInfo));
//        if (!sysUser.getPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))){
//            return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR,null);
//        }
//        redisUtil.set(sysUserInfo.getEmail()+"oldPassword",sysUserInfo.getPwd(),600);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,true);
//    }

    /**
     * @Description 修改密码
     * @param sysUser
     * @return
     */
    @Override
    public ResponseVO changePassword(SysUser sysUser,String token,HttpServletRequest request) {
        SysUser sysUserInfo = sysUserMapper.queryByEmail(sysUser.getEmail());
        if (!token.equals(redisUtil.hmget(sysUser.getEmail()).get("accessToken"))){
            return new ResponseVO(ErrorCodeEnum.OFFSITELOGIN,null);
        }
        if(sysUser.getOldPwd().equals((sysUser.getPwd()))){
            return new ResponseVO(ErrorCodeEnum.PASSWORD_CANNOT_IDENTICAL,null);
        }
        if (!Utils.isValidPwd(sysUser.getPwd())) {
            return new ResponseVO(ErrorCodeEnum.PWD_Valid,null);
        }
        if (!sysUser.getOldPwd().equals(DesUtils.decrypt(sysUserInfo.getPwd()))){
            return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR,request.getSession().getAttribute("language").toString());
        }
        sysUser.setPwd(DesUtils.encrypt(sysUser.getPwd()));
        Integer result = sysUserMapper.changePassword(sysUser);
        redisUtil.del(sysUser.getEmail());
        return new ResponseVO(ErrorCodeEnum.SUCCESS,result,request.getSession().getAttribute("language").toString());

    }

    /**
     * @Description 修改个人信息:昵称，头像，性别，生日，国家，地区
     * @param
     * @return
     */
    @Override
    public ResponseVO changeInformation(String userName, String contact_phone, String contact_phone_code,HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        sysUserMapper.changeInformation(userName,contact_phone,contact_phone_code,sysUser.getId());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }



    /**
     * @Description 注销:七天未登录注销账号，否则重新激活。注销申请后改变状态为注销，若在规定时间内登录则恢复状态为正常。开启定时扫描
     * 七天后无登录动作则改变状态为禁用
     * @param sysUser
     * @return
     */
    @Override
    public ResponseVO logout(SysUser sysUser,HttpServletRequest request) {

        return null;
    }

    /**
     * @Description  重新获取token
     *
     * */
    public ResponseVO getRefreshToken(AuthInfoVO info,HttpServletRequest request) throws BusinessException {
        log.info("请求数据:{}",new Gson().toJson(info.getRefreshToken()));
        ResponseVO responseVo=new ResponseVO();


        info.setTokenType("bearer");

        info.setExpiresIn(DesUtils.EXPIRESIN);
        info.setRefreshExpires(DesUtils.REFRESHEXPIRES);
        String str=DesUtils.decrypt(info.getRefreshToken());

        long times=System.currentTimeMillis();
        info.setAccessToken(DesUtils.encrypt(str+"|"+times));

        info.setRefreshToken(DesUtils.encrypt(str));

        Map<Object, Object> vo=  redisUtil.hmget( str );

        Map<String, Object> map =new HashMap<>(  );
        map.put( "accessToken",info.getAccessToken() );
        map.put( "refreshToken",info.getRefreshToken() );
        map.put( "expiresIn",info.getExpiresIn()+"" );
        map.put( "userinfo",vo.get( "userinfo" ));
        map.put( "times",times+"" );

        redisUtil.hmset( str, map);

        responseVo.setData(info);
        return responseVo;
    }

    @Override
    public ResponseVO getUserInforList(SysUserVo vo, HttpServletRequest request) {
        SysUser su =(SysUser) request.getSession().getAttribute("sysUserInfo");

        List<SysUser> list=sysUserMapper.queryAll(su.getId(),vo.getUser_name(),vo.getPhone(),vo.getP()*vo.getSize(),vo.getSize());
        for (SysUser sysUser:list ) {
            sysUser.setRoleList(sysRoleMapper.queryByUserId(sysUser.getId()));
            sysUser.setOrgList(sysRoleMapper.queryOrgByUserId(sysUser.getId()));
        }
        vo.setUserInfoList(list);
        int tatol=sysUserMapper.queryAllTatol(su.getId(),vo.getUser_name(),vo.getPhone());
        vo.setTotal(tatol);

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO editUser(SysUser sysUser, HttpServletRequest request) {


        sysUser.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysUserMapper.editUser(sysUser);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO setUserRole(UserRoleVo vo, HttpServletRequest request) {

        sysUserRoleMapper.delByUserId(vo.getUser_id());
        for (Long roleId:vo.getRole_id()) {
            SysUserRole sysUserRole=new SysUserRole();
            sysUserRole.setRole_id(roleId);
            sysUserRole.setUser_id(vo.getUser_id());
            sysUserRoleMapper.addSysUserRole(sysUserRole);
        }
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO UserOrgVo(UserOrgVo vo, HttpServletRequest request) {

        SysUser su =(SysUser) request.getSession().getAttribute("sysUserInfo");

        sysUserOrgMapper.delByUserId(vo.getUser_id());
        for (Long orgId:vo.getOrg_id()) {
            SysUserOrg sysUserOrg=new SysUserOrg();
            sysUserOrg.setOrg_id(orgId);
            sysUserOrg.setUser_id(vo.getUser_id());
            sysUserOrgMapper.addSysUserOrg(sysUserOrg);
        }
//        sysUserStationManagementMapper.delByUserId(vo.getUser_id());
//        for (Long station_management_id:vo.getStation_management_id()) {
//            SysUserStationManagement sysUserStationManagement=new SysUserStationManagement();
//            sysUserStationManagement.setStation_management_id(station_management_id);
//            sysUserStationManagement.setUser_id(vo.getUser_id());
//            sysUserStationManagementMapper.addSysUserStationManagement(sysUserStationManagement);
//        }


        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getUserOrgList(Long userId, HttpServletRequest request) {

        List<String> list=sysUserOrgMapper.queryByUserId(userId);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO findSelfMenu(Long userId, HttpServletRequest request) {

        List<SysMenu> list=sysMenuMapper.getMenuByUserId(userId);
        getSysMenu(userId,list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());

    }

    public  List<SysMenu> getSysMenu(Long userId,List<SysMenu> list){
        for (SysMenu sysMenu:list ) {
            List<SysMenu> vo =sysMenuMapper.getMenuByParentId(userId,sysMenu.getId());
            sysMenu.setChildren(vo);
            if (vo!=null){
                getSysMenu(userId,vo);
            }
        }
        return list;
    }

    /**
     * 根据用户id 获取所有下级以及本身用户id
     * */
    public List<Long> getUserIdByPid(Long userId){
        List<Long> list=new ArrayList<>();
        list.add(userId);
        List<SysOrg>sysOrg=sysOrgMapper.queryByUserId(userId);

        getSysOrgByPid(sysOrg,list);
        return  list;
    }

    public List<Long> getSysOrgByPid(List<SysOrg> list,List<Long> arr){
        for (SysOrg sysOrg:list) {
            List<SysUserOrg> sysUserOrgList=sysOrgMapper.queryByOrgId(sysOrg.getId());
            for (SysUserOrg sysUserOrg:sysUserOrgList) {
                arr.add(sysUserOrg.getUser_id());
                List<SysOrg>sysOrgs=sysOrgMapper.queryByUserId(sysUserOrg.getUser_id());
                getSysOrgByPid(sysOrgs,arr);
            }
        }
        return arr;
    }


    @Override
    public ResponseVO getUserAuthList(HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");

        List<SysUserAuthVo> list=sysUserOrgMapper.queryById(sysUser.getId());
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());

    }
    @Override
    public ResponseVO getUserAuth(Long id,Integer type,HttpServletRequest request) {

        List<SysAuth> list=sysUserOrgMapper.queryAuthById(type,id);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());

    }
    @Override
    public ResponseVO saveSysAuth(SysAuth vo,HttpServletRequest request) {

        sysUserOrgMapper.delSysAuth(vo.getType(),vo.getId());
        for (Long user_id:vo.getUserIds()) {
            vo.setUser_id(user_id);
            sysUserOrgMapper.saveSysAuth(vo);
        }

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }



}
