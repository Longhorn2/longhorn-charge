package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.SysAlarmMapper;
import com.longhorn.dto.service.SysAlarmService;
import com.longhorn.model.dto.SysAlarmConfig;
import com.longhorn.model.dto.SysUser;
import com.longhorn.model.vo.SysAlarmConfigVo;
import com.longhorn.model.vo.SysAlarmListVo;
import com.longhorn.model.vo.SysAlarmVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class SysAlarmImpl implements SysAlarmService {
    @Autowired
    private SysAlarmMapper sysAlarmMapper;

    @Resource
    private SysUserImpl sysUserImpl;
    @Override
    public ResponseVO getHisAlarmList(SysAlarmVo vo, HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");

        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());


        List<SysAlarmListVo> list= sysAlarmMapper.pageQueryList(sysUser.getId(),useridList,vo.getOperator(),vo.getName(),vo.getPile_code(),vo.getModel(),vo.getId(),vo.getP()*vo.getSize(),vo.getSize());
        int total= sysAlarmMapper.pageQueryListNum(sysUser.getId(),useridList,vo.getOperator(),vo.getName(),vo.getPile_code(),vo.getModel(),vo.getId());

        vo.setTotal(total);
        vo.setList(list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO addSysAlarmConfig(SysAlarmConfig vo, HttpServletRequest request) {

        if (sysAlarmMapper.querySysAlarmConfigByStationId(vo.getStation_management_id()).size()>0){
            return new ResponseVO(ErrorCodeEnum.STATION_EXIST,null,request.getSession().getAttribute("language").toString());
        }
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        vo.setUser_id(sysUser.getId());
        vo.setId(IdWorker.getId());
        vo.setCreate_time(new Timestamp(System.currentTimeMillis()));
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysAlarmMapper.addSysAlarmConfig(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO getSysAlarmConfigList(SysAlarmConfigVo vo, HttpServletRequest request) {
        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());

        List<SysAlarmConfig> list=sysAlarmMapper.pageQuerySysAlarmConfigList(sysUser.getId(),useridList,vo.getContact_phone(),vo.getEmail(),vo.getStation_management_id(),vo.getP()*vo.getSize(), vo.getSize() );

        vo.setList(list);
        Integer total=sysAlarmMapper.pageQuerySysAlarmConfigNum(sysUser.getId(),useridList,vo.getContact_phone(),vo.getEmail(),vo.getStation_management_id());
        vo.setTotal(total);

        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO updSysAlarmConfig(SysAlarmConfig vo, HttpServletRequest request) {

        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));

        sysAlarmMapper.updSysAlarmConfig(vo);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }

    @Override
    public ResponseVO delSysAlarmConfig(Long id, HttpServletRequest request) {

        sysAlarmMapper.delSysAlarmConfigById(id);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }


}
