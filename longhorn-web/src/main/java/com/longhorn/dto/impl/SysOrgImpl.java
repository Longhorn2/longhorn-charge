package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.StationManagementMapper;
import com.longhorn.dto.mapper.SysOrgMapper;
import com.longhorn.dto.mapper.SysRoleMapper;
import com.longhorn.dto.mapper.SysRoleMenuMapper;
import com.longhorn.dto.service.SysOrgService;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.OrgListVo;
import com.longhorn.model.vo.RoleListVo;
import com.longhorn.model.vo.StationInfoAllListVo;
import com.longhorn.model.vo.StationListByOrgVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class SysOrgImpl implements SysOrgService {
    @Autowired
    private SysOrgMapper sysOrgMapper;

    @Autowired
    private StationManagementMapper stationManagementMapper;

    @Resource
    private SysUserImpl sysUserImpl;
    @Override
    public ResponseVO getList(Long userId,HttpServletRequest request) {

        List<SysOrg> list=sysOrgMapper.queryAll(userId);
        //List<SysOrg> orgList=getOrgList(list);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO addOrg(SysOrg vo, HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");

        vo.setId(IdWorker.getId());
        vo.setCreate_time(new Timestamp(System.currentTimeMillis()));
        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
//        if (sysOrgMapper.queryByName(vo.getOrg_name()).size()>0){
//            return new ResponseVO(ErrorCodeEnum.ORG_EXIST,null,request.getSession().getAttribute("language").toString());
//        }
        vo.setUser_id(sysUser.getId());
        sysOrgMapper.addSysOrg(vo);
//        if (vo.getSysOrg()!=null){
//            addOrgList(vo.getSysOrg(),vo.getId());
//        }
//        SysOrgAuth sysOrgAuth=new SysOrgAuth();
//        sysOrgAuth.setId(IdWorker.getId());
//        sysOrgAuth.setOrg_id(vo.getId());
//        sysOrgAuth.setUser_id(userId);
//        sysOrgMapper.addSysOrgAuth(sysOrgAuth);

//        SysUserOrg sysUserOrg=new SysUserOrg();
//        sysUserOrg.setOrg_id(vo.getId());
//        sysUserOrg.setUser_id(sysUser.getId());
//        sysOrgMapper.addSysUserOrg(sysUserOrg);


        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());
    }

    /**
     * 递归获取组织结构
     * */
    public void addOrgList(List<SysOrg> list,Long parent_id){
        for (SysOrg sysOrg:list) {
            sysOrg.setId(IdWorker.getId());
            sysOrg.setCreate_time(new Timestamp(System.currentTimeMillis()));
            sysOrg.setUpdate_time(new Timestamp(System.currentTimeMillis()));
            sysOrg.setParent_id(parent_id);
            sysOrgMapper.addSysOrg(sysOrg);
            if (sysOrg.getSysOrg()!=null){
                addOrgList(sysOrg.getSysOrg(),sysOrg.getId());
            }
        }
    }

    /**
     * 递归添加组织结构
     * */
    public List<SysOrg> getOrgList(List<SysOrg> list){
        for (SysOrg sysOrg:list) {
            List<SysOrg> sysOrgList=sysOrgMapper.queryByPId(sysOrg.getId());
            sysOrg.setSysOrg(sysOrgList);
            getOrgList(sysOrgList);
        }
        return list;
    }


    @Override
    public ResponseVO edit(SysOrg vo, HttpServletRequest request) {

        vo.setUpdate_time(new Timestamp(System.currentTimeMillis()));
        sysOrgMapper.editById(vo);
        return new ResponseVO(StatusCodeEnum.SUCCESS,vo,request.getSession().getAttribute("language").toString());

    }


    public ResponseVO del(Long id, HttpServletRequest request) {
        sysOrgMapper.delById(id);
        //delOrgList(id);
        //sysOrgMapper.delSysOrgAuth(id);
        sysOrgMapper.delSysUserOrgById(id);

        return new ResponseVO(StatusCodeEnum.SUCCESS,null,request.getSession().getAttribute("language").toString());

    }

    /**
     * 递归删除组织结构
     * */
    public void delOrgList(Long parent_id){
        List<SysOrg> list=sysOrgMapper.queryByPId(parent_id);
        for (SysOrg sysOrg:list) {
            sysOrgMapper.delById(sysOrg.getId());
            sysOrgMapper.delSysOrgAuth(sysOrg.getId());
            delOrgList(sysOrg.getId());
        }
    }

    @Override
    public ResponseVO getStationListByOrg(StationListByOrgVo vo, HttpServletRequest request) {
        //List<StationInfoAllListVo> list= stationManagementMapper.queryByOrg(vo.getUser_id(),vo.getCountry(),vo.getProvince(),vo.getCity(),vo.getStationName());
        List<StationInfoAllListVo> list= stationManagementMapper.queryByOrg(vo.getUser_id(),vo.getArea(),vo.getStationName());
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO getStationListByUserId(Long userid, HttpServletRequest request) {

        SysUser sysUser =(SysUser) request.getSession().getAttribute("sysUserInfo");
        List<Long> useridList=sysUserImpl.getUserIdByPid(sysUser.getId());

        List<StationInfoAllListVo> list= stationManagementMapper.queryAllByUserId(useridList,sysUser.getId());
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }
    @Override
    public ResponseVO getOrgListById(Long userId,Long id, HttpServletRequest request) {

        List<SysOrg> list= sysOrgMapper.queryByParentId(userId,id);
        return new ResponseVO(StatusCodeEnum.SUCCESS,list,request.getSession().getAttribute("language").toString());
    }



}
