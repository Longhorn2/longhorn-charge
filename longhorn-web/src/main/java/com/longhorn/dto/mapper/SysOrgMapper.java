package com.longhorn.dto.mapper;

import com.longhorn.model.dto.*;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysOrgMapper {

    List<SysOrg> queryAll(@Param("userId") Long userId);
    List<SysOrg> queryByParentId(@Param("userId") Long userId,@Param("parent_id") Long parent_id);
    List<SysOrg> queryByPId(@Param("parent_id") Long parent_id);


    SysOrg queryById(@Param("id") Long id);
    void addSysOrg(SysOrg sysOrg);
//    int queryAllTatol(@Param("role_name") String index);
    void editById(SysOrg sysOrg);
//
    void delById(@Param("id") Long id);


    @Select("SELECT * FROM sys_org where org_name=#{org_name}")
    List<SysOrg> queryByName(@Param("org_name")String name);

    @Insert("INSERT INTO sys_org_auth(id,org_id,user_id) " +
            "VALUES (#{id},#{org_id},#{user_id})")
    void addSysOrgAuth(SysOrgAuth sysOrgAuth);
    @Update("DELETE from sys_org_auth  WHERE org_id = #{id}")
    void delSysOrgAuth(@Param("id") Long id);
    @Update("DELETE from sys_user_org  WHERE org_id = #{id}")
    void delSysUserOrgById(@Param("id") Long id);

    @Select("SELECT * FROM sys_account_user where user_id=#{id}")
    SysAccountUser queryPByUserId(@Param("id") Long id);

    @Insert("INSERT INTO sys_user_org(org_id,user_id) " +
            "VALUES (#{org_id},#{user_id})")
    void addSysUserOrg(SysUserOrg sysUserOrg);

    @Select("SELECT * FROM sys_org where user_id=#{user_id}")
    List<SysOrg> queryByUserId(@Param("user_id")Long userId);

    @Select("SELECT * FROM sys_user_org where org_id=#{org_id}")
    List<SysUserOrg> queryByOrgId(@Param("org_id")Long org_id);
}
