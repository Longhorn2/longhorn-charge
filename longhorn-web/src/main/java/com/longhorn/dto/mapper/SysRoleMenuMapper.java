package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysMenu;
import com.longhorn.model.dto.SysRole;
import com.longhorn.model.dto.SysRoleMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysRoleMenuMapper {

    void delByRoleId(@Param("role_id") Long role_id);
    void addSysRoleMenu(SysRoleMenu sysRoleMenu);

    List<SysMenu> getMenuByRoleId(@Param("id") Long id);
}
