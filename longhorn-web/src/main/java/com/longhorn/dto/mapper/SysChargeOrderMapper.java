package com.longhorn.dto.mapper;

import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.dto.SysChargeOrder;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.dto.SysOrderLog;
import com.longhorn.model.vo.PileManagementVo;
import com.longhorn.model.vo.SysChargeOrderListVo;
import com.longhorn.model.vo.SysChargeOrderVo;
import com.longhorn.model.vo.SysOrderLogVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysChargeOrderMapper {

    List<PileManagement> getList(PileManagementVo vo);


    List<SysChargeOrderListVo> getChargeOrderList(SysChargeOrderVo vo);
    Integer getListNum(SysChargeOrderVo vo);

    List<SysChargeOrderListVo> getInvalidChargeOrderList(SysChargeOrderVo vo);


    Integer getInvalidChargeOrderListNum(SysChargeOrderVo vo);

    List<SysOrderLogVo> querySysOrderLogByOrderId(@Param("order_id")Long order_id);

}
