package com.longhorn.dto.mapper;

import com.longhorn.model.dto.StationImg;
import com.longhorn.model.dto.SysDataType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StationImgMapper {
    void  addStationImg(StationImg StationImg);
    void delStationImg(@Param("station_management_id")Long station_management_id);
    List<StationImg> queryStationImgById(@Param("station_management_id")Long station_management_id);
}
