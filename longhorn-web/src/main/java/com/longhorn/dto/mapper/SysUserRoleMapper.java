package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysUser;
import com.longhorn.model.dto.SysUserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysUserRoleMapper {
    void delByUserId(@Param("user_id")Long user_id);
    int addSysUserRole(SysUserRole sysUserRole);

}
