package com.longhorn.dto.mapper;

import com.longhorn.model.dto.*;
import com.longhorn.model.vo.SysAlarmListVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysAlarmMapper {

    List<SysAlarmListVo> pageQueryList(@Param("user_id")Long user_id,@Param("ids") List<Long> userId,@Param("operator")String operator,@Param("name")String name,@Param("pile_code")String pile_code,@Param("model")String model,@Param("id")Long id,@Param("p")Integer p,@Param("size")Integer size);
    int pageQueryListNum(@Param("user_id")Long user_id,@Param("ids")  List<Long> userId,@Param("operator")String operator,@Param("name")String name,@Param("pile_code")String pile_code,@Param("model")String model,@Param("id")Long id);

    @Insert("INSERT INTO sys_alarm_config(id,station_management_id,contact_person,contact_phone,email,gunTemperature,gunPushMethod,gunChargeStatus,batteryTemperature,batteryPushMethod,batteryChargeStatus,offline,offlinePushMethod,alarmRate,alarmPushMethod,create_time,update_time,user_id,contact_phone_code) " +
            "VALUES (#{id},#{station_management_id},#{contact_person},#{contact_phone},#{email},#{gunTemperature},#{gunPushMethod},#{gunChargeStatus},#{batteryTemperature},#{batteryPushMethod},#{batteryChargeStatus},#{offline},#{offlinePushMethod},#{alarmRate},#{alarmPushMethod},#{create_time},#{update_time},#{user_id},#{contact_phone_code})")
    void addSysAlarmConfig(SysAlarmConfig sysAlarmConfig);


    @Select("SELECT * FROM sys_alarm_config where station_management_id=#{station_management_id}")
    List<SysAlarmConfig> querySysAlarmConfigByStationId(@Param("station_management_id")Long station_management_id);


    List<SysAlarmConfig> pageQuerySysAlarmConfigList(@Param("user_id")Long user_id,@Param("ids") List<Long> userId,@Param("contact_phone")String contact_phone,@Param("email")String email,@Param("station_management_id")Long station_management_id,@Param("p")Integer p,@Param("size")Integer size);


    Integer pageQuerySysAlarmConfigNum(@Param("user_id")Long user_id,@Param("ids") List<Long> userId,@Param("contact_phone")String contact_phone,@Param("email")String email,@Param("station_management_id")Long station_management_id);

    @Update("UPDATE sys_alarm_config SET contact_person = #{contact_person},contact_phone=#{contact_phone},email=#{email}, " +
            "gunTemperature=#{gunTemperature},gunPushMethod=#{gunPushMethod},gunChargeStatus=#{gunChargeStatus},batteryTemperature=#{batteryTemperature}," +
            "batteryPushMethod=#{batteryPushMethod},offline=#{offline},offlinePushMethod=#{offlinePushMethod},alarmRate=#{alarmRate},alarmPushMethod=#{alarmPushMethod},update_time=#{update_time},contact_phone_code=#{contact_phone_code} WHERE id = #{id}")
    void updSysAlarmConfig(SysAlarmConfig sysAlarmConfig);

    @Update("DELETE from sys_alarm_config  WHERE id = #{id}")
    void delSysAlarmConfigById(@Param("id")Long id);
}
