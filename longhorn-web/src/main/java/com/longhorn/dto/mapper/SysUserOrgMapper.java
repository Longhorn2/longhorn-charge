package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysAuth;
import com.longhorn.model.dto.SysUserOrg;
import com.longhorn.model.vo.SysUserAuthVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysUserOrgMapper {
    void delByUserId(@Param("user_id")Long user_id);
    int addSysUserOrg(SysUserOrg sysUserOrg);
    List<String>queryByUserId(@Param("user_id")Long user_id);

    @Select("select distinct su.id,su.user_name from sys_user su\n" +
            "        left join sys_account_user sau on sau.pid=#{id}\n" +
            "        WHERE\n" +
            "        sau.user_id=su.id\n")
    List<SysUserAuthVo>queryById(@Param("id")Long id);

    @Select("select * from sys_auth \n" +
            "    WHERE\n" +
            "  id=#{id} and type=#{type}\n")
    List<SysAuth>queryAuthById(@Param("type")Integer type,@Param("id")Long id);

    @Insert("INSERT INTO sys_auth(id,user_id,type) " +
            "VALUES (#{id},#{user_id},#{type})")
    void saveSysAuth(SysAuth sysAuth);

    @Update("DELETE from sys_auth WHERE id = #{id} and type=#{type}")
    void delSysAuth(@Param("type")Integer type,@Param("id")Long id);
}
