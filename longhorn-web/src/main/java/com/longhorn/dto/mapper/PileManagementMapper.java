package com.longhorn.dto.mapper;

import com.longhorn.model.dto.PileManagement;
import com.longhorn.model.dto.SysDataType;
import com.longhorn.model.vo.PileManagementVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PileManagementMapper {
    int addPileManagement(PileManagement pileManagement);

    int updatePileManagement(PileManagement pileManagement);
    @Select("SELECT * FROM pile_management where pile_code=#{pile_code}")
    List<PileManagement> queryByPileCode(@Param("pile_code")String pile_code);

    @Select("SELECT * FROM pile_management where id=#{id}")
    PileManagement queryByPileId(@Param("id")Long id);

    List<PileManagement> getList(PileManagementVo vo);

    @Select("select sd.* from sys_data_type sd\n" +
            "LEFT JOIN station_charge_type st on st.station_management_id=#{station_management_id}\n" +
            "WHERE st.sys_data_type_id=sd.id\n" +
            "ORDER BY sd.sort ASC")
    List<SysDataType> queryById(@Param("station_management_id")Long station_management_id);

    Integer getListNum(PileManagementVo vo);
//    List<StationInfoAllListVo> queryAllByUserId(@Param("user_id")Long userId);
}
