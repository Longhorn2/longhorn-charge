package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysAccountUser;
import com.longhorn.model.dto.SysMenu;
import com.longhorn.model.dto.SysUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Component
public interface SysUserMapper {
    SysUser queryByUserName(@Param("userName")String userName);
    SysUser queryByEmail(@Param("email")String email);
    SysUser queryByEmailAndPwd(SysUser sysUser);
    int addUser(SysUser sysUser);

    int addCustomer(SysUser sysUser);

    SysUser queryByAll(SysUser sysUser);

    int changePassword(SysUser sysUser);

    int changeInformation(@Param("userName") String userName, @Param("contact_phone") String contact_phone, @Param("contact_phone_code") String contact_phone_code,@Param("id") Long id);

    List<SysUser> queryAll(@Param("id") Long id,@Param("user_name") String user_name,@Param("phone") String phone,@Param("p") Integer p,@Param("size") Integer size);


    int queryAllTatol(@Param("id") Long id,@Param("user_name") String user_name,@Param("phone") String phone);
    void editUser(SysUser sysUser);

    List<SysUser> queryCustomerList(@Param("userId") Long userId,@Param("country") String country,@Param("province") String province,@Param("customer_type")String customer_type,@Param("status")Integer status,@Param("sex")Integer sex,@Param("email")String email,@Param("p") Integer p,@Param("size") Integer size);
    int queryCustomerTotal(@Param("userId") Long userId,@Param("country") String country,@Param("province") String province,@Param("customer_type")String customer_type,@Param("status")Integer status,@Param("sex")Integer sex,@Param("email")String email);
    int updCustomer(SysUser sysUser);
    void updCustomerById(@Param("status") Integer status, @Param("id")Long id, @Param("update_time")Timestamp update_time);

    List<SysUser> queryCustomerByCustomerType(@Param("userId") Long userId,@Param("customer_type") String customer_type);

    @Insert("INSERT INTO sys_account_user(id,pid,user_id) " +
            "VALUES (#{id},#{pid},#{user_id})")
    void addSysAccountUser(SysAccountUser sysAccountUser);

}
