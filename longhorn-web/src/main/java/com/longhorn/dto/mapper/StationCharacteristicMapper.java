package com.longhorn.dto.mapper;

import com.longhorn.model.dto.StationCharacteristic;
import com.longhorn.model.dto.SysDataType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StationCharacteristicMapper {
    void  addStationCharacteristic(StationCharacteristic stationCharacteristic);
    void delStationCharacteristicByStationManagementId(@Param("station_management_id")String station_management_id);

    List<SysDataType> queryStationCharacteristicById(@Param("station_management_id")Long station_management_id);
}
