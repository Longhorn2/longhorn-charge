package com.longhorn.dto.mapper;

import com.longhorn.model.dto.StationManagement;
import com.longhorn.model.vo.StationInfoAllListVo;
import com.longhorn.model.vo.StationInfoVo;
import com.longhorn.model.vo.StationManagementListVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StationManagementMapper {
    int addStationManagement(StationManagement stationManagement);

    int updateStationManagement(StationManagement stationManagement);
    List<StationManagement> queryByUserId(StationInfoVo vo);
    Integer queryByUserIdNum(StationInfoVo vo);
    List<StationInfoAllListVo> queryAllByUserId(@Param("ids")List<Long> userIds,@Param("user_id")Long userId);

    List<StationInfoAllListVo> queryByOrg(@Param("user_id")Long userId,@Param("area")String area,@Param("stationName")String stationName);

    StationManagementListVo queryStationById(@Param("id")Long id);

    Integer queryByUserIdNum(@Param("user_id")Long userId);
}
