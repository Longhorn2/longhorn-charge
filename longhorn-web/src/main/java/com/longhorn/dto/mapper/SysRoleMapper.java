package com.longhorn.dto.mapper;

import com.longhorn.model.dto.*;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysRoleMapper {

    List<SysRole> queryAll(@Param("id") Long id,@Param("role_name") String index, @Param("p") Integer p, @Param("size") Integer size);
    int queryAllTatol(@Param("id") Long id,@Param("role_name") String index);
    void editById(SysRole sysRole);

    void delById(@Param("id") Long id);
    void addRole(SysRole sysRole);

    List<SysRole> queryByUserId(@Param("id") Long userId);

    @Insert("INSERT INTO sys_role_auth(id,role_id,user_id) " +
            "VALUES (#{id},#{role_id},#{user_id})")
    void addSysRoleAuth(SysRoleAuth sysRoleAuth);

    @Update("delete from sys_role_auth where role_id = #{id}")
    void delSysRoleAuth(@Param("id") Long id);

    @Insert("INSERT INTO sys_user_role(role_id,user_id) " +
            "VALUES (#{role_id},#{user_id})")
    void addSysUserRole(SysUserRole sysUserRole);

    @Select("select distinct r.*,u.user_name as user_name from sys_role r\n" +
            "        left join sys_user u on u.id=r.user_id\n" +
            "        WHERE\n" +
            "        r.user_id=#{id}\n" +
            "        ORDER BY r.create_time DESC")
    List<SysRole> queryAllById(@Param("id") Long id);

    @Select("SELECT org.* FROM sys_org org \n" +
            " LEFT JOIN sys_user_org sa on sa.user_id=#{id} \n" +
            "where sa.org_id=org.id")
    List<SysOrg> queryOrgByUserId(@Param("id") Long id);


}
