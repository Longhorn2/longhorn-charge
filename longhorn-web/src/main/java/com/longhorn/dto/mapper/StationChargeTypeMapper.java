package com.longhorn.dto.mapper;

import com.longhorn.model.dto.StationChargeType;
import com.longhorn.model.dto.SysDataType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface StationChargeTypeMapper {
    void  addStationChargeType(StationChargeType StationChargeType);
    void  delStationChargeType(@Param("station_management_id")String station_management_id);

    List<SysDataType> queryStationChargeTypeById(@Param("station_management_id")Long station_management_id);
}
