package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysDataType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysDataTypeMapper {

    //List<SysDataType> queryAll();
    List<SysDataType> queryByCode(@Param("index") String index,@Param("p")Integer p,@Param("size")Integer size);
    int queryByCodeTotal(@Param("index") String index);
    void addSysDataType(SysDataType sysDataType);
    void editById(SysDataType sysDataType);

    void delById(@Param("id") Long id);
    List<SysDataType> queryAllByCode(@Param("code") String code,@Param("language")String language);


}
