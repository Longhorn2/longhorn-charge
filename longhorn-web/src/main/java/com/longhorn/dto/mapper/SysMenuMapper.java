package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysMenu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysMenuMapper {
    /**
     * @param parentid
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where parent_id =#{parent_id}  order by sort ASC")
    List<SysMenu> menuByPidList(@Param( "parent_id" )long parent_id);



    /**
     * @param id
     * @return Integer
     */
    @Update("DELETE from sys_menu  WHERE id = #{id}")
    Integer delByInfo(@Param("id") long id);

    /**
     * @param name 名称
     * @return List<SysMenu>
     */
    @Select("select * from sys_menu where name=#{name}")
    List<SysMenu> selectByName(@Param("name") String name);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_menu SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_menu SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    @Update("UPDATE sys_menu SET status = #{status},menu_name=#{menu_name},visible=#{visible}, " +
            "sort=#{sort},icon=#{icon},router_name=#{router_name},router_path=#{router_path}," +
            "router_views=#{router_views},type=#{type} WHERE id = #{id}")
    Integer updateById(SysMenu sysMenu);

    /**
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where parent_id is null or parent_id='' order by sort ASC")
    List<SysMenu> sortEnd();

    /**
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where pid =1")
    List<SysMenu> sortSon();


    List<SysMenu> getUserMenu(@Param( "user_id" )Long userId,@Param( "pid" )Long pid);

    @Select("SELECT m.* FROM sys_menu m \n" +
            "LEFT JOIN sys_user_role role on role.user_id=#{user_id}\n" +
            "LEFT JOIN sys_role_menu r on r.role_id=role.role_id\n" +
            "WHERE\n" +
            "r.menu_id=m.id and (m.parent_id is null or m.parent_id='')\n" +
            "ORDER BY m.sort ASC")
    List<SysMenu> getMenuByUserId(@Param("user_id")Long userId);

    @Select("SELECT m.* FROM sys_menu m \n" +
            "LEFT JOIN sys_role_menu r on r.role_id=#{id}\n" +
            "WHERE\n" +
            "r.menu_id=m.id and (m.parent_id is null or m.parent_id='')\n" +
            "ORDER BY m.sort ASC")
    List<SysMenu> getMenuByRoleId(@Param("id")Long id);

    @Select("SELECT m.* FROM sys_menu m \n" +
            "LEFT JOIN sys_role_menu r on r.role_id=#{id}\n" +
            "WHERE\n" +
            "r.menu_id=m.id and  m.parent_id=#{parent_id} \n" +
            "ORDER BY m.sort ASC")
    List<SysMenu> getMenuByParentIdAndRoleId(@Param("id")Long id,@Param("parent_id")Long parent_id);


    @Select("SELECT m.* FROM sys_menu m \n" +
            "LEFT JOIN sys_user_role role on role.user_id=#{user_id}\n" +
            "LEFT JOIN sys_role_menu r on r.role_id=role.role_id\n" +
            "WHERE\n" +
            "r.menu_id=m.id and  m.parent_id=#{parent_id} \n" +
            "ORDER BY m.sort ASC")
    List<SysMenu> getMenuByParentId(@Param("user_id")Long userId,@Param("parent_id")Long parent_id);


    @Insert("INSERT INTO sys_menu(id,menu_name,visible,parent_id,status,sort,icon,router_name," +
            "router_path,router_views,type) " +
            "VALUES (#{id},#{menu_name},#{visible},#{parent_id},#{status},#{sort},#{icon},#{router_name}," +
            "#{router_path},#{router_views},#{type})")
    void addMenu(SysMenu sysMenu);
    /**
     * 根据父级菜单获取子集菜单及按钮
     * @param userId 用户id
     * @param pid 父级菜单
     * @return list
     */
    List<SysMenu> selectMenuByParentId(@Param( "user_id" )Long userId,@Param( "pidList" )List<Long> pid);

    /**
     * 根据pid 查询所有sysMenu
     * @param pid pid
     * @return list
     */
    List<SysMenu> selectAllMenuByPid(@Param("pid") Long pid);

}
