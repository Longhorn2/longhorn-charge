package com.longhorn.dto.mapper;

import com.longhorn.model.dto.SysUserStationManagement;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface SysUserStationManagementMapper {
    void delByUserId(@Param("user_id")Long user_id);
    int addSysUserStationManagement(SysUserStationManagement sysUserStationManagement);

}
