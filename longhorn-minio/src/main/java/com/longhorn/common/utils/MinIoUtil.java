package com.longhorn.common.utils;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.model.vo.FileVo;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PutObjectOptions;
import io.minio.messages.Bucket;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * Minio工具类
 */
@Slf4j
@Component
public class MinIoUtil {

    public static MinioClient minioClient;
    @Value("${spring.minio.endpoint}")
    private String endpoint;
    @Value("${spring.minio.accessKey}")
    private String accessKey;
    @Value("${spring.minio.secretKey}")
    private String secretKey;
    @Value("${spring.minio.bucketName}")
    private String bucketName;

    /**
     * 初始化minio配置
     */
    @PostConstruct
    public void init() {
        try {
            log.info("Minio Initialize........................");
            minioClient = MinioClient.builder().endpoint(endpoint).credentials(accessKey, secretKey).build();
            createBucket(bucketName);
            log.info("Minio Initialize........................successful");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("初始化minio配置异常: 【{}】", e.fillInStackTrace());
        }
    }

    /**
     * 判断bucket是否存在
     */
    @SneakyThrows(Exception.class)
    public static boolean bucketExists(String bucketName) {
        return minioClient.bucketExists(bucketName);
    }

    /**
     * 创建bucket
     */
    @SneakyThrows(Exception.class)
    public static void createBucket(String bucketName) {
        boolean isExist = minioClient.bucketExists(bucketName);
        if (!isExist) {
            minioClient.makeBucket(bucketName);
        }
    }

    /**
     * 获取全部bucket
     */
    @SneakyThrows(Exception.class)
    public static List<Bucket> getAllBuckets() {
        return minioClient.listBuckets();
    }

    /**
     * 文件上传
     *
     * @param bucketName: 桶名
     * @param fileName:   文件名
     * @param filePath:   文件路径
     */
    @SneakyThrows(Exception.class)
    public static void upload(String bucketName, String fileName, String filePath) {
        minioClient.putObject(bucketName, fileName, filePath, null);
    }

    /**
     * 上传文件
     * 返回可以直接预览文件的URL
     */
    public FileVo uploadFile(MultipartFile file) {
        try {
            //如果存储桶不存在则创建
            if (!bucketExists(bucketName)) {
                createBucket(bucketName);
            }
            PutObjectOptions putObjectOptions = new PutObjectOptions(file.getInputStream().available(), -1);
            putObjectOptions.setContentType(file.getContentType());
            String originalFilename = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //得到文件流
            InputStream inputStream = file.getInputStream();
            //保证文件不重名(并且没有特殊字符)
            //String fileName = bucketName + DateUtil.format(new Date(), "_yyyyMMddHHmmss") + originalFilename;
            String fileName = bucketName + IdWorker.getId() + originalFilename;

            minioClient.putObject(bucketName, fileName, inputStream, putObjectOptions);
            FileVo vo=new FileVo();
            vo.setFileName(fileName);
            String fileUrl=getPreviewFileUrl(bucketName, fileName);
            vo.setFileUrl(fileUrl);
            return vo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public FileVo uploadFileByName(MultipartFile file,String name) {
        try {
            //如果存储桶不存在则创建
            if (!bucketExists(name)) {
                createBucket(name);
            }
            PutObjectOptions putObjectOptions = new PutObjectOptions(file.getInputStream().available(), -1);
            putObjectOptions.setContentType(file.getContentType());
            String originalFilename = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            //得到文件流
            InputStream inputStream = file.getInputStream();
            //保证文件不重名(并且没有特殊字符)
            //String fileName = bucketName + DateUtil.format(new Date(), "_yyyyMMddHHmmss") + originalFilename;
            String fileName = IdWorker.getId() + originalFilename;

            minioClient.putObject(name, fileName, inputStream, putObjectOptions);
            FileVo vo=new FileVo();
            vo.setFileName(fileName);
            String fileUrl=getPreviewFileUrl(name, fileName);
            vo.setFileUrl(fileUrl);
            return vo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 文件上传
     * 返回下载文件url地址 和下面upload方法仅传参不同
     * bucketName 也可以直接从ParamConfig对象中获取
     */
    @SneakyThrows(Exception.class)
    public static String upload(String bucketName, String fileName, InputStream stream) {
        minioClient.putObject(bucketName, fileName, stream, new PutObjectOptions(stream.available(), -1));
        return getPreviewFileUrl(bucketName, fileName);
    }

    /**
     * 文件上传
     * 返回下载文件url地址  和上面upload方法仅传参不同
     */
    @SneakyThrows(Exception.class)
    public static String upload(String bucketName, MultipartFile file) {
        final InputStream is = file.getInputStream();
        final String fileName = file.getOriginalFilename();
        minioClient.putObject(bucketName, fileName, is, new PutObjectOptions(is.available(), -1));
        is.close();
        return getPreviewFileUrl(bucketName, fileName);
    }

    /**
     * 删除文件
     *
     * @param bucketName: 桶名
     * @param fileName:   文件名
     */
    @SneakyThrows(Exception.class)
    public static void deleteFile(String bucketName, String fileName) {
        minioClient.removeObject(bucketName, fileName);
    }

    /**
     * 下载文件
     */
//    @SneakyThrows(Exception.class)
//    public static void download(String bucketName, String fileName, HttpServletResponse response) {
//        // 获取对象的元数据
//        final ObjectStat stat = minioClient.statObject(bucketName, fileName);
//        response.setContentType(stat.contentType());
//        response.setCharacterEncoding("UTF-8");
//        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
//        InputStream is = minioClient.getObject(bucketName, fileName);
//        IOUtils.copy(is, response.getOutputStream());
//        is.close();
//    }

    /**
     * 获取minio文件的下载或者预览地址
     * 取决于调用本方法的方法中的PutObjectOptions对象有没有设置contentType
     *
     * @param bucketName: 桶名
     * @param fileName:   文件名
     */
    @SneakyThrows(Exception.class)
    public static String getPreviewFileUrl(String bucketName, String fileName) {
        return minioClient.presignedGetObject(bucketName, fileName);
    }

    /**
     * 获取minio文件的下载或者预览地址
     * 取决于调用本方法的方法中的PutObjectOptions对象有没有设置contentType
     *
     * @param bucketName: 桶名
     * @param :   文件实体类
     */
    @SneakyThrows(Exception.class)
    public FileVo  getFileUrl(String bucketName, FileVo vo) {
        String url=minioClient.presignedGetObject(bucketName, vo.getFileName());
        vo.setFileUrl(url);
        return vo;
    }

    /**
     * 删除文件
     *
     * @param bucketName: 桶名
     * @param fileName:   文件名
     */
    @SneakyThrows(Exception.class)
    public  void deleteFileByName(String bucketName, String fileName) {
        minioClient.removeObject(bucketName, fileName);
    }


    /**
     * 下载文件
     */
    @SneakyThrows(Exception.class)
    public static void download(String bucketName, String fileName, HttpServletResponse response) {
        // 获取对象的元数据
        final ObjectStat stat = minioClient.statObject(bucketName, fileName);
        response.setContentType(stat.contentType());
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        InputStream is = minioClient.getObject(bucketName, fileName);
        IOUtils.copy(is, response.getOutputStream());
        is.close();
    }
}
