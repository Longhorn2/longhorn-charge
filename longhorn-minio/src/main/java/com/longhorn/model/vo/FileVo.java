package com.longhorn.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 上传文件vo 返回
 * */
@Data
public class FileVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 文件名称
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private String fileName;
    /**
     * 文件预览地址
     * */
    private String fileUrl;

}
