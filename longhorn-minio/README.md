## longhorn-minio
######minio文件服务api接口调用服务

#####上传文件 /api/v1/longhorn-minio/minio/uploadFile
##### 根据文件名获取文件地址 /api/v1/longhorn-minio/minio/getFileUrlByName
##### 根据文件名删除文件 /api/v1/longhorn-minio/minio/delFileByName
##### 根据文件名下载文件 /api/v1/longhorn-minio/minio/downloadFileByName 
