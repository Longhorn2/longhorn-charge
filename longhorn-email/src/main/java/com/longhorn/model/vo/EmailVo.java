package com.longhorn.model.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmailVo implements Serializable {

    /**
     * 发送到的邮箱地址
     * */
    private String toEmail;
    /**
     * 标题
     * */
    private String title;
    /**
     * 邮箱内容
     * */
    private String message;
}
