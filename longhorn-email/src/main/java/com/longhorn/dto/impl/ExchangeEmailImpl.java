package com.longhorn.dto.impl;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.service.EmailService;
import com.longhorn.model.vo.EmailVo;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExchangeEmailImpl implements EmailService {
    @Value("${spring.mail.hostname}")
    private String hostname;
    @Value("${spring.mail.username}")
    private String username;
    @Value("${spring.mail.password}")
    private String password;



    public void sendEmail(){
        ExchangeClient client = new ExchangeClient.ExchangeClientBuilder()
                .hostname(hostname)
                .exchangeVersion(ExchangeVersion.Exchange2010)
                .username(username)
                .password(password)
                .recipientTo("guimin.ding@long-horn.com")
                .subject("Test Subject")
                .message("Test Message")
                .build();
        client.sendExchange();

    }

    @Override
    public ResponseVO sendEmail(EmailVo vo) {

        try {
            ExchangeClient client = new ExchangeClient.ExchangeClientBuilder()
                    .hostname(hostname)
                    .exchangeVersion(ExchangeVersion.Exchange2010)
                    .username(username)
                    .password(password)
                    .recipientTo(vo.getToEmail())
                    .subject(vo.getTitle())
                    .message(vo.getMessage())
                    .build();
            client.sendExchange();
        }catch (Exception e){
            return new ResponseVO(ErrorCodeEnum.EMAIL_ERROR,e.toString());
        }


        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
    }
}
