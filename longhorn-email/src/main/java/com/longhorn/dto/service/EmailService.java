package com.longhorn.dto.service;

import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.vo.EmailVo;

public interface EmailService {
    /**
     * toEmail 接收验证码的邮箱
     * text 主题
     * message 主体信息
     * */
    public ResponseVO sendEmail(EmailVo vo);
}