package com.longhorn.controller;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.longhorn.common.business.BusinessException;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.impl.OrderServiceImpl;
import com.longhorn.model.dto.Order;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 分库分表
 * @author gmding
 * @date 2023-05-08
 * */
@Api(tags = "sharding测试")
@RestController
@RequestMapping("/sharding/")
@Slf4j
public class ShardingController {

    @Autowired
    private OrderServiceImpl orderServiceImpl;


    @ApiOperation(value = "充电数据新增")
    @RequestMapping(value = "/andChargeProcessData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO andChargeProcessData(@RequestBody Order vo, HttpServletRequest request) throws BusinessException {
        log.info("充电数据新增:{}",new Gson().toJson(vo));
        return orderServiceImpl.addOneOrder(vo);
    }
    @ApiOperation("充电数据实时查询")
    @RequestMapping(value = "/showChargeData",method = RequestMethod.GET)
    public ResponseVO showChargeData(@RequestParam("orderId")Long orderId, HttpServletRequest request) throws BusinessException{
        log.info("订单号：orderId:{}",orderId);
        return orderServiceImpl.showCharge(orderId,request);
    }

    @ApiOperation("saas充电过程查询")
    @RequestMapping(value = "/saasChargeData",method = RequestMethod.GET)
    public ResponseVO saasChargeData(@RequestParam("orderId")Long orderId, HttpServletRequest request) throws BusinessException{
        log.info("订单号：orderId:{}",orderId);
        return orderServiceImpl.saasChargeData(orderId,request);
    }
}
