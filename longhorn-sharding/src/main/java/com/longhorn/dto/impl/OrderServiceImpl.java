package com.longhorn.dto.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.longhorn.common.business.ResponseVO;
import com.longhorn.common.enums.ErrorCodeEnum;
import com.longhorn.common.enums.StatusCodeEnum;
import com.longhorn.dto.mapper.OrderMapper;
import com.longhorn.dto.service.OrderService;
import com.longhorn.model.dto.*;
import com.longhorn.model.vo.SaasOrderVO;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    //列出所有订单
    @Override
    public List<Order> getAllOrder() {
        List<Order> order_list = orderMapper.selectAllOrder();
        return order_list;
    }

    @Override
    public ResponseVO showCharge(Long orderId, HttpServletRequest request) {
        List<OrderVO> orders = orderMapper.showCharge(orderId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,orders,request.getSession().getAttribute("language").toString());
    }

    @Override
    public ResponseVO saasChargeData(Long orderId, HttpServletRequest request) {
        List<SaasOrderVO> orders = orderMapper.saasChargeData(orderId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,orders,request.getSession().getAttribute("language").toString());
    }


    //添加订单
    @Override
    public ResponseVO addOneOrder(Order orderOne) {
//        for (int i=0;i<5;i++){
//            orderOne.setOrderId(Long.valueOf(i));
//            orderOne.setId(IdWorker.getId());
//            orderOne.setPile_code("123456");
//            orderOne.setCreate_time(new Timestamp(System.currentTimeMillis()));
//            int num = orderMapper.insertOneOrder(orderOne);
//        }
        orderOne.setId(IdWorker.getId());
        orderOne.setCreate_time(new Timestamp(System.currentTimeMillis()));
        int num = orderMapper.insertOneOrder(orderOne);
        return new ResponseVO(StatusCodeEnum.SUCCESS,null);
    }

}
