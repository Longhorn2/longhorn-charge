package com.longhorn.dto.service;


import com.longhorn.common.business.ResponseVO;
import com.longhorn.model.dto.Order;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderService {
    public ResponseVO addOneOrder(Order userOne);
    public List<Order> getAllOrder();

    ResponseVO showCharge(Long orderId, HttpServletRequest request);
    ResponseVO saasChargeData(Long orderId, HttpServletRequest request);
}
