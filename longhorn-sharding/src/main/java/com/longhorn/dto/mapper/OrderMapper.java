package com.longhorn.dto.mapper;

import com.longhorn.model.dto.Order;
import com.longhorn.model.dto.OrderVO;
import com.longhorn.model.vo.SaasOrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OrderMapper {
    int insertOneOrder(Order orderOne);
    String selectNameById(String userId);
    List<Order> selectAllOrder();

    List<OrderVO> showCharge(@Param("orderId") Long orderId);

    @Select("select voltage,current,temperature,power,create_time from t_order  where orderId=#{orderId} order by create_time limit 0,90")
    List<SaasOrderVO> saasChargeData(@Param("orderId") Long orderId);

}