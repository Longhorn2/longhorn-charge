package com.longhorn.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
public class OrderVO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 电压
     */
    private BigDecimal voltage;
    /**
     * 电流
     */
    private BigDecimal current;
    /**
     * 温度
     */
    private BigDecimal temperature;
    /**
     * 功率
     */
    private BigDecimal power;
}
