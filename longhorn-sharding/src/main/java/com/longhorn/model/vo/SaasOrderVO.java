package com.longhorn.model.vo;

import com.longhorn.model.dto.OrderVO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class SaasOrderVO extends OrderVO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 时间
     */
    private Timestamp create_time;
}
